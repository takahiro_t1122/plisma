<?php

use Cake\Core\Configure;

$activationUrl = [
    '_full' => true,
    'plugin' => null,
    'controller' => 'Users',
    'action' => 'resetPassword',
    isset($token) ? $token : ''
];

if ($role == ROLE_CORP) {
    $occupation = $company_name;
} else {
    $occupation = $bank_name;
}
?>

<?= __("{0} {1}様

いつもplismaをご利用いただき、誠にありがとうございます。

パスワード再設定のお手続きを受け付けいたしました。
お手数お掛け致しますが、下記URLを{2}時間以内にクリックし、メールアドレスの認証を完了してください。
{3}

{2}時間を超過しますと、セキュリティ保持のため上記URLの有効期限が切れます。
有効期限が切れた場合は、お手数お掛け致しますが、再度お手続きをお願い致します。",
$occupation,
$last_name.$first_name,
Configure::read('Users.Token.expiration') / (60 * 60),
$this->Url->build($activationUrl)
) ?>

<?= $this->element("Email/footer"); ?>