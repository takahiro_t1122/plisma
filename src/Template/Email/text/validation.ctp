<?php
use Cake\Core\Configure;

$activationUrl = [
    '_full' => true,
    'plugin' => null,
    'controller' => 'Users',
    'action' => 'validateEmail',
    isset($token) ? $token : ''
];
?>

<?= __("この度はplismaに登録のお申込みいただき、誠にありがとうございます。

このメールは、お客様がご登録されたメールアドレスに、
間違いがないかどうかを、ご本人さまにご確認いただくためのものです。

お手数お掛け致しますが、下記URLを{0}時間以内にクリックし、メールアドレスの認証を完了してください。
{1}

{0}時間を超過しますと、セキュリティ保持のため上記URLの有効期限が切れます。
有効期限が切れた場合は、お手数お掛け致しますが、サポートへご連絡ください。", 
Configure::read('Users.Token.expiration') / (60 * 60),
$this->Url->build($activationUrl)
) ?>

<?= $this->element("Email/footer"); ?>