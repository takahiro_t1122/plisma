<?= __(
"---------------------------------------------------------------------
銀行ユーザ追加のおしらせ
---------------------------------------------------------------------

新規に銀行ユーザが追加されました。
契約情報、入金情報を確認し、確認が取れましたらUsers.activeの値を1に更新してください。

銀行名: {0}
支店名: {1}
登録ユーザメールアドレス: {2}
担当者名: {3}

認識相違なければ以下のリンクから手続きを完了させてください。
アクティブURL => pending(未実装)",
$bank_name,
$bank_branch_name,
$email,
$last_name.$first_name
) ?>