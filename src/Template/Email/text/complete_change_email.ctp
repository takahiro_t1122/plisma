<?php

use Cake\Core\Configure;

if ($role == ROLE_CORP) {
    $occupation = $company_name;
} else {
    $occupation = $bank_name;
}
?>

<?= __("{0} {1}様

いつもplismaをご利用いただき、誠にありがとうございます。

ご登録メールアドレスの変更処理が完了いたしました。

**********************************
ご変更前メールアドレス: {2}
　　　　↓
ご変更後メールアドレス: {3}
**********************************

今後ともplismaをどうぞ宜しくお願いいたします。",
$occupation,
$last_name.$first_name,
$change_email,
$email
) ?>

<?= $this->element("Email/footer"); ?>
