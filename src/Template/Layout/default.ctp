<?php

use Cake\Core\Configure;

$cakeDescription = 'plisma';

?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <!-- bootstrap ui -->
        <?= $this->Html->css('/bootstrap_u_i/css/bootstrap-reboot.min.css') ?>
        <?= $this->Html->css('/bootstrap_u_i/css/bootstrap.min.css') ?>

        <?= $this->Html->css('https://fonts.googleapis.com/css?family=Noto+Sans+JP&amp;subset=japanese') ?>
        <?= $this->Html->css('/scss/plisma-base.min.css') ?>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <?= $this->Html->script('/bootstrap_u_i/js/jquery.min.js') ?>
        <?= $this->Html->script('/bootstrap_u_i/js/popper.min.js') ?>
        <?= $this->Html->script('/bootstrap_u_i/js/bootstrap.min.js') ?>

        <?= $this->Html->script('/js/default.min.js', ['block' => true]); ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <header class="header">
            <?php
                if ($this->request->getSession()->read('Auth.User.role') === ROLE_CORP) {
                    echo $this->element("menu/corp");
                } else if ($this->request->getSession()->read('Auth.User.role') === ROLE_BANK) {
                    echo $this->element("menu/bank");
                } else {
                    echo $this->element("menu/default");
                }
            ?>
        </header>
        <?= $this->Flash->render() ?>
        <div class="container-fluid clearfix px-0">
            <?= $this->fetch('content') ?>
        </div>
        <footer class="footer pt-1">
            <?= $this->element("footer/default") ?>
        </footer>
    </body>
</html>