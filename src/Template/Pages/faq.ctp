<?php

echo $this->Html->script('/js/pages/faq.js', ['block' => true]);
echo $this->Html->css('/scss/pages/faq.min.css');

?>
<div class="contents-wrapper container" id="category">
    <ul>
        <li><a href="#service">サービス</a></li>
        <li><a href="#register-loan">案件登録</a></li>
        <li><a href="#register-user">ユーザ登録</a></li>
        <li><a href="#payment">支払</a></li>
    </ul>
</div>

<div class="contents-wrapper container" id="service">
    <div class="contents-label">
        <?= __('サービス') ?>
    </div>
    <div class="mb-4">
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#service_1" class="" aria-expanded="true">
                    <strong>Q:</strong>セキュリティが不安です。
                </a>
            </div>
            <div id="service_1" class="in b-t collapse" style="">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-success">A</span>
                    </div>
                    <p class="text-muted">当社のサービスは審査に通過した金融機関のみが登録されております。<br />
                    よって投稿された案件は不特定多数に見られるのではなく、特定の銀行に見られるものであり、情報が漏洩することはありません。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#service_2">
                    <strong>Q:</strong>料金プランを知りたいです。
                </a>
            </div>
            <div id="service_2" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-info">A</span>
                    </div>
                    <p class="text-muted">コーポレートユーザーは基本利用料無料となっております。<br />
                    オプションプランとして、案件登録通知機能（コーポレートユーザーが案件を登録した際に銀行へ通知を送る機能）は1行あたり500円で利用可能です。</p>
                </div>
            </div>
        </div>
    </div>
    <div class="back-to-pagetop text-right">
        <a href="#category">Page Topへ戻る</a>
    </div>
</div>

<div class="contents-wrapper container" id="register-loan">
    <div class="contents-label">
        <?= __('案件登録') ?>
    </div>
    <div class="mb-4">
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-loan_1" class="" aria-expanded="true">
                    <strong>Q:</strong>案件の制限はありますか。
                </a>
            </div>
            <div id="register-loan_1" class="in b-t collapse" style="">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-success">A</span>
                    </div>
                    <p class="text-muted">登録可能案件は1件のみです。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-loan_2">
                    <strong>Q:</strong>銀行の乗り換えを考えており、開示したくない銀行があります。
                </a>
            </div>
            <div id="register-loan_2" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-info">A</span>
                    </div>
                    <p class="text-muted">案件登録時に開示したくない銀行を選択することができます。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-loan_3">
                    <strong>Q:</strong>アカウント登録に必要なものを教えてください。
                </a>
            </div>
            <div id="register-loan_3" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-warning">A</span>
                    </div>
                    <p class="text-muted">アカウント登録時には、XXXX、XXXXが必要です。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-loan_4">
                    <strong>Q:</strong>案件登録時に必要な情報は何ですか。
                </a>
            </div>
            <div id="register-loan_4" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-success">A</span>
                    </div>
                    <p class="text-muted">案件登録時には、案件名、融資希望金額、、、、</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-loan_5">
                    <strong>Q:</strong>案件を削除したい。
                </a>
            </div>
            <div id="register-loan_5" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-success">A</span>
                    </div>
                    <p class="text-muted">案件更新画面から削除することができます。</p>
                </div>
            </div>
        </div>
    </div>
    <div class="back-to-pagetop text-right">
        <a href="#category">Page Topへ戻る</a>
    </div>
</div>

<div class="contents-wrapper container" id="register-user">
    <div class="contents-label">
        <?= __('ユーザ登録') ?>
    </div>
    <div class="mb-4">
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-user_1" class="" aria-expanded="true">
                    <strong>Q:</strong>ユーザー登録したいのに、メールアドレスが使用されている。
                </a>
            </div>
            <div id="register-user_1" class="in b-t collapse" style="">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-success">A</span>
                    </div>
                    <p class="text-muted">新規登録して頂いている最中に登録画面から離脱してしまった場合、アカウントは仮登録している状態となります。<br />
                    そのため、新規登録ではなく、ログイン画面から再ログインしていただけます。登録メールアドレスとパスワードを入力いただくようお願いいたします。<br />
                    それでもログインできない場合は、サポートチームまでご連絡下さいませ。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-user_2">
                    <strong>Q:</strong>plismaからメールが届かない。
                </a>
            </div>
            <div id="register-user_2" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-info">A</span>
                    </div>
                    <p class="text-muted">まずは、登録メールアドレスが正しいかどうかご確認ください。</p>

                    <p class="text-muted">また、セキュリティ設定のためユーザー受信拒否と認識されている、お客様が迷惑メール対策等でドメイン指定受信を設定されている場合に、メールが正しく届かない場合がございます。</p>

                    <p class="text-muted">ご登録メールアドレスはPCメールアドレスを推奨しております。<br />
                    弊社からお客様あてにお送りするメールのドメインは「@plisma.co.jp」です。こちらの受信設定を許可いただくようお願いいたします。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-user_3">
                    <strong>Q:</strong>パスワードを忘れた。
                </a>
            </div>
            <div id="register-user_3" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-warning">A</span>
                    </div>
                    <p class="text-muted">パスワードを忘れた場合は、ログイン画面の<br />
                    「メールアドレスまたはパスワードを忘れた場合」<br />
                    より、パスワードの再発行手続きをお願いいたします。</p>

                    <p class="text-muted">ご登録のメールアドレスを入力して頂くと、そのメールアドレス宛てにパスワード再発行のご案内メールをお送りいたします。ご案内メール送信後、24時間以内に手続きをしていただくようお願いします。</p>

                    <p class="text-muted">なお、ユーザーIDのメールアドレスをお忘れになられた場合は、ご登録氏名とご登録電話番号を明記のうえ、ご連絡お願い致します。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-user_4">
                    <strong>Q:</strong>退会したい。
                </a>
            </div>
            <div id="register-user_4" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-success">A</span>
                    </div>
                    <p class="text-muted">アカウントの停止・退会をご希望される場合は、お手数ですがこちらのリンクから手続きをお願いいたします。<br />
                    URL<br />
                    退会後に問題が生じた場合のご対応はできかねますのでご了承下さい。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#register-user_5">
                    <strong>Q:</strong>ユーザー情報を変更したい。
                </a>
            </div>
            <div id="register-user_5" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-success">A</span>
                    </div>
                    <p class="text-muted">ログイン後、設定ページからユーザー情報を変更できます。</p>
                </div>
            </div>
        </div>
    </div>
    <div class="back-to-pagetop text-right">
        <a href="#category">Page Topへ戻る</a>
    </div>
</div>

<div class="contents-wrapper container" id="payment">
    <div class="contents-label">
        <?= __('支払') ?>
    </div>
    <div class="mb-4">
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#payment_1" class="" aria-expanded="true">
                    <strong>Q:</strong>支払方法について。
                </a>
            </div>
            <div id="payment_1" class="in b-t collapse" style="">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-success">A</span>
                    </div>
                    <p class="text-muted">MasterCard、Visa、American Express、JCB、Diners Club, Discoverのカードをご利用していただけます。<br />
                    クレジットカードに限らず、デビットカードも、上記の会社のものであればご利用いただけます。</p>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="card-header no-border">
                <a data-toggle="collapse" data-target="#payment_2">
                    <strong>Q:</strong>領収書はありますか。
                </a>
            </div>
            <div id="payment_2" class="collapse b-t">
                <div class="card-body">
                    <div class="float-left mr-2">
                        <span class="text-md w-32 avatar circle bg-info">A</span>
                    </div>
                    <p class="text-muted">要検討</p>
                </div>
            </div>
        </div>
    </div>
    <div class="back-to-pagetop text-right">
        <a href="#category">Page Topへ戻る</a>
    </div>
</div>

<div class="contents-wrapper container" id="register-user">
    <div class="contents-label">
        <?= __('お問い合わせ') ?>
    </div>
    <div class="row mb-4">
        <div class="col-lg-6">
            <div class="contents-semi-label">
                <?= __('電話でのお問い合わせ') ?>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="contents-semi-label">
                <?= __('メールでのお問い合わせ') ?>
            </div>

        </div>
    </div>
</div>