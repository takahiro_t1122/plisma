<?php
use Cake\Core\Configure;
echo $this->Html->css('/scss/pages/terms.min.css');

?>

<div class="contents-wrapper container tokusyo">
    <div class="contents-label mb-3">
        <?= __('特定商取引法に基づく表示') ?>
    </div>
    <div class="container text-center">
        <div class="row">
            <div class="col-md-2 offset-md-1 text-left"><p>販売業者の名称</p></div>
            <div class="col-md-9 text-left"><p>株式会社plisma</p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>販売業者の住所</p></div>
            <div class="col-md-9 text-left"><p><?= ADDRESS ?></p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>販売業者の連絡先</p></div>
            <div class="col-md-9 text-left"><p>Email: <?= EMAIL_BASE ?><br />
            TEL: xxx-xxxx-xxxx<br />
            受付時間: 平日10:00〜12:00, 13:00〜17:00 （土・日・祝祭日は休業日となっております）</p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>代表者</p></div>
            <div class="col-md-9 text-left"><p>代表取締役: <?= CEO ?></p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>商品の価格</p></div>
            <div class="col-md-9 text-left"><p>各商品ごとに表示</p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>商品以外の必要金額</p></div>
            <div class="col-md-9 text-left"><p>消費税</p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>支払方法</p></div>
            <div class="col-md-9 text-left"><ul><li>請求書払い</li>
            <li>クレジットカード払い（Visa、Mastercard、American Express、Discover、Diners Club、JCB）</li></ul></div>
            <div class="col-md-2 offset-md-1 text-left"><p>支払期限</p></div>
            <div class="col-md-9 text-left"><ul><li>請求書払い: 請求書に記載のお支払期日（通常、末日締め翌月末払い）までにお支払い</li>
            <li>クレジットカード払い: 購入手続き完了時に課金され､お客様の契約に基づいた引き落とし日にお支払い</li></ul></div>
            <div class="col-md-2 offset-md-1 text-left"><p>商品の引渡時期</p></div>
            <div class="col-md-9 text-left"><p>原則として商品購入手続き完了後に利用開始となります</p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>返品の取扱方法</p></div>
            <div class="col-md-9 text-left"><p>商品の特性上、返品は承っておりません</p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>解約条件</p></div>
            <div class="col-md-9 text-left"><p>途中解約による返金は承っておりません</p></div>
            <div class="col-md-2 offset-md-1 text-left"><p>利用推奨環境</p></div>
            <div class="col-md-9 text-left"><p>Google Chrome v75.0</p></div>
        </div>
    </div>
</div>