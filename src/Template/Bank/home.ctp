<?php

use Cake\Core\Configure;
use Cake\Cache\Cache;

echo $this->Html->css('/scss/bank/search-condition.min.css');
echo $this->Html->script('/js/bank/home.min.js', ['block' => true]);

?>
<?php if ($user->phone) { ?>
    <div class="contents-wrapper container profile">
        <div class="profile-contents row">
            <div class="occupation col-12 d-flex justify-content-center font-3x-large py-3">
                <?= h($user->bank->bank_name) ?>
            </div>
            <div class="user-info col-6">
                <div class="bank-name col-12">
                    <?= h($user->bank->bank_name.$user->bank_branch_name) ?>
                </div>
                <div class="position col-12">
                    <?= h($user->position) ?>
                </div>
                <div class="name col-12">
                    <?= h($user->last_name).' '.h($user->first_name) ?>
                </div>
                <div class="phone col-12">
                    <?= h($user->phone) ?>
                </div>
                <div class="update-button col-6 text-left">
                    <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'edit', $user->id]); ?>'" value="ユーザ情報を更新する" class="button button-sm py-1 w-100" />
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="contents-wrapper container">
    <div class="contents-semi-label">
        <?= __('担当案件更新情報') ?>
    </div>
    <div class="favs">
        <div class="contents-semi-label">
            <?= __('お気に入り案件') ?>
        </div>
        <div class="fav-items">
            <?php foreach($fav_items as $fav_item) { ?>
                <a href="./view-detail?id=<?= $fav_item->id ?>">
                    <?php echo h($fav_item->loan_title) ?>
                </a><br />
            <?php } ?>
        </div>
    </div>
</div>

<div class="contents-wrapper container search-condition">
    <div class="contents-semi-label">
        <?= __('案件検索') ?>
    </div>

    <div class="search-contents row">
        <?= $this->Form->create('', ['id' => 'search_param', 'type' => 'get', 'class' => 'col-12']);?>
        <fieldset>

        <div class="keyword-search row">
            <?php echo $this->Form->control('search', ['label' => __('キーワード'), 'default' => h($this->request->getSession()->read('param.search'))]); ?>
        </div>

        <div class="down-triangle">
            <!-- triangle here --->
        </div>

        <div class="contents-semi-label">
            <?= __('詳細条件設定(任意)') ?>
        </div>

        <div class="advanced-setting">
            <div class="row table-row">
                <div class="col-3 label-area">
                    <?php echo $this->Form->label('area', __('所在地')); ?>
                </div>
                <div class="col-9 form-area d-flex align-items-center">
                    <?php echo $this->Form->control('area', [
                        'type' => 'select',
                        'class' => 'form-control-sm w-100',
                        'options' => $area_options,
                        'label' => false,
                        'default' => h($this->request->getSession()->read('param.area'))]); ?>
                </div>
            </div>
            <div class="row table-row">
                <div class="col-3 label-area">
                    <?php echo $this->Form->label('employees', __('社員数')); ?>
                </div>
                <div class="col-9 form-area d-flex align-items-center">
                    <?php echo $this->Form->control('employees_min', [
                        'type' => 'select',
                        'class' => 'form-control-sm w-100',
                        'options' => ['' => '', '1' => '1人', '5' => '5人', '20' => '20人', '100' => '100人', '300' => '300人'],
                        'label' => false,
                        'default' => h($this->request->getSession()->read('param.employees_min'))]); ?>
                    <div class="tilde">〜</div>
                    <?php echo $this->Form->control('employees_max', [
                        'type' => 'select',
                        'class' => 'form-control-sm w-100',
                        'options' => ['' => '', '300' => '300人', '100' => '100人', '20' => '20人', '5' => '5人'],
                        'label' => false,
                        'default' => h($this->request->getSession()->read('param.employees_max'))]); ?>
                </div>
            </div>
            <div class="row table-row">
                <div class="col-3 label-area">
                    <?php echo $this->Form->label('desired_loan_amount', __('融資希望額')); ?>
                </div>
                <div class="col-9 form-area d-flex align-items-center">
                    <?php echo $this->Form->control('desired_loan_amount_min',[
                        'label' => false,
                        'class' => 'form-control-sm w-100',
                        'options' => ['' => '', '1000000' => '1百万円', '5000000' => '5百万円', '10000000' => '10百万円', '30000000' => '30百万円', '100000000' => '100百万円'],
                        'default' => h($this->request->getSession()->read('param.desired_loan_amount_min'))]); ?>
                    <div class="tilde">〜</div>
                    <?php echo $this->Form->control('desired_loan_amount_max',[
                        'label' => false,
                        'class' => 'form-control-sm w-100',
                        'options' => ['' => '', '100000000' => '100百万円', '30000000' => '30百万円', '10000000' => '10百万円', '5000000' => '5百万円', '1000000' => '1百万円'],
                        'default' => h($this->request->getSession()->read('param.desired_loan_amount_max'))]); ?>
                </div>
            </div>
            <div class="row table-row">
                <div class="col-3 label-area">
                    <?php echo $this->Form->label('loan_period', __('融資期間')); ?>
                </div>
                <div class="col-9 form-area d-flex align-items-center">
                    <?php echo $this->Form->control('loan_period_min',[
                        'label' => false,
                        'class' => 'form-control-sm w-100',
                        'options' => ['' => '', '1' => '1ヶ月', '3' => '3ヶ月', '6' => '6ヶ月', '12' => '12ヶ月 (1年)', '24' => '24ヶ月 (2年)', '36' => '36ヶ月 (3年)'],
                        'default' => h($this->request->getSession()->read('param.loan_period_min'))]); ?>
                    <div class="tilde">〜</div>
                    <?php echo $this->Form->control('loan_period_max',[
                        'label' => false,
                        'class' => 'form-control-sm w-100',
                        'options' => ['' => '', '36' => '36ヶ月 (3年)', '24' => '24ヶ月 (2年)', '12' => '12ヶ月 (1年)', '6' => '6ヶ月', '3' => '3ヶ月', '1' => '1ヶ月'],
                        'default' => h($this->request->getSession()->read('param.loan_period_max'))]); ?>
                </div>
            </div>
            <div class="row table-row">
                <div class="col-3 label-area">
                    <?php echo $this->Form->label('loan_purpose', __('融資使途')); ?>
                </div>
                <div class="col-9 form-area d-flex align-items-center">
                    <?php echo $this->Form->select('loan_purpose',
                        ['0' => '運転資金', '1' => '設備資金', '2' => 'その他'],
                        ['multiple' => 'checkbox',
                        'class' => 'checkbox justify-content-center',
                        'val' => $this->request->getSession()->read('param.loan_purpose')]); ?>
                </div>
            </div>
            <div class="row table-row">
                <div class="col-12 label-area">
                    <?php echo $this->Form->label('category', __('業種')); ?>
                </div>
            </div>
            <div class="row table-row">
                <div class="col-12 py-3">
                    <ul>
                        <?php foreach($category_list as $category) { ?>
                            <li class="row category">
                                <div class="col-3 px-0">
                                    <input type="checkbox" class="checkbox" name="category[]" id=<?php echo $category->id ?> value=<?php echo $category->id;
                                    if (in_array($category->id, $this->request->getSession()->read('param.category'))) echo ' checked'; ?>>
                                    <label for=<?php echo $category->id ?>><?php echo $category->name ?></label>
                                </div>
                                <ul class="col-9">
                                    <?php foreach($sub_category_list as $sub_category) {
                                        if ($sub_category->category_id == $category->id) { ?>
                                        <li class="sub-category align-top">
                                            <input type="checkbox" class="checkbox" name="sub_category[]" id=<?php echo $sub_category->id ?> value=<?php echo $sub_category->id;
                                            if (in_array($sub_category->id, $this->request->getSession()->read('param.sub_category'))) echo ' checked'; ?>>
                                            <label for=<?php echo $sub_category->id ?>><?php echo $sub_category->name ?></label>
                                        </li>
                                    <?php }} ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            </fieldset>
            <div class="row">
                <div class="col-6 text-right pr-5">
                    <?php echo $this->Form->button(__('リセット'), ['type' => 'button', 'id' => 'reset', 'class' => 'button button-md']); ?>
                </div>
                <div class="col-6 text-left pl-5">
                    <?php echo $this->Form->button(__('検索'), ['id' => 'submit', 'class' => 'button button-md']); ?>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>