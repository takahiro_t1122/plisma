<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;

echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.2/jquery.modal.min.js');
echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.2/jquery.modal.min.css');

echo $this->Html->css('/scss/bank/searched-view.min.css');
echo $this->Html->script('/js/bank/search.min.js', ['block' => true]);

//ToDo
$header_img = LOAN_HEADER_DIR . $loginId . PNG;
$header_img_suffix = $header_img . '?' . date('YmdHis');
$header_no_img = LOAN_HEADER_DIR . 'no_image' . PNG;

?>
<div class="contents-wrapper container search-result">
    <div class="contents-semi-label">
        <?= __('Bank web-top screen') ?>
    </div>
    <div class="row">

        <nav id="sidebar" class="sidebar-wrapper col-lg-3">
            <div id="dismiss" class="d-lg-none">
                <i class="fas fa-arrow-left"></i>
            </div>

            <div class="sidebar-header contents-semi-label">
                <?= __('条件設定') ?>
            </div>

            <div class="sidebar-content">
                <div class="category">
                    <div class="category-label">
                        <?php echo $this->Form->label('category', __('業種')); ?>
                    </div>
                    <div class="category-content">
                        <?php
                            foreach($subcategory_list as $subcategory) {
                                if (in_array($subcategory->category_id, $category_array, true)){
                                    if ($previous_category != $subcategory->category->name) {
                                        if ($subcategory === reset($subcategory_list)) { ?> <div class="cate-label"> <?php }
                                        else {?> <div class="cate-label pt-2"> <?php } ?>
                                            <?= $subcategory->category->name ?></div>
                                        <div class="subcate-label pl-3">すべての項目</div>
                                <?php }
                                    $previous_category = $subcategory->category->name;
                                } else {
                                    if ($previous_category != $subcategory->category->name) {
                                        if ($subcategory === reset($subcategory_list)) { ?> <div class="cate-label"> <?php }
                                        else {?> <div class="cate-label pt-2"> <?php } ?><?= $subcategory->category->name ?></div>
                                <?php } ?>
                                        <div class="subcate-label pl-3"><?= $subcategory->name ?></div>
                                <?php
                                    $previous_category = $subcategory->category->name;
                                }
                            }
                        ?>
                    </div>
                    <div class="category-update row py-2">
                        <div class="col-lg-9 px-0 d-flex align-items-center justify-content-center">
                            <div class="text-center">
                                <a href="#category-update" rel="modal:open" class="my-1 button button-xs button-gray py-1 px-2" role="button">業種変更</a>
                            </div>
                            <div id="category-update" class="modal">
                                <div class="contents-semi-label">
                                    <?= __('業種選択') ?>
                                </div>
                                <?= $this->Form->create('', ['url' => ['controller' => 'bank', 'action' => 'search'], 'type' => 'get']) ?>
                                <fieldset>
                                    <div class="row table-row">
                                        <div class="col-12 py-3">
                                            <div class="input select">
                                                <ul>
                                                    <li class="row category-header mb-2 font-weight-bold">
                                                        <div class="col-3">
                                                            業種
                                                        </div>
                                                        <div class="col-9">
                                                            業種詳細
                                                        </div>
                                                    </li>
                                                    <?php foreach($category_list as $category) { ?>
                                                        <li class="row category">
                                                            <div class="col-3">
                                                                <input type="checkbox" class="checkbox" name="category[]" id=<?php echo $category->id ?> value=<?php echo $category->id;
                                                                if (in_array($category->id, $this->request->getSession()->read('param.category'))) echo ' checked'; ?>>
                                                                <label for=<?php echo $category->id ?>><?php echo $category->name ?></label>
                                                            </div>
                                                            <ul class="col-9">
                                                                <?php foreach($sub_category_list as $sub_category) {
                                                                    if ($sub_category->category_id == $category->id) { ?>
                                                                    <li class="sub-category align-top">
                                                                        <input type="checkbox" class="checkbox" name="sub_category[]" id=<?php echo $sub_category->id ?> value=<?php echo $sub_category->id;
                                                                        if (in_array($sub_category->id, $this->request->getSession()->read('param.sub_category'))) echo ' checked'; ?>>
                                                                        <label for=<?php echo $sub_category->id ?>><?php echo $sub_category->name ?></label>
                                                                    </li>
                                                                <?php }} ?>
                                                            </ul>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="col-12 text-center">
                                    <?= $this->Form->button(__('検索'), ['id' => 'submit', 'class' => 'button button-md']); ?>
                                </div>
                                <?= $this->Form->end() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $this->Form->create('', ['id' => 'search_param', 'type' => 'get']);?>
                    <fieldset>
                        <div class="keyword pt-3">
                            <div class="keyword-label">
                                <?php echo $this->Form->label(__('キーワード')); ?>
                            </div>
                            <div class="keyword-content">
                                <?php echo $this->Form->control('search', ['label' => false, 'default' => h($this->request->getSession()->read('param.search'))]);?>
                            </div>
                        </div>
                        <div class="area pt-3">
                            <div class="area-label">
                                <?php echo $this->Form->label(__('所在地')); ?>
                            </div>
                            <div class="area-content row">
                                <div class="col-5 pr-2">
                                    <?php echo $this->Form->control('area', [
                                        'type' => 'select',
                                        'class' => 'form-control-sm w-100',
                                        'options' => $area_options,
                                        'label' => false,
                                        'default' => h($this->request->getSession()->read('param.area'))]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="employees pt-3">
                            <div class="employees-label">
                                <?php echo $this->Form->label('employees', __('社員数')); ?>
                            </div>
                            <div class="empployees-content row">
                                <div class="col-5 pr-2">
                                    <?php echo $this->Form->control('employees_min', [
                                        'type' => 'select',
                                        'class' => 'form-control-sm w-100',
                                        'options' => ['' => '', '1' => '1人', '5' => '5人', '20' => '20人', '100' => '100人', '300' => '300人'],
                                        'label' => false,
                                        'default' => h($this->request->getSession()->read('param.employees_min'))]); ?>
                                </div>
                                <div class="tilde col-1 px-0">〜</div>
                                <div class="col-5 pl-0">
                                    <?php echo $this->Form->control('employees_max', [
                                        'type' => 'select',
                                        'class' => 'form-control-sm w-100',
                                        'options' => ['' => '', '300' => '300人', '100' => '100人', '20' => '20人', '5' => '5人'],
                                        'label' => false,
                                        'default' => h($this->request->getSession()->read('param.employees_max'))]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="desired-loan-amount pt-3">
                            <div class="desired-loan-amount-label">
                                <?php echo $this->Form->label('desired_loan_amount', __('融資希望額')); ?>
                            </div>
                            <div class="desired-loan-amount-content row">
                                <div class="col-5 pr-2">
                                    <?php echo $this->Form->control('desired_loan_amount_min',[
                                        'type' => 'select',
                                        'class' => 'form-control-sm w-100',
                                        'options' => ['' => '', '1000000' => '1百万円', '5000000' => '5百万円', '10000000' => '10百万円', '30000000' => '30百万円', '100000000' => '100百万円'],
                                        'label' => false,
                                        'default' => h($this->request->getSession()->read('param.desired_loan_amount_min'))]); ?>
                                </div>
                                <div class="tilde col-1 px-0">〜</div>
                                <div class="col-5 pl-0">
                                    <?php echo $this->Form->control('desired_loan_amount_max',[
                                        'type' => 'select',
                                        'class' => 'form-control-sm w-100',
                                        'options' => ['' => '', '100000000' => '100百万円', '30000000' => '30百万円', '10000000' => '10百万円', '5000000' => '5百万円', '1000000' => '1百万円'],
                                        'label' => false,
                                        'default' => h($this->request->getSession()->read('param.desired_loan_amount_max'))]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="loan-period pt-3">
                            <div class="loan-period-label">
                                <?php echo $this->Form->label('loan_period', __('融資期間')); ?>
                            </div>
                            <div class="loan-period-content row">
                                <div class="col-5 pr-2">
                                    <?php echo $this->Form->control('loan_period_min',[
                                        'type' => 'select',
                                        'class' => 'form-control-sm w-100',
                                        'options' => ['' => '', '1' => '1ヶ月', '3' => '3ヶ月', '6' => '6ヶ月', '12' => '12ヶ月 (1年)', '24' => '24ヶ月 (2年)', '36' => '36ヶ月 (3年)'],
                                        'label' => false,
                                        'default' => h($this->request->getSession()->read('param.loan_period_min'))]); ?>
                                </div>
                                <div class="tilde col-1 px-0">〜</div>
                                <div class="col-5 pl-0">
                                    <?php echo $this->Form->control('loan_period_max',[
                                        'type' => 'select',
                                        'class' => 'form-control-sm w-100',
                                        'options' => ['' => '', '36' => '36ヶ月 (3年)', '24' => '24ヶ月 (2年)', '12' => '12ヶ月 (1年)', '6' => '6ヶ月', '3' => '3ヶ月', '1' => '1ヶ月'],
                                        'label' => false,
                                        'default' => h($this->request->getSession()->read('param.loan_period_max'))]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="loan-purpose pt-3">
                            <div class="loan-purpose-label">
                                <?php echo $this->Form->label('loan_purpose', __('融資使途')); ?>
                            </div>
                            <div class="loan-purpose-content">
                                <?php echo $this->Form->select('loan_purpose',
                                    ['0' => '運転資金', '1' => '設備資金', '2' => 'その他'],
                                    ['multiple' => 'checkbox',
                                    'class' => 'checkbox',
                                    'val' => h($this->request->getSession()->read('param.loan_purpose'))]); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5 d-flex align-items-center justify-content-center">
                                <?php echo $this->Form->button(__('リセット'), ['type' => 'button', 'id' => 'reset', 'class' => 'button button-xs']); ?>
                            </div>
                            <div class="col-5 d-flex align-items-center justify-content-center">
                                <?php echo $this->Form->button(__('フィルター'), ['id' => 'submit', 'class' => 'button button-xs']); ?>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </fieldset>
            </div>
        </nav>

        <main class="page-content col-lg-9">
            <div class="contents-semi-label">
                ヒット件数: <?= $this->Paginator->param('count') ?>
            </div>
            <div class="sort-area">
                <?= $this->Form->create('', ['id' => 'sort_param', 'type' => 'get']);?>
                    <fieldset>
                        <div class="d-flex justify-content-end">
                            <?php
                                echo $this->Form->control('sort', [
                                    'type' => 'select',
                                    'options' => ['', 'release_desc' => '公開日が新しい順', 'view_desc' => '閲覧数が多い順', 'view_asc' => '閲覧数が少ない順'],
                                    'value' => $this->request->getSession()->read('param.sort'),
                                    'label' => __('ソート')]);
                            ?>
                        </div>
                    </fieldset>
                <?= $this->Form->end() ?>
            </div>
            <div class="item-area">
                <div class="row">
                    <?php foreach($loans as $loan) { ?>
                        <div class="loan-item col-lg-4 pb-2">
                            <a href="./view-detail?id=<?= $loan->id ?>">
                                <div class="thumbnail text-center">
                                    <!-- change picture -->
                                    <?php
                                        if(file_exists(LOAN_HEADER_DIR . $loan->user->id . PNG)) { ?>
                                            <img src="<?= '/'. LOAN_HEADER_DIR . $loan->user->id . PNG ?>" class="img-responsive w-100" />
                                    <?php
                                        } else { ?>
                                            <img src="<?= '/'. LOAN_HEADER_DIR . 'no_image' . PNG ?>" class="w-100" />
                                    <?php } ?>
                                </div>
                            </a>
                            <div class="row view-status text-right">
                                <div class="col-12 view-num">
                                    <i class="fas fa-eye"></i>
                                    <?= Cache::read($loan->id) === false ? 0 : Cache::read($loan->id) ?>
                                    <?php
                                        if (in_array($loan->id, $bank_favorite, true)) {
                                            echo '<i class="fas fa-star pl-2"></i>';
                                        } else {
                                            echo '<i class="far fa-star pl-2"></i>';
                                        }
                                    ?>
                                    <?= $loan->bank_favorite_count ?>
                                </div>
                            </div>
                            <a href="./view-detail?id=<?= $loan->id ?>">
                                <div class="row item-detail">
                                    <div class="col-12 title">
                                        <?= h($loan->loan_title) ?>
                                    </div>
                                    <div class="col-12 desired-loan">
                                        融資希望額: <?= h(round($loan->desired_loan_amount / 10000)) ?>万円
                                    </div>
                                </div>
                                <div class="row item-owner">
                                </div>
                                <div class="row three-columns text-center">
                                    <div class="col-4 pr-2">
                                        <div class="amount-label">
                                            <?= __('融資希望額') ?>
                                        </div>
                                        <div class="amount-value">
                                            <?= "¥".h(number_format($loan->desired_loan_amount)) ?>
                                        </div>
                                    </div>
                                    <div class="col-4 px-1">
                                        <div class="month-label">
                                            <?= __('融資期間') ?>
                                        </div>
                                        <div class="month-value">
                                            <?= h($loan->loan_period).'ヶ月' ?>
                                        </div>
                                    </div>
                                    <div class="col-4 pl-2">
                                        <div class="month-label">
                                            <?= __('使途') ?>
                                        </div>
                                        <div class="month-value">
                                            <?php
                                            if ($loan->loan_purpose == 0) { echo '運転資金'; }
                                            else if ($loan->loan_purpose == 1) { echo '設備資金'; }
                                            else if ($loan->loan_purpose == 2) { echo 'その他';  }  ?>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                </div>

                <div class="paginator row">
                    <ul class="pagination col-12 d-flex justify-content-center">
                        <?= $this->Paginator->first('<<') ?>
                        <?= $this->Paginator->prev('<') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next('>') ?>
                        <?= $this->Paginator->last('>>') ?>
                    </ul>
                </div>
            </div>
        </main>

    </div>
</div>