<?php

$loan = $entities['loan'];
$finance = $entities['finance'];

echo $this->Html->css('/scss/bank/view-detail.min.css');
echo '<script type="text/javascript">var var_loan_id = '.json_encode($loan->id).';</script>';
echo $this->Html->script('/js/bank/view_detail.min.js', ['block' => true]);

?>

<div class="contents-wrapper container view-detail">

    <div class="user-info">
        <div class="contents-semi-label">
            <?= __('会社情報') ?>
        </div>
        <div class="contents">
            <table>
                <tr class="row mx-0">
                    <th class="col-3"><?= __('会社名') ?></th>
                    <td class="col-9"><?= h($loan->user->company_name) ?></td>
                </tr>
                <tr class="row mx-0">
                    <th class="col-3"><?= __('URL') ?></th>
                    <td class="col-9"><a href=<?= $loan->user->url ?> target="_blank"><?= h($loan->user->url) ?></a></td>
                </tr>
                <tr class="row mx-0">
                    <th class="col-3"><?= __('所在地') ?></th>
                    <td class="col-9"><?= h($loan->user->company_address1) ?></td>
                </tr>
                <tr class="row mx-0">
                    <th class="col-3"><?= __('設立年月日') ?></th>
                    <td class="col-9"><?= h($loan->user->established_in) ?></td>
                </tr>
                <tr class="row mx-0">
                    <th class="col-3"><?= __('社員数') ?></th>
                    <td class="col-9"><?= h($loan->user->staff_count).' 人' ?></td>
                </tr>
                <tr class="row mx-0">
                    <th class="col-3 bottom"><?= __('担当者名') ?></th>
                    <td class="col-9 bottom"><?= h($loan->user->last_name." ".$loan->user->first_name." (".$loan->user->last_name_kana." ".$loan->user->first_name_kana.")") ?></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="loan-info mt-5">
        <div class="contents-semi-label">
            <?= __('案件詳細') ?>
        </div>
        <div class="contents">
            <table>
                <tbody>
                    <tr class="row mx-0">
                        <th class="col-3"><?= __('案件名') ?></th>
                        <td class="col-9"><?= h($loan->loan_title) ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3"><?= __('希望融資額') ?></th>
                        <td class="col-3 left"><?= number_format(h($loan->desired_loan_amount)).' 円' ?></td>
                        <th class="col-2"><?= __('希望融資期間') ?></th>
                        <td class="col-4"><?= h($loan->loan_period).' ヶ月' ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3"><?= __('資金使途') ?></th>
                        <?php if ($loan->loan_purpose == 2) { ?>
                            <td class="col-9"><?= nl2br($loan->loan_purpose_detail) ?></td>
                        <?php } else { ?>
                            <td class="col-9"><?= h($loan->loan_purpose_view) ?></td>
                        <?php } ?>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3"><?= __('業種') ?></th>
                        <td class="col-9"><?= h($loan->category->name."  >  ".$loan->sub_category->name) ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3"><?= __('事業内容') ?></th>
                        <td class="col-9"><?= nl2br(h($loan->job_description)) ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3 bottom"><?= __('追加コメント') ?></th>
                        <td class="col-9 bottom"><?= nl2br(h($loan->prospect)) ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="finance-info mt-5">
        <div class="contents-semi-label">
            <?= __('財務情報') ?>
        </div>
        <div class="contents">
            <table>
                <tbody>
                    <tr class="row mx-0">
                        <th class="col-3"><?= __('総資産') ?></th>
                        <td class="col-3 left"><?= number_format(h($finance->assets)).' 円' ?></td>
                        <th class="col-2"><?= __('融資残高') ?></th>
                        <td class="col-4"><?= number_format(h($finance->loan_balance)).' 円' ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-12"><?= h(array_keys($finance->revenues)[0]).'年'.h($finance->financial_month).'月期' ?></th>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3 pl-5">売上</th>
                        <td class="col-9"><?= h($finance->revenues[array_keys($finance->revenues)[0]]).' 円' ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3 pl-5">損益</th>
                        <td class="col-9"><?= h($finance->profits_losses[array_keys($finance->profits_losses)[0]]).' 円' ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-12"><?= h(array_keys($finance->revenues)[1]).'年'.h($finance->financial_month).'月期' ?></th>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3 pl-5">売上</th>
                        <td class="col-9"><?= h($finance->revenues[array_keys($finance->revenues)[1]]).' 円' ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3 pl-5">損益</th>
                        <td class="col-9"><?= h($finance->profits_losses[array_keys($finance->profits_losses)[1]]).' 円' ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-12"><?= h(array_keys($finance->revenues)[2]).'年'.h($finance->financial_month).'月期' ?></th>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3 pl-5">売上</th>
                        <td class="col-9"><?= h($finance->revenues[array_keys($finance->revenues)[2]]).' 円' ?></td>
                    </tr>
                    <tr class="row mx-0">
                        <th class="col-3 pl-5 bottom">損益</th>
                        <td class="col-9 bottom"><?= h($finance->profits_losses[array_keys($finance->profits_losses)[2]]).' 円' ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-6 text-right pr-5">
            <?php
                if ($loan->is_favorited) {
                    echo $this->Form->button(__('お気に入り登録解除'), ['type' => 'button', 'id' => 'fav_btn', 'class' => 'button button-md']);
                } else {
                    echo $this->Form->button(__('お気に入り登録'), ['type' => 'button', 'id' => 'fav_btn', 'class' => 'button button-md']);
                }
            ?>
        </div>
        <div class="col-6 text-left pl-5">
            <?php echo $this->Form->button(__('コンタクト開始'), ['class' => 'button button-md']); ?>
        </div>
    </div>
</div>