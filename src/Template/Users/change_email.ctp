<?php

echo $this->Html->css('/scss/users/profile.min.css');

?>
<div class="contents-wrapper container change-password">
    <div class="contents-label">
        <?= __('メールアドレス変更') ?>
    </div>
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create($user) ?>
    <fieldset>
        <div class="row py-3">
            <div class="offset-lg-1 col-lg-2 d-flex align-items-center justify-content-start">
                <?php echo $this->Form->label(__('email')); ?>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-start pl-5">
                <?php echo $this->Form->control('email', ['label' => false, 'required' => true]); ?>
            </div>
        </div>

    </fieldset>
    <div class="row">
        <div class="offset-lg-1 col-lg-11">
            <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), ['class' => 'button button-md']); ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>