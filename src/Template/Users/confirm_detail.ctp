<?php

echo $this->Html->css('/scss/users/register.min.css');

?>
<div class="contents-wrapper container register-detail-confirm">
<?= $this->Form->create($user); ?>
    <fieldset>
        <legend><?= __('入力情報確認') ?></legend>
        <?php
            foreach ($input_data as $key => $value):
                echo $this->Form->hidden($key, ['value' => $value]);
            endforeach;
            echo $this->Form->hidden('mode', ['value' => 'complete']);
        ?>
        <div align="center">
            <table>
                <?php if ($input_data['role'] === ROLE_CORP) { ?>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Company Name') ?></td>
                        <td><?= h($input_data['company_name']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Company Name Kana') ?></td>
                        <td><?= h($input_data['company_name_kana']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Established') ?></td>
                        <td><?= h($input_data['established_in']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __('代表取締役姓') ?></td>
                        <td><?= h($input_data['president_last_name']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __('代表取締役名') ?></td>
                        <td><?= h($input_data['president_first_name']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __('代表取締役姓 ふりがな') ?></td>
                        <td><?= h($input_data['president_last_name_kana']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __('代表取締役名 ふりがな') ?></td>
                        <td><?= h($input_data['president_first_name_kana']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Staff count') ?></td>
                        <td><?= h($input_data['staff_count']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'URL') ?></td>
                        <td><?= h($input_data['url']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Company ZipCode') ?></td>
                        <td><?= h($input_data['company_zipcode']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Prefecture') ?></td>
                        <td><?= h($input_data['company_address1']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'City Ward') ?></td>
                        <td><?= h($input_data['company_address2']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Building') ?></td>
                        <td><?= h($input_data['company_address3']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Company Phone') ?></td>
                        <td><?= h($input_data['company_phone']) ?></td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Bank Name') ?></td>
                        <td><?= h($input_data['bank_name']) ?></td>
                    </tr>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Bank Branch Name') ?></td>
                        <td><?= h($input_data['bank_branch_name']) ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td><?= __d('CakeDC/Users', 'Staff Last Name') ?></td>
                    <td><?= h($input_data['last_name']) ?></td>
                </tr>
                <tr>
                    <td><?= __d('CakeDC/Users', 'Staff First Name') ?></td>
                    <td><?= h($input_data['first_name']) ?></td>
                </tr>
                <tr>
                    <td><?= __d('CakeDC/Users', 'Last Name Kana') ?></td>
                    <td><?= h($input_data['last_name_kana']) ?></td>
                </tr>
                <tr>
                    <td><?= __d('CakeDC/Users', 'First Name Kana') ?></td>
                    <td><?= h($input_data['first_name_kana']) ?></td>
                </tr>
                <?php if ($input_data['role'] === ROLE_BANK) { ?>
                    <tr>
                        <td><?= __d('CakeDC/Users', 'Position') ?></td>
                        <td><?= h($input_data['position']) ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td><?= __d('CakeDC/Users', 'Phone') ?></td>
                    <td><?= h($input_data['phone']) ?></td>
                </tr>
            </table>
        </div>
    </fieldset>
    <div class="row">
        <div class="col-6 text-right pr-5">
            <input type="button" onclick="history.back()"  value="戻る" class="button button-md" id="back" />
        </div>
        <div class="col-6 text-left pl-5">
            <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), ['class' => 'button button-md']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>