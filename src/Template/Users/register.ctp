<?php
use Cake\Core\Configure;

echo $this->Html->css('/scss/users/register.min.css');

?>
<div class="contents-wrapper container register-contents">
    <?= $this->Form->create($user); ?>
    <div class="contents-label">
        <?= __d('CakeDC/Users', 'Add User') ?>
    </div>
    <fieldset>
        <div class="row py-3">
            <div class="offset-lg-4 col-lg-2 d-flex align-items-center justify-content-start">
                <?php echo $this->Form->label(__d('CakeDC/Users', 'Email')); ?>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-start pl-5">
                <?php echo $this->Form->control('email', ['label' => false, 'id' => 'email-input']); ?>
            </div>
        </div>
        <div class="row py-3">
            <div class="offset-lg-4 col-lg-2 d-flex align-items-center justify-content-start">
                <?php echo $this->Form->label(__d('CakeDC/Users', 'Password')); ?>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-start pl-5">
                <?php echo $this->Form->control('password', ['label' => false, 'id' => 'password-input', 'class' => 'd-flex justify-content-start']); ?>
            </div>
        </div>
        <div class="row py-3">
            <div class="offset-lg-4 col-lg-2 d-flex align-items-center justify-content-start">
                <?php echo $this->Form->label(__d('CakeDC/Users', 'Confirm password')); ?>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-start pl-5">
                <?php echo $this->Form->control('password_confirm', ['type' => 'password', 'label' => false]); ?>
            </div>
        </div>
        <div class="row py-3">
            <div class="offset-lg-4 col-lg-1 d-flex align-items-center justify-content-end">
                <?php echo $this->Form->control('tos', ['type' => 'checkbox', 'label' => false, 'required' => true]); ?>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-start pl-0">
                <label class="mb-0" for="tos"><a href="../pages/terms-of-service">利用規約</a>・<a href="../pages/privacy">個人情報保護方針</a>・<a href="../pages/nda">秘密保持契約</a>に同意する</label>
            </div>
        </div>
        <div class="row pb-3">
            <div class="offset-lg-4 col-lg-1 d-flex align-items-center justify-content-end">
                <?php echo $this->Form->control('mail_magazine', ['type' => 'checkbox', 'label' => false, 'required' => false]); ?>
            </div>
            <div class="col-lg-6 d-flex align-items-center justify-content-start pl-0">
                <label class="mb-0" for="mail-magazine">メールマガジンに登録する</label>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), ['class' => 'button button-md']) ?>
    <?= $this->Form->end() ?>
</div>
