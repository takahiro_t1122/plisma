<?php
use Cake\Core\Configure;

echo $this->Html->css('/scss/users/register.min.css');
echo $this->Html->script('/js/users/register_detail.min.js', ['block' => true]);

?>
<div class="contents-wrapper container register-contents">
    <?= $this->Form->create($user); ?>
    <fieldset>
        <legend><?= __('ユーザー種別選択') ?></legend>
        <?php
            echo $this->Form->label(__('あなたは企業ですか。銀行ですか。'));
            echo $this->Form->control('role', [
                'label' => false,
                'options' => [
                    'corp' => __d('CakeDC/Users', 'Corporation'),
                    'bank' => __d('CakeDC/Users', 'Bank')],
                'type' => 'radio',
                'class' => 'register-type',
                'required' => true
            ]);
            ?>
    </fieldset>
    <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), ['class' => 'button button-md']) ?>
    <?= $this->Form->end() ?>
</div>