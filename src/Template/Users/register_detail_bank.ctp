<?php

// jQuery UI
echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js');
echo $this->Html->css('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css');
echo $this->Html->css('/scss/users/register.min.css');
?>

<div class="contents-wrapper container register-detail-contents">
    <div class="contents-label">
        <?= __d('CakeDC/Users', 'Add Detail of Bank user') ?>
    </div>
    <?= $this->Form->create($user); ?>
    <fieldset>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('bank_id', [
                    'type' => 'select',
                    'options' => $bank_list,
                    'value' => h($this->request->getSession()->read('inputData.bank_name')),
                    'label' => __d('CakeDC/Users', 'Bank Name'),
                    'id' => 'combobox']);
                ?>
            </div>
            <div class="col-6 my-3">
                <?php echo $this->Form->control('bank_branch_name', [
                    'type' => 'text',
                    'value' => h($this->request->getSession()->read('inputData.bank_branch_name')),
                    'label' => __d('CakeDC/Users', 'Bank Branch Name'),
                    'placeholder' => '融資支店']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('last_name', [
                    'type' => 'text',
                    'value' => h($this->request->getSession()->read('inputData.last_name')),
                    'label' => __d('CakeDC/Users', 'Staff Last Name'),
                    'placeholder' => '山田']);
                ?>
            </div>
            <div class="col-6 my-3">
                <?php echo $this->Form->control('first_name', [
                    'type' => 'text',
                    'value' => h($this->request->getSession()->read('inputData.first_name')),
                    'label' => __d('CakeDC/Users', 'Staff First Name'),
                    'placeholder' => '太郎']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('last_name_kana', [
                    'type' => 'text',
                    'value' => h($this->request->getSession()->read('inputData.last_name_kana')),
                    'label' => __d('CakeDC/Users', 'Last Name Kana'),
                    'placeholder' => 'やまだ']);
                ?>
            </div>
            <div class="col-6 my-3">
                <?php echo $this->Form->control('first_name_kana', [
                    'type' => 'text',
                    'value' => h($this->request->getSession()->read('inputData.first_name_kana')),
                    'label' => __d('CakeDC/Users', 'First Name Kana'),
                    'placeholder' => 'たろう']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-12 my-3 pl-3">
                <?php echo $this->Form->control('position', [
                    'type' => 'text',
                    'value' => h($this->request->getSession()->read('inputData.position')),
                    'label' => __d('CakeDC/Users', 'Position'),
                    'placeholder' => '課長代理']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-12 my-3 pl-3">
                <?php echo $this->Form->control('phone', [
                    'type' => 'text',
                    'value' => h($this->request->getSession()->read('inputData.phone')),
                    'label' => __d('CakeDC/Users', 'Phone'),
                    'placeholder' => '03-1234-5678']);
                ?>
            </div>
        </div>
        <?php echo $this->Form->hidden('mode', ['value' => 'confirm']); ?>
    </fieldset>
    <div class="text-center">
        <?= $this->Form->button(__('次へ'),  ['class' => 'button button-md']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>

<script>
!function($){$.widget("ui.combobox",{_create:function(){var input,that=this,select=this.element.hide(),selected=select.children(":selected"),value=selected.val()?selected.text():"",wrapper=this.wrapper=$("<span>").addClass("ui-combobox").insertAfter(select);function removeIfInvalid(element){var value=$(element).val(),matcher=new RegExp("^"+$.ui.autocomplete.escapeRegex(value)+"$","i"),valid=!1;select.children("option").each(function(){if($(this).text().match(matcher))return this.selected=valid=!0,!1})}(input=$("<input>").appendTo(wrapper).val(value).attr("title","").addClass("ui-state-default ui-combobox-input").autocomplete({delay:0,minLength:0,source:function(request,response){var matcher=new RegExp($.ui.autocomplete.escapeRegex(request.term),"i");response(select.children("option").map(function(){var text=$(this).text();if(this.value&&(!request.term||matcher.test(text)))return{label:text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+$.ui.autocomplete.escapeRegex(request.term)+")(?![^<>]*>)(?![^&;]+;)","gi"),"$1"),value:text,option:this}}))},select:function(event,ui){ui.item.option.selected=!0,that._trigger("selected",event,{item:ui.item.option})},change:function(event,ui){if(!ui.item)return removeIfInvalid(this)}}).addClass("ui-widget ui-widget-content ui-corner-left")).data("ui-autocomplete")._renderItem=function(ul,item){return $("<li>").data("ui-autocomplete-item",item).append("<a>"+item.label+"</a>").appendTo(ul)}},destroy:function(){this.wrapper.remove(),this.element.show(),$.Widget.prototype.destroy.call(this)}})}(jQuery),$(function(){$("#combobox").combobox(),$("#toggle").click(function(){$("#combobox").toggle()}).button()});
</script>