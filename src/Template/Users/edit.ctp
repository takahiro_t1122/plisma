<?php

use Cake\Core\Configure;

$Users = ${$tableAlias};

echo $this->Html->css('/scss/users/register.min.css');
if ($Users['role'] == ROLE_CORP) echo $this->Html->script('https://yubinbango.github.io/yubinbango/yubinbango.js');
?>

<div class="contents-wrapper container register-detail-contents h-adr">
    <div class="contents-label">
        <?= __d('CakeDC/Users', 'Edit User') ?>
    </div>
    <?= $this->Form->create($Users); ?>
    <span class="p-country-name" style="display:none;">Japan</span>
    <fieldset>
        <?php if (h($Users->role) == ROLE_CORP) { ?>
            <div class="row mx-5">
                <div class="col-6 pl-3">
                    <?php echo $this->Form->control('company_name', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Company Name')]);
                    ?>
                </div>
                <div class="col-6">
                    <?php echo $this->Form->control('company_name_kana', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Company Name Kana')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-12 my-3 pl-3">
                    <?php echo $this->Form->control('established_in', [
                        'type' => 'date',
                        'dateFormat' => 'YMD',
                        'monthNames' => false,
                        'empty' => ['year' => __d('CakeDC/Users', 'Year'),
                                    'month' => __d('CakeDC/Users', 'Month'),
                                    'day' => __d('CakeDC/Users', 'Day')],
                        'year' => ['start' => 1930,
                                    'end' => date('Y')],
                        'label' => __d('CakeDC/Users', 'Established'),
                        'disabled' => 'disabled']);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('president_last_name', [
                        'type' => 'text',
                        'value' => h($this->request->session()->read('inputData.president_last_name')),
                        'label' => __('代表取締役姓')]);
                    ?>
                </div>
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('president_first_name', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.president_first_name')),
                    'label' => __('代表取締役名')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('president_last_name_kana', [
                        'type' => 'text',
                        'value' => h($this->request->session()->read('inputData.president_last_name_kana')),
                        'label' => __('代表取締役姓 ふりがな')]);
                    ?>
                </div>
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('president_first_name_kana', [
                        'type' => 'text',
                        'value' => h($this->request->session()->read('inputData.president_first_name_kana')),
                        'label' => __('代表取締役名 ふりがな')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-12 my-3 pl-3">
                    <?php echo $this->Form->control('staff_count', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Staff count')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-12 my-3 pl-3">
                    <?php echo $this->Form->control('url', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'URL')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('company_zipcode', [
                        'type' => 'text',
                        'class' => 'p-postal-code',
                        'label' => __d('CakeDC/Users', 'Company ZipCode')]);
                    ?>
                </div>
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('company_address1', [
                        'type' => 'text',
                        'class' => 'p-region',
                        'label' => __d('CakeDC/Users', 'Prefecture')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('company_address2', [
                        'type' => 'text',
                        'class' => 'p-locality p-street-address p-extended-address',
                        'label' => __d('CakeDC/Users', 'City Ward')]);
                    ?>
                </div>
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('company_address3', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Building')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('company_phone', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Company Phone')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('last_name', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Staff Last Name')]);
                    ?>
                </div>
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('first_name', [
                    'type' => 'text',
                    'label' => __d('CakeDC/Users', 'Staff First Name')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('last_name_kana', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Last Name Kana')]);
                    ?>
                </div>
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('first_name_kana', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'First Name Kana')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('phone', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Phone')]);
                    ?>
                </div>
            </div>
        <?php } else if (h($Users->role) == ROLE_BANK) { ?>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('bank_name', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Bank Name'),
                        'disabled' => 'disabled']);
                    ?>
                </div>
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('bank_branch_name', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Bank Branch Name')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('last_name', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Staff Last Name')]);
                    ?>
                </div>
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('first_name', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Staff First Name')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('last_name_kana', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Last Name Kana')]);
                    ?>
                </div>
                <div class="col-6 my-3">
                    <?php echo $this->Form->control('first_name_kana', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'First Name Kana')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('position', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Position')]);
                    ?>
                </div>
            </div>
            <div class="row mx-5">
                <div class="col-6 my-3 pl-3">
                    <?php echo $this->Form->control('phone', [
                        'type' => 'text',
                        'label' => __d('CakeDC/Users', 'Phone')]);
                    ?>
                </div>
            </div>
        <?php } ?>
    </fieldset>
    <div class="row">
        <div class="col-6 text-right pr-5">
            <input type="button" onclick="history.back()"  value="戻る" class="button button-md" id="back" />
        </div>
        <div class="col-6 text-left pl-5">
            <?= $this->Form->button(__('更新'), ['class' => 'button button-md']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>