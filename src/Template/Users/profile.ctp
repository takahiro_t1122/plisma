<?php

echo $this->Html->css('/scss/users/profile.min.css');

?>

<div class="contents-wrapper container profile">
    <div class="contents-label">
        <?= __('プロフィール') ?>
    </div>
    <div class="profile-contents row">
        <?php if (h($user->role) == ROLE_BANK) { ?>
            <div class="occupation col-12">
                <?= $user->bank->bank_name.' '.$user->bank_branch_name ?>
            </div>
            <div class="name col-12">
                <?= $user->position.' '.$user->last_name.' '.$user->first_name ?>
            </div>
            <div class="phone col-12">
                <?= $user->phone ?>
            </div>
            <div class="email col-12">
                <?= $user->email ?>
            </div>
        <?php } else if (h($user->role) == ROLE_CORP) { ?>
            <div class="occupation col-12">
                <?= $user->company_name ?>
            </div>
            <div class="url col-12">
                <a href=<?= $user->url ?> target="_blank"><?= $user->url ?></a>
            </div>
            <div class="zip col-12">
                <?= '〒'. substr($user->company_zipcode, 0, 3).'-'.substr($user->company_zipcode, 3) ?></a>
            </div>
            <div class="address col-12">
                <?= $user->company_address1.$user->company_address2.$user->company_address3 ?></a>
            </div>
            <div class="establish col-12">
                <?= '設立:　'.$user->established_in?>
            </div>
            <div class="name col-12">
                <?= $user->last_name.' '.$user->first_name ?>
            </div>
            <div class="phone col-12">
                <?= $user->phone ?>
            </div>

        <?php } ?>
    </div>

</div>

<li><?= $this->Html->link(__('Change Profile'), ['controller' => 'Users', 'action' => 'edit', $user->id]); ?></li>
<?php if (h($user->role) == ROLE_CORP) { ?>
    <li><?= $this->Form->postLink(__('Delete User'), ['controller' => 'Users', 'action' => 'delete', $user->id], ['confirm' => '削除してもよろしいでしょうか?']); ?></li>
<?php } ?>
