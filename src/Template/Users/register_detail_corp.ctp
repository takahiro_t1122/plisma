<?php

use Cake\Core\Configure;

header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

echo $this->Html->script('https://yubinbango.github.io/yubinbango/yubinbango.js');
echo $this->Html->css('/scss/users/register.min.css');
?>

<div class="contents-wrapper container h-adr register-detail-contents">
    <div class="contents-label">
        <?= __d('CakeDC/Users', 'Add Detail of Corporation user') ?>
    </div>
    <?= $this->Form->create($user); ?>
    <span class="p-country-name" style="display:none;">Japan</span>
    <fieldset>
        <div class="row mx-5">
            <div class="col-6 pl-3">
                <?php echo $this->Form->control('company_name', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.company_name')),
                    'label' => __d('CakeDC/Users', 'Company Name'),
                    'placeholder' => 'Sample株式会社']);
                ?>
            </div>
            <div class="col-6">
                <?php echo $this->Form->control('company_name_kana', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.company_name_kana')),
                    'label' => __d('CakeDC/Users', 'Company Name Kana'),
                    'placeholder' => 'さんぷるかぶしきがいしゃ']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('established_in', [
                    'type' => 'date',
                    'dateFormat' => 'YMD',
                    'monthNames' => false,
                    'empty' => ['year' => __d('CakeDC/Users', 'Year'),
                                'month' => __d('CakeDC/Users', 'Month'),
                                'day' => __d('CakeDC/Users', 'Day')],
                    'year' => ['start' => 1930,
                                'end' => date('Y')],
                    'value' => h($this->request->session()->read('inputData.established_in')),
                    'label' => __d('CakeDC/Users', 'Established')]);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('president_last_name', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.president_last_name')),
                    'label' => __('代表取締役姓'),
                    'placeholder' => '山田']);
                ?>
            </div>
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('president_first_name', [
                'type' => 'text',
                'value' => h($this->request->session()->read('inputData.president_first_name')),
                'label' => __('代表取締役名'),
                'placeholder' => '太郎']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3">
                <?php echo $this->Form->control('president_last_name_kana', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.president_last_name_kana')),
                    'label' => __('代表取締役姓 ふりがな'),
                    'placeholder' => 'やまだ']);
                ?>
            </div>
            <div class="col-6 my-3">
                <?php echo $this->Form->control('president_first_name_kana', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.president_first_name_kana')),
                    'label' => __('代表取締役名 ふりがな'),
                    'placeholder' => 'たろう']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('staff_count', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.staff_count')),
                    'label' => __d('CakeDC/Users', 'Staff count'),
                    'placeholder' => '50']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-12 my-3 pl-3">
                <?php echo $this->Form->control('url', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.url')),
                    'label' => __d('CakeDC/Users', 'URL'),
                    'placeholder' => 'http://example.com']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('company_zipcode', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.company_zipcode')),
                    'class' => 'p-postal-code',
                    'label' => __d('CakeDC/Users', 'Company ZipCode'),
                    'placeholder' => '1000001']);
                ?>
            </div>
            <div class="col-6 my-3">
                <?php echo $this->Form->control('company_address1', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.company_address1')),
                    'class' => 'p-region',
                    'label' => __d('CakeDC/Users', 'Prefecture'),
                    'placeholder' => '東京都']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('company_address2', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.company_address2')),
                    'class' => 'p-locality p-street-address p-extended-address',
                    'label' => __d('CakeDC/Users', 'City Ward'),
                    'placeholder' => '千代田区千代田 1-1']);
                ?>
            </div>
            <div class="col-6 my-3">
                <?php echo $this->Form->control('company_address3', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.company_address3')),
                    'label' => __d('CakeDC/Users', 'Building'),
                    'placeholder' => '日本ビルディング 105']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('company_phone', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.company_phone')),
                    'label' => __d('CakeDC/Users', 'Company Phone'),
                    'placeholder' => '03-1234-5678']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('last_name', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.last_name')),
                    'label' => __d('CakeDC/Users', 'Staff Last Name'),
                    'placeholder' => '山田']);
                ?>
            </div>
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('first_name', [
                'type' => 'text',
                'value' => h($this->request->session()->read('inputData.first_name')),
                'label' => __d('CakeDC/Users', 'Staff First Name'),
                'placeholder' => '太郎']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3">
                <?php echo $this->Form->control('last_name_kana', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.last_name_kana')),
                    'label' => __d('CakeDC/Users', 'Last Name Kana'),
                    'placeholder' => 'やまだ']);
                ?>
            </div>
            <div class="col-6 my-3">
                <?php echo $this->Form->control('first_name_kana', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.first_name_kana')),
                    'label' => __d('CakeDC/Users', 'First Name Kana'),
                    'placeholder' => 'たろう']);
                ?>
            </div>
        </div>
        <div class="row mx-5">
            <div class="col-6 my-3 pl-3">
                <?php echo $this->Form->control('phone', [
                    'type' => 'text',
                    'value' => h($this->request->session()->read('inputData.phone')),
                    'label' => __d('CakeDC/Users', 'Phone'),
                    'placeholder' => '03-1234-5678']);
                ?>
            </div>
        </div>
        <?php echo $this->Form->hidden('mode', ['value' => 'confirm']); ?>
    </fieldset>
    <div class="text-center">
        <?= $this->Form->button(__d('CakeDC/Users', 'Next'), ['class' => 'button button-md']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
