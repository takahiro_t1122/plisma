<?php

use Cake\Core\Configure;

?>
<div class="contents-wrapper container additional-info-registered">
    <div class="contents-label text-center">
        <?= __('ご登録ありがとうございます。') ?>
    </div>
    <div class="complete-contents text-center">
        <p>ご入力ありがとうございました。<br />
        弊社にてご入力を頂いた内容と契約時の情報を確認し、<br />
        入金情報が確認できましたらご登録のメールアドレスに利用開始可能のご連絡を差し上げます。</p>

        <p>ご契約いただいていない場合は、弊社から折返し連絡させていただきます。</p>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <input type="button" onClick="location.href='/'" value="TOPへ戻る" class="button button-md" />
        </div>
    </div>
</div>