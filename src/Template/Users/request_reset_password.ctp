<div class="contents-wrapper container reset-password text-center">
    <div class="contents-label">
        <?= __('パスワード再設定') ?>
    </div>
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create('User') ?>
    <fieldset>
        <?= $this->Form->control('reference',['label' => false, 'placeholder' => 'メールアドレス']) ?>
        <?= $this->Form->label(__('メールアドレスを入力してください。')) ?>
    </fieldset>
    <div class="row">
        <div class="col-12">
            <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), ['class' => 'button button-sm']); ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
