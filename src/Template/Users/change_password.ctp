<?php

echo $this->Html->css('/scss/users/profile.min.css');

?>
<div class="contents-wrapper container change-password">
    <div class="contents-label">
        <?= __d('CakeDC/Users', 'Please enter the new password') ?>
    </div>
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create($user) ?>
    <fieldset>
        <?php if ($validatePassword) : ?>
            <div class="row py-3">
                <div class="offset-1 col-2 d-flex align-items-center justify-content-start">
                    <?php echo $this->Form->label(__d('CakeDC/Users', 'Current password')); ?>
                </div>
                <div class="col-9 d-flex align-items-center justify-content-start pl-5">
                    <?= $this->Form->control('current_password', ['type' => 'password', 'required' => true, 'label' => false]);
                    ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row py-3">
            <div class="offset-1 col-2 d-flex align-items-center justify-content-start">
                <?php echo $this->Form->label(__d('CakeDC/Users', 'New password')); ?>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-start pl-5">
                <?php echo $this->Form->control('password', ['label' => false, 'id' => 'password-input']); ?>
            </div>
        </div>
        <div class="row py-3">
            <div class="offset-1 col-2 d-flex align-items-center justify-content-start">
                <?php echo $this->Form->label(__d('CakeDC/Users', 'Confirm password')); ?>
            </div>
            <div class="col-6 d-flex align-items-center justify-content-start pl-5">
                <?php echo $this->Form->control('password_confirm', ['type' => 'password', 'label' => false]); ?>
            </div>
        </div>

    </fieldset>
    <div class="row">
        <div class="offset-1 col-11">
            <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), ['class' => 'button button-md']); ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>