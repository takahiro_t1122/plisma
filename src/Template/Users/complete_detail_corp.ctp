<?php

use Cake\Core\Configure;

?>
<div class="contents-wrapper container additional-info-registered">
    <div class="contents-label text-center">
        <?= __('ご登録ありがとうございます。') ?>
    </div>
    <div class="complete-contents text-center">
        <p>登録手続きは完了致しました。</p>
        <p>早速案件を登録してみましょう。<br />
        「案件登録へ」をクリックして融資希望条件を入力してください。</p>
    </div>
    <div class="row">
        <div class="col-6 text-right">
            <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'Corp', 'action' => 'createLoanItem']); ?>'" value="案件登録へ" class="button button-md" />
        </div>
        <div class="col-6 text-left">
            <input type="button" onClick="location.href='/'" value="TOPへ戻る" class="button button-md" />
        </div>
    </div>
</div>