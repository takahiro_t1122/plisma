<?php

echo $this->Html->css('/scss/corp/create-loan.min.css');

//ToDo change
$header_img = LOAN_HEADER_DIR . $loanData['user_id'] . PNG;
$header_img_suffix = $header_img . '?' . date('YmdHis');
?>

<div class="contents-wrapper container confirm-loan">
<?= $this->Form->create(); ?>
    <fieldset>
        <legend><?= __('入力情報確認') ?></legend>
        <?php
            foreach ($loanData as $key => $value):
                echo $this->Form->hidden($key, ['value' => $value]);
            endforeach;
            echo $this->Form->hidden('mode', ['value' => 'complete']);
        ?>
        <div align="center">
            <table>
                <tbody>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('案件名') ?></td>
                        <td class="col-8"><?= h($loanData['loan_title']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('案件表示画像') ?></td>
                        <td class="col-8">
                            <?php if (file_exists($header_img)) {
                                echo $this->Html->image('../' . $header_img_suffix, ['class' => 'w-50']);
                            } else {
                                ;
                            } ?>
                        </td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('融資希望額') ?></td>
                        <td class="col-8"><?= h($loanData['desired_loan_amount']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('希望融資期間') ?></td>
                        <td class="col-8"><?= h($loanData['loan_period']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('資金使途') ?></td>
                        <td class="col-8"><?= h($loanData['loan_purpose_view']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('その他の場合') ?></td>
                        <td class="col-8"><?= h($loanData['loan_purpose_detail']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('事業業種') ?></td>
                        <td class="col-8"><?= h($loanData['category_view']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('業種詳細') ?></td>
                        <td class="col-8"><?= h($loanData['sub_category_view']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('事業内容') ?></td>
                        <td class="col-8"><?= nl2br(h($loanData['job_description'])) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('追加コメント') ?></td>
                        <td class="col-8"><?= nl2br(h($loanData['prospect'])) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('総資産') ?></td>
                        <td class="col-8"><?= h($loanData['assets']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('融資残高') ?></td>
                        <td class="col-8"><?= h($loanData['loan_balance']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('決算月') ?></td>
                        <td class="col-8"><?= h($loanData['financial_month']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('決算期①') ?></td>
                        <td class="col-8"><?= h($loanData['fiscal_year_one']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('売上高') ?></td>
                        <td class="col-8"><?= h($loanData['revenue_one']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('当期損益') ?></td>
                        <td class="col-8"><?= h($loanData['profits_losses_one']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('決算期②') ?></td>
                        <td class="col-8"><?= h($loanData['fiscal_year_two']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('売上高') ?></td>
                        <td class="col-8"><?= h($loanData['revenue_two']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('当期損益') ?></td>
                        <td class="col-8"><?= h($loanData['profits_losses_two']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('決算期③') ?></td>
                        <td class="col-8"><?= h($loanData['fiscal_year_three']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('売上高') ?></td>
                        <td class="col-8"><?= h($loanData['revenue_three']) ?></td>
                    </tr>
                    <tr class="row py-1">
                        <th class="offset-2 col-2"><?= __('当期損益') ?></td>
                        <td class="col-8"><?= h($loanData['profits_losses_three']) ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </fieldset>
    <div class="row">
        <div class="col-6 text-right pr-5">
            <input type="button" onclick="history.back()"  value="戻る" class="button button-md" id="back" />
        </div>
        <div class="col-6 text-left pl-5">
            <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), ['class' => 'button button-md']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>