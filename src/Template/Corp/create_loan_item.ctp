<?php
session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

//ToDo
$header_img = LOAN_HEADER_DIR . $loginId . PNG;
$header_img_suffix = $header_img . '?' . date('YmdHis');
$header_no_img = LOAN_HEADER_DIR . 'no_image' . PNG;

echo $this->Html->css('/scss/corp/create-loan.min.css');
echo $this->Html->script('/js/corp/create_loan_item.min.js', ['block' => true]);
echo '<script type="text/javascript">var header_img_var = '.json_encode($header_img_suffix).';</script>';
echo $this->Html->script('/js/corp/upload_loan_image.min.js', ['block' => true]);

?>

<div class="contents-wrapper container create-loan">
    <div class="contents-label">
        <?= __('融資案件登録') ?>
    </div>
    <?= $this->Form->create($form); ?>
    <fieldset>
        <div class="contents-semi-label row mx-lg-3">
            <?= __('事業情報') ?>
        </div>
        <div class="row m-lg-3">
            <div class="col-lg-12">
                <?php echo $this->Form->label('loan_title', __('案件名')); ?>
            </div>
            <div class="col-lg-12">
                <?php echo $this->Form->control('loan_title', [
                    'label' => false,
                    'placeholder' => '〇〇対応に向けた設備投資',
                    'value' => h($this->request->getSession()->read('loanData.loan_title'))]); ?>
            </div>
        </div>
        <div class="row m-lg-3">
            <div class="col-lg-6">
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('desired_loan_amount', __('融資希望額')); ?>
                    </div>
                    <div class="col-lg-12">
                        <?php echo $this->Form->control('desired_loan_amount', [
                            'label' => false,
                            'placeholder' => '1,000,000',
                            'value' => h($this->request->getSession()->read('loanData.desired_loan_amount'))]);?>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('loan_period', __('希望融資期間')); ?>
                    </div>
                    <div class="col-lg-12">
                        <?php echo $this->Form->control('loan_period', [
                            'label' => false,
                            'placeholder' => '36',
                            'value' => h($this->request->getSession()->read('loanData.loan_period'))]);?>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('loan_purpose', __('資金使途')); ?>
                    </div>
                    <div class="col-lg-12">
                        <?php echo $this->Form->control('loan_purpose', [
                        'type' => 'select',
                        'class' => 'form-control-md',
                        'options' => ['' => '', '0' => '運転資金', '1' => '設備資金', '2' => 'その他'],
                        'label' => false,
                        'value' => h($this->request->getSession()->read('loanData.loan_purpose'))]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('loan_purpose_detail', __('その他の場合')); ?>
                    </div>
                    <div class="col-lg-12">
                        <?php echo $this->Form->control('loan_purpose_detail', [
                            'label' => false,
                            'placeholder' => '資金使途を記載',
                            'value' => h($this->request->getSession()->read('loanData.loan_purpose_detail'))]);?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('loan_header_img', __('案件登録画像')); ?>
                    </div>
                    <div class="col-lg-12 pb-3" id="loan-header-img">
                        <?php
                            if (file_exists($header_img)) {
                                echo $this->Html->image('../'.$header_img_suffix, ['class' => 'w-100']);
                            } else {
                                echo $this->Html->image('../'.$header_no_img, ['class' => 'w-100']);
                            }
                        ?>
                    </div>
                    <div class="col-lg-12 px-3 d-flex align-items-center">
                        <?= $this->Html->link("登録", ['controller' => 'Corp', 'action' => 'uploadLoanImage'], ['class' => 'text-center my-1 button button-xs button-gray py-1 px-2 mr-2', 'id' => 'upload-link']);?>
                        <?= $this->Html->link("削除", ['controller' => 'Corp', 'action' => 'deleteLoanImage', 'loginId' => h($loginId)], ['class' => 'text-center my-1 button button-xs button-gray py-1 px-2 mr-2']);?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-lg-3">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('category', __('事業業種')); ?>
                    </div>
                    <div class="col-lg-12">
                        <?php echo $this->Form->control('category', [
                            'type' => 'select',
                            'class' => 'form-control-md',
                            'options' => $category_list,
                            'label' => false,
                            'value' => h($this->request->getSession()->read('loanData.category'))]); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('sub_category', __('業種詳細')); ?>
                    </div>
                    <div class="col-lg-12">
                        <?php echo $this->Form->control('sub_category', [
                        'type' => 'select',
                        'class' => 'form-control-md',
                        'label' => false,
                        'options' => [
                            $this->request->getSession()->read('loanData.sub_category') =>
                            $this->request->getSession()->read('loanData.sub_category_name')],
                        'default' => h($this->request->getSession()->read('loanData.sub_category'))]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-lg-3">
            <div class="col-lg-12">
                <?php echo $this->Form->label('job_description', __('事業内容')); ?>
            </div>
            <div class="col-lg-12">
                <?php echo $this->Form->control('job_description', [
                    'type' => 'textarea',
                    'escape' => true,
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.job_description'))]); ?>
            </div>
        </div>
        <div class="row m-lg-3">
            <div class="col-lg-12">
                <?php echo $this->Form->label('prospect', __('追加コメント')); ?>
            </div>
            <div class="col-lg-12">
                <?php echo $this->Form->control('prospect', [
                    'type' => 'textarea',
                    'escape' => true,
                    'label' => false,
                    'placeholder' => '融資依頼の背景、今後の展望等',
                    'value' => h($this->request->getSession()->read('loanData.prospect'))]); ?>
            </div>
        </div>
        <div class="contents-semi-label row mx-lg-3">
            <?= __('財務情報(三期分を含む)') ?>
        </div>
        <div class="row m-lg-3">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('assets', __('総資産')); ?>
                    </div>
                    <div class="col-lg-12">
                        <?php echo $this->Form->control('assets', [
                        'label' => false,
                        'placeholder' => '10,000,000',
                        'value' => h($this->request->getSession()->read('loanData.assets'))]); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <?php echo $this->Form->label('loan_balance', __('融資残高')); ?>
                    </div>
                    <div class="col-lg-12">
                        <?php echo $this->Form->control('loan_balance', [
                            'label' => false,
                            'placeholder' => '5,000,000',
                            'value' => h($this->request->getSession()->read('loanData.loan_balance'))]);?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-lg-3">
            <div class="col-lg-12">
                <?php echo $this->Form->label('financial_month', __('決算月')); ?>
            </div>
            <div class="col-lg-12">
                <?php echo $this->Form->control('financial_month', [
                    'type' => 'month',
                    'class' => 'form-control-md',
                    'empty' => ['month' => __('月')],
                    'label' => false,
                    'monthNames' => false,
                    'value' => h($this->request->getSession()->read('loanData.financial_month'))]); ?>
            </div>
        </div>
        <div class="row mx-lg-3 text-center">
            <div class="col-lg-2">
                <?php echo $this->Form->label('fiscal_year', __('決算期')); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('fiscal_year_one', [
                    'type' => 'year',
                    'class' => 'form-control-md',
                    'empty' => ['year' => __('年')],
                    'year' => ['start' => date('Y')-3,
                                'end' => date('Y')],
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.fiscal_year_one'))]); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('fiscal_year_two', [
                    'type' => 'year',
                    'class' => 'form-control-md',
                    'empty' => ['year' => __('年')],
                    'year' => ['start' => date('Y')-3,
                                'end' => date('Y')],
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.fiscal_year_two'))]); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('fiscal_year_three', [
                    'type' => 'year',
                    'class' => 'form-control-md',
                    'empty' => ['year' => __('年')],
                    'year' => ['start' => date('Y')-3,
                                'end' => date('Y')],
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.fiscal_year_three'))]); ?>
            </div>
        </div>
        <div class="row mx-lg-3 text-center">
            <div class="col-lg-2">
                <?php echo $this->Form->label('revenue', __('売上高')); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('revenue_one', [
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.revenue_one'))]); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('revenue_two', [
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.revenue_two'))]); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('revenue_three', [
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.revenue_three'))]); ?>
            </div>
        </div>
        <div class="row mx-lg-3 text-center">
            <div class="col-lg-2">
                <?php echo $this->Form->label('profits_losses', __('当期損益')); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('profits_losses_one', [
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.profits_losses_one'))]); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('profits_losses_two', [
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.profits_losses_two'))]); ?>
            </div>
            <div class="col-lg-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('profits_losses_three', [
                    'label' => false,
                    'value' => h($this->request->getSession()->read('loanData.profits_losses_three'))]); ?>
            </div>
        </div>
        <?php echo $this->Form->hidden('mode', ['value' => 'confirm']);?>
    </fieldset>
    <div class="row mx-lg-3">
        <div class="col-lg-6 text-right pr-5">
            <?= $this->Form->button(__('一時保存'), ['name' => 'temp', 'class' => 'button button-md']) ?>
        </div>
        <div class="col-lg-6 text-left pl-5">
            <?= $this->Form->button(__('確認'), ['name' => 'reserve', 'class' => 'button button-md']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>

</div>