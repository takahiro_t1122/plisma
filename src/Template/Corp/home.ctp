<?php

use Cake\Core\Configure;

$loan = $entity['loan'];
$finance = $entity['finance'];
?>

<?php if ($user->phone) { ?>
    <div class="contents-wrapper container profile">
        <div class="profile-contents row">
            <div class="occupation col-12 d-flex justify-content-center font-3x-large py-3">
                <?= h($loan->user->company_name) ?>
            </div>
            <div class="user-info col-6">
                <div class="company-name col-12">
                    <?= h($loan->user->company_name) ?>
                </div>
                <div class="url col-12">
                    <a href=<?= $loan->user->url ?> target="_blank"><?= h($loan->user->url) ?></a>
                </div>
                <div class="zip col-12">
                    <?= '〒'. h(substr($loan->user->company_zipcode, 0, 3)).'-'.h(substr($loan->user->company_zipcode, 3)) ?></a>
                </div>
                <div class="address col-12">
                    <?= h($loan->user->company_address1.$loan->user->company_address2.$loan->user->company_address3) ?></a>
                </div>
                <div class="establish col-12">
                    <?= h($loan->user->established_in) ?>
                </div>
                <div class="company-phone col-12">
                    <?= h($loan->user->company_phone) ?></a>
                </div>
                <div class="name col-12">
                    <?= h($loan->user->last_name).' '.h($loan->user->first_name) ?>
                </div>
                <div class="phone col-12">
                    <?= h($loan->user->phone) ?>
                </div>
                <div class="update-button col-6 text-left">
                    <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'edit', $loan->user->id]); ?>'" value="ユーザ情報を更新する" class="button button-sm py-1 w-100" />
                </div>
            </div>
            <div class="finance-info col-6">
                <div class="assets col-12">
                    <?= h(number_format($finance->assets)) ?>
                </div>
                <div class="loan_balance col-12">
                    <?= h(number_format($finance->loan_balance)) ?>
                </div>
                <div class="pdate-button col-6 text-center">
                    <input type="button" value="財務情報編集" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'updateFinanceItem', 'finance_id' => h($finance->id)]); ?>'" class="button button-sm py-1 w-100" />
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="contents-wrapper container">
    <div class="contents-semi-label">
        <?= __('新着情報') ?>
    </div>
</div>

<!--
<li><?= $this->Form->postLink(__('Delete User'), ['controller' => 'Users', 'action' => 'delete', $loan->user->id], ['confirm' => '削除してもよろしいでしょうか?']); ?></li>

<div class="actions columns large-2 medium-3">
    <h3><?= __('Corporation web-top screen') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__d('CakeDC/Users', 'Logout'), ['controller' => 'users', 'action' => 'logout'], ['confirm' => 'ログアウトしてもよいですか?']) ?></li>
        <li><?= $this->Html->link(__d('CakeDC/Users', 'Create Loan item'), ['controller' => 'corp', 'action' => 'createLoanItem']) ?></li>
        <li><?= $this->Html->link(__d('CakeDC/Users', 'Check registered Loan item'), ['controller' => 'corp', 'action' => 'viewLoanItem']) ?></li>
        <li><?= $this->Html->link(__d('CakeDC/Users', 'Check profile'), ['controller' => 'users', 'action' => 'profile']) ?></li>
    </ul>
</div>
<div class="users large-10 medium-9">
    temporary placement for payment module
    <form action="stripe-func" method="POST">
        <script
            src="https://checkout.stripe.com/checkout.js"
            class="stripe-button"
            data-key=<?php echo json_encode(Configure::read('Stripe.publishable_key')); ?>
            data-amount=<?php echo $amout_value; ?>
            data-name="有料プラン"
            data-description="Example charge"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
            data-locale="auto"
            data-currency="jpy">
        </script>
    </form>
    <p>現在の値: <?php echo $current_value; ?></p>
</div>
-->