<?php

$Finance = ${$tableAlias};
echo $this->Html->css('/scss/corp/create-loan.min.css');
echo $this->Html->script('/js/corp/update_finance_item.min.js', ['block' => true]);

?>
<div class="contents-wrapper container create-loan">
    <div class="contents-label">
        <?= __('財務情報更新') ?>
    </div>
    <?= $this->Form->create($Finance); ?>
    <fieldset>
        <div class="row m-3">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <?php echo $this->Form->label('assets', __('総資産')); ?>
                    </div>
                    <div class="col-12">
                        <?php echo $this->Form->control('assets', [
                        'label' => false,
                        'value' => h($Finance->assets)]); ?>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <?php echo $this->Form->label('loan_balance', __('融資残高')); ?>
                    </div>
                    <div class="col-12">
                        <?php echo $this->Form->control('loan_balance', [
                            'label' => false,
                            'value' => h($Finance->loan_balance)]);?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-3">
            <div class="col-12">
                <?php echo $this->Form->label('financial_month', __('決算月')); ?>
            </div>
            <div class="col-12">
                <?php echo $this->Form->control('financial_month', [
                    'label' => false,
                    'value' => h($Finance->financial_month),
                    'disabled' => 'disabled']); ?>
            </div>
        </div>
        <div class="row mx-3 text-center">
            <div class="col-2">
                <?php echo $this->Form->label('fiscal_year', __('決算期')); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('fiscal_year_one', [
                    'type' => 'year',
                    'class' => 'form-control-sm',
                    'empty' => ['year' => __('年')],
                    'year' => ['start' => date('Y')-3,
                                'end' => date('Y')],
                    'label' => false,
                    'value' => h(array_keys($Finance->revenues)[0])]); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('fiscal_year_two', [
                    'type' => 'year',
                    'class' => 'form-control-sm',
                    'empty' => ['year' => __('年')],
                    'year' => ['start' => date('Y')-3,
                                'end' => date('Y')],
                    'label' => false,
                    'value' => h(array_keys($Finance->revenues)[1])]); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('fiscal_year_three', [
                    'type' => 'year',
                    'class' => 'form-control-sm',
                    'empty' => ['year' => __('年')],
                    'year' => ['start' => date('Y')-3,
                                'end' => date('Y')],
                    'label' => false,
                    'value' => h(array_keys($Finance->revenues)[2])]); ?>
            </div>
        </div>
        <div class="row mx-3 text-center">
            <div class="col-2">
                <?php echo $this->Form->label('revenue', __('売上高')); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('revenue_one', [
                    'label' => false,
                    'value' => h($Finance->revenues[array_keys($Finance->revenues)[0]])]); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('revenue_two', [
                    'label' => false,
                    'value' => h($Finance->revenues[array_keys($Finance->revenues)[1]])]); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('revenue_three', [
                    'label' => false,
                    'value' => h($Finance->revenues[array_keys($Finance->revenues)[2]])]); ?>
            </div>
        </div>
        <div class="row mx-3 text-center">
            <div class="col-2">
                <?php echo $this->Form->label('profits_losses', __('当期損益')); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('profits_losses_one', [
                    'label' => false,
                    'value' => h($Finance->profits_losses[array_keys($Finance->profits_losses)[0]])]); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('profits_losses_two', [
                    'label' => false,
                    'value' => h($Finance->profits_losses[array_keys($Finance->profits_losses)[1]])]); ?>
            </div>
            <div class="col-3 d-flex align-items-center justify-content-center">
                <?php echo $this->Form->control('profits_losses_three', [
                    'label' => false,
                    'value' => h($Finance->profits_losses[array_keys($Finance->profits_losses)[2]])]); ?>
            </div>
        </div>
    </fieldset>
    <div class="row mx-3">
        <div class="col-6 text-right pr-5">
        <input type="button" onclick="history.back()"  value="キャンセル" class="button button-md" id="back" />
        </div>
        <div class="col-6 text-left pl-5">
            <?= $this->Form->button(__('更新'), ['class' => 'button button-md']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
    <div class="delete-item">
    <?php if (!empty($finance['id']))?>
        <?= $this->Html->link(__('Delete Finance Information'), ['controller' => 'corp', 'action' => 'deleteFinanceInfo', 'finance_id' => h($finance->id)], ['confirm' => 'データを削除してもよろしいでしょうか?']); ?>
    </div>
</div>