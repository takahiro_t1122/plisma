<?php

echo $this->Html->css('/scss/corp/view-loan-item.min.css');
$loan = $entities['loan'];
$finance = $entities['finance'];
$blockbanks = $entities['blockbanks'];

//ToDo change
$header_img = LOAN_HEADER_DIR . h($loan->user_id) . PNG;
$header_img_suffix = $header_img . '?' . date('YmdHis');
echo '<script type="text/javascript">var header_img_var = '.json_encode($header_img).';</script>';
echo $this->Html->script('/js/corp/upload_loan_image.min.js', ['block' => true]);
?>

<div class="contents-wrapper container view-loan-item">

    <div class="loan-info">
        <div class="contents-semi-label">
            <?= __('案件詳細') ?>
        </div>
        <div class="contents">
            <?php if (!is_null($loan)){ ?>
                <table>
                    <tbody>
                        <tr class="row mx-0">
                            <th class="col-3"><?= __('案件名') ?></th>
                            <td class="col-9"><?= h($loan->loan_title) ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 d-flex align-items-start py-0 bottom">
                                <div class="row mx-0 w-100">
                                    <div class="col-8 px-0 d-flex align-items-center"><?= __('案件画像') ?></div>
                                    <?php if (file_exists($header_img)) { ?>
                                        <div class="col-4 px-0 d-flex align-items-center justify-content-end">
                                            <?= $this->Html->link("更新", ['controller' => 'Corp', 'action' => 'uploadLoanImage'], ['class' => 'text-center my-1 button button-xs button-gray py-1 px-2 mr-2', 'id' => 'upload-link']);?>
                                        </div>
                                        <div class="offset-8 col-4 px-0 d-flex align-items-center justify-content-end">
                                        <?= $this->Html->link("削除", ['controller' => 'Corp', 'action' => 'deleteLoanImage', 'loginId' => h($loan->user_id)], ['class' => 'text-center my-1 button button-xs button-gray py-1 px-2 mr-2']);?>
                                    <?php } else { ?>
                                        <div class="col-4 px-0 d-flex align-items-center justify-content-end">
                                            <?= $this->Html->link("登録", ['controller' => 'Corp', 'action' => 'uploadLoanImage'], ['class' => 'text-center my-1 button button-xs button-gray py-1 px-2 mr-2', 'id' => 'upload-link']);?>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </th>
                            <td class="col-9">
                                <div class="row">
                                    <div class="col-6" id="loan-header-img">
                                        <?php if (file_exists($header_img)) {
                                            echo $this->Html->image('../' . $header_img_suffix, ['class' => 'w-100']);
                                        } else {
                                            ;
                                        } ?>
                                </div>
                            </td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3"><?= __('希望融資額') ?></th>
                            <td class="col-3 left"><?= number_format(h($loan->desired_loan_amount)).' 円' ?></td>
                            <th class="col-2"><?= __('希望融資期間') ?></th>
                            <td class="col-4"><?= h($loan->loan_period).' ヶ月' ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3"><?= __('資金使途') ?></th>
                            <?php if ($loan->loan_purpose == 2) { ?>
                                <td class="col-9"><?= nl2br(h($loan->loan_purpose_detail)) ?></td>
                            <?php } else { ?>
                                <td class="col-9"><?= h($loan->loan_purpose_view) ?></td>
                            <?php } ?>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3"><?= __('業種') ?></th>
                            <td class="col-9"><?= h($loan->category->name."  >  ".$loan->sub_category->name) ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3"><?= __('事業内容') ?></th>
                            <td class="col-9"><?= nl2br(h($loan->job_description)) ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3"><?= __('追加コメント') ?></th>
                            <td class="col-9"><?= nl2br(h($loan->prospect)) ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 d-flex align-items-center py-0">
                                <div class="row mx-0 w-100">
                                    <div class="col-8 px-0 d-flex align-items-center"><?= __('案件ステータス') ?></div>
                                    <div class="col-4 px-0 d-flex align-items-center justify-content-end">
                                        <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'visibleStatusChange', 'loan_id' => h($loan->id)]); ?>'" value="変更" class="my-1 button button-xs button-gray py-1 px-2" />
                                    </div>
                                </div>
                            </th>
                            <td class="col-9">
                                <?php if ($loan->is_visible) { echo __('公開');
                                } else { echo __('非公開'); } ?>
                            </td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 d-flex align-items-center py-0 bottom">
                                <div class="row mx-0 w-100">
                                    <div class="col-8 px-0 d-flex align-items-center"><?= __('非公開先銀行') ?></div>
                                    <div class="col-4 px-0 d-flex align-items-center justify-content-end">
                                        <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'viewBlock', 'loan_id' => h($loan->id)]); ?>'" value="更新" class="my-1 button button-xs button-gray py-1 px-2"/>
                                    </div>
                                </div>
                            </th>
                            <td class="col-9 bottom">
                                <?php foreach ($blockbanks as $bank) {
                                    echo $bank->bank->bank_name;
                                    if (next($blockbanks)) {
                                        echo "、";
                                    }} ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="delete-item">
                    案件情報を削除する場合、データが削除され復元することができません。それでも削除しますか。<br />
                    <?= $this->Html->link(__('Delete Loan Item'), ['controller' => 'corp', 'action' => 'deleteLoanItem', 'loan_id' => h($loan->id)], ['confirm' => 'データを削除してもよろしいでしょうか?']); ?>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <input type="button" value="案件詳細編集" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'updateLoanItem', 'loan_id' => h($loan->id)]); ?>'" class="button button-md" />
                    </div>
                </div>
            <?php } else { ?>
                入力されていません。
            <?php } ?>
        </div>
    </div>

    <div class="finance-info mt-3">
        <div class="contents-semi-label">
            <?= __('財務情報') ?>
        </div>
        <div class="contents">
            <?php if (!is_null($finance)){ ?>
                <table>
                    <tbody>
                        <tr class="row mx-0">
                            <th class="col-3"><?= __('総資産') ?></th>
                            <td class="col-3 left"><?= number_format(h($finance->assets)).' 円' ?></td>
                            <th class="col-2"><?= __('融資残高') ?></th>
                            <td class="col-4"><?= number_format(h($finance->loan_balance)).' 円' ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-12"><?= h(array_keys($finance->revenues)[0]).'年'.h($finance->financial_month).'月期' ?></th>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 pl-5">売上</th>
                            <td class="col-9"><?= h($finance->revenues[array_keys($finance->revenues)[0]]).' 円' ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 pl-5">損益</th>
                            <td class="col-9"><?= h($finance->profits_losses[array_keys($finance->profits_losses)[0]]).' 円' ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-12"><?= h(array_keys($finance->revenues)[1]).'年'.h($finance->financial_month).'月期' ?></th>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 pl-5">売上</th>
                            <td class="col-9"><?= h($finance->revenues[array_keys($finance->revenues)[1]]).' 円' ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 pl-5">損益</th>
                            <td class="col-9"><?= h($finance->profits_losses[array_keys($finance->profits_losses)[1]]).' 円' ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-12"><?= h(array_keys($finance->revenues)[2]).'年'.h($finance->financial_month).'月期' ?></th>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 pl-5">売上</th>
                            <td class="col-9"><?= h($finance->revenues[array_keys($finance->revenues)[2]]).' 円' ?></td>
                        </tr>
                        <tr class="row mx-0">
                            <th class="col-3 pl-5 bottom">損益</th>
                            <td class="col-9 bottom"><?= h($finance->profits_losses[array_keys($finance->profits_losses)[2]]).' 円' ?></td>
                        </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-12 text-center">
                        <input type="button" value="財務情報編集" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'updateFinanceItem', 'finance_id' => h($finance->id)]); ?>'" class="button button-md" />
                    </div>
                </div>
            <?php } else { ?>
                入力されていません。
            <?php } ?>
        </div>
    </div>

    <?php if (!is_null($loan) && !is_null($finance)) { ?>
        <div class="file-info mt-3">
            <div class="contents-semi-label">
                <?= __('追加資料アップロード') ?>
            </div>
            <div class="contents">
                <p>案件概要や今後の展望等、銀行に説明が必要な追加資料があれば、アップロードしてください。</p>
                <?php if (!is_null($files)) { ?>
                    <div class="row pb-3">
                        <div class="col-12">
                            【アップロード済資料】
                        </div>
                    <?php foreach ($files as $file) { ?>
                        <div class="col-3 d-flex align-items-center ml-3">
                            <?= $file ?>
                        </div>
                        <div class="col-8 px-0 d-flex align-items-center">
                            <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'fileDownload', 'loan_id' => h($loan->id), 'file_name' => h($file)]); ?>'" value="確認" class="my-1 button button-xs button-gray py-1 px-2 mr-2" />
                            <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'fileDelete', 'loan_id' => h($loan->id), 'file_name' => h($file)]); ?>'" value="削除" class="my-1 button button-xs button-gray py-1 px-2" />
                        </div>
                    <?php
                    } ?>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-12">
                        【追加】
                    </div>
                    <div class="col-12 ml-3">
                        <?= $this->form->create($fileform, ['enctype' => 'multipart/form-data', 'controller' => 'corp', 'action' => 'viewLoanItem', 'type' => 'post']); ?>
                        <?= $this->form->input('upload_file', ['type' => 'file', 'label' => false]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <?= $this->Form->button(__('アップロード'), ['class' => 'button button-md']) ?>
                    </div>
                </div>
                <?= $this->form->end(); ?>
            </div>
        </div>
    <?php } ?>

</div>