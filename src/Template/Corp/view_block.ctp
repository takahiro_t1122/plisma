<?php

// jQuery UI
echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js');
echo $this->Html->css('https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css');

echo $this->Html->css('/scss/corp/view-block.min.css');
//echo $this->Html->script('/js/corp/view_block.min.js', ['block' => true]);

?>

<div class="contents-wrapper container bank-view-block">
    <div class="contents-label pb-3">
        <?= __('非公開先銀行設定') ?>
    </div>
    <fieldset>
        <div class="contents-semi-label row my-0">
            <h5 class="offset-1 col-4">銀行名入力</h5>
        </div>
        <div class="explain">
            <p class="offset-1">非公開にしたい銀行名を入力してください。</p>
        </div>
        <?= $this->Form->create('blockbanks'); ?>
        <div class="row">
            <div class="offset-1 col-6 d-flex align-items-center my-1 pb-5">
                <?php echo $this->Form->control('bank_id', [
                    'type' => 'select',
                    'options' => $bank_list,
                    'value' => h($this->request->getSession()->read('inputData.bank_name')),
                    'label' => false,
                    'id' => 'combobox']);
                ?>
            </div>
            <div class="col-5">
                <?= $this->Form->button(__('追加する'), ['name' => 'block', 'class' => 'my-1 button button-xs']); ?>
            </div>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
    <?php foreach($blockbanks as $bank) {
        $key[] = h($bank->bank_id);
        $value[] = h($bank->bank->bank_name);
        }
        $block_bank_list = array_combine($key, $value);?>
    <fieldset>
        <?= $this->Form->create(); ?>
        <div class="contents-semi-label row">
            <h5 class="offset-1 col-4">非公開先銀行</h5>
        </div>
        <div class="row">
            <div class="offset-1 col-6">
                <?php echo $this->Form->control('unblock', [
                    'type' => 'select',
                    'label' => false,
                    'options' => $block_bank_list,
                    'multiple' => 'checkbox']); ?>
            </div>
            <div class="col-5">
                <?= $this->Form->button(__('ブロック解除'), ['name' => 'remove_block', 'class' => 'my-1 button button-xs']) ?>
            </div>
    </fieldset>
    <?= $this->Form->end() ?>

    <?php
        if (!is_null($loan->is_visible)) { ?>
            <div class="row mt-3">
                <div class="update-button col-12 text-center">
                    <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'viewLoanItem']); ?>'" value="設定完了" class="button button-md" />
                </div>
            </div>
        <?php } else { ?>
            <div class="row mt-3">
                <div class="comp-unopen-button col-6 text-right">
                    <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'visibleStatusChange', 'loan_id' => $loan->id, 'status' => 'hide']); ?>'" value="設定完了し案件非公開" class="button button-md" />
                </div>
                <div class="comp-open-button col-6 text-left">
                    <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'corp', 'action' => 'visibleStatusChange', 'loan_id' => $loan->id, 'status' => 'open']); ?>'" value="設定完了し案件公開" class="button button-md" />
                </div>
            </div>
        <?php } ?>
</div>

<script>
!function($){$.widget("ui.combobox",{_create:function(){var input,that=this,select=this.element.hide(),selected=select.children(":selected"),value=selected.val()?selected.text():"",wrapper=this.wrapper=$("<span>").addClass("ui-combobox").insertAfter(select);function removeIfInvalid(element){var value=$(element).val(),matcher=new RegExp("^"+$.ui.autocomplete.escapeRegex(value)+"$","i"),valid=!1;select.children("option").each(function(){if($(this).text().match(matcher))return this.selected=valid=!0,!1})}(input=$("<input>").appendTo(wrapper).val(value).attr("title","").addClass("ui-state-default ui-combobox-input").autocomplete({delay:0,minLength:0,source:function(request,response){var matcher=new RegExp($.ui.autocomplete.escapeRegex(request.term),"i");response(select.children("option").map(function(){var text=$(this).text();if(this.value&&(!request.term||matcher.test(text)))return{label:text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)("+$.ui.autocomplete.escapeRegex(request.term)+")(?![^<>]*>)(?![^&;]+;)","gi"),"$1"),value:text,option:this}}))},select:function(event,ui){ui.item.option.selected=!0,that._trigger("selected",event,{item:ui.item.option})},change:function(event,ui){if(!ui.item)return removeIfInvalid(this)}}).addClass("ui-widget ui-widget-content ui-corner-left")).data("ui-autocomplete")._renderItem=function(ul,item){return $("<li>").data("ui-autocomplete-item",item).append("<a>"+item.label+"</a>").appendTo(ul)}},destroy:function(){this.wrapper.remove(),this.element.show(),$.Widget.prototype.destroy.call(this)}})}(jQuery),$(function(){$("#combobox").combobox(),$("#toggle").click(function(){$("#combobox").toggle()}).button()});
</script>