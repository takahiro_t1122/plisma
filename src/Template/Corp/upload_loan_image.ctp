<?php

// cropper
echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css');
echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js', ['block' => true]);

echo $this->Html->css('/scss/corp/upload-loan-image.min.css');

?>

<div class="contents-wrapper container upload-loan-image">
    <div class="cropper-container w-100">
        <div class="triming-image text-center">
            <input type="file" id="triming_image" name="triming_image" class="triming_image my-3" required />
        </div>
        <img src="" alt="トリミング画像" id="trimed_image" style="display: none;" />
        <div class="text-center">
            <input type="button" id="crop_btn" value="画像をトリミングして保存" class="button button-md" />
        </div>
    </div>
</div>

<?= $this->Form->create(); ?>
    <fieldset>
        <?php echo $this->Form->hidden('trimedImageForm', ['value' => '']);?>
    </fieldset>
<?= $this->Form->end() ?>

<script>
    $(function(){
        $('#triming_image').on('change', function(event){
            var trimingImage = event.target.files;
            var csrf = $('[name="_csrfToken"]').val();

            // imageタグは1つしかファイルを送信できない仕組みと複数送信する仕組みの二通りありますので、サーバー側でチェックを忘れないようにしてください。
            if(trimingImage.length > 1){
                console.log(trimingImage.length + 'つのファイルが選択されました。');
                return false;
            }
            // 改め代入します。
            trimingImage = trimingImage[0];

            if(!trimingImage.type.match('image/jp.*') // jpg jpeg でない
                &&!trimingImage.type.match('image/png') // png でない
                &&!trimingImage.type.match('image/gif') // gif でない
                &&!trimingImage.type.match('image/bmp') // bmp でない
            ){
                alert('No Support ' + trimingImage.type + ' type image');
                $(this).val('');
                return false;
            }

            var fileReader = new FileReader();
            fileReader.onload = function(e){
                var int32View = new Uint8Array(e.target.result);
                if((int32View.length>4 && int32View[0]==0xFF && int32View[1]==0xD8 && int32View[2]==0xFF && int32View[3]==0xE0)
                || (int32View.length>4 && int32View[0]==0xFF && int32View[1]==0xD8 && int32View[2]==0xFF && int32View[3]==0xDB)
                || (int32View.length>4 && int32View[0]==0xFF && int32View[1]==0xD8 && int32View[2]==0xFF && int32View[3]==0xD1)
                || (int32View.length>4 && int32View[0]==0x89 && int32View[1]==0x50 && int32View[2]==0x4E && int32View[3]==0x47)
                || (int32View.length>4 && int32View[0]==0x47 && int32View[1]==0x49 && int32View[2]==0x46 && int32View[3]==0x38)
                || (int32View.length=2 && int32View[0]==0x42 && int32View[1]==0x4D && int32View[2]==0x46 && int32View[3]==0x38)
                ) {
                    // success
                    $('#trimed_image').css('display', 'block');
                    $('#trimed_image').attr('src', URL.createObjectURL(trimingImage));
                    return true;
                } else {
                    // failed
                    alert('No Support ' + trimingImage.type + ' type image');
                    $('#trimed_image').val('');
                    return false;
                }
            };

            fileReader.readAsArrayBuffer(trimingImage);
            fileReader.onloadend = function(e){
                var image = document.getElementById('trimed_image');
                var button = document.getElementById('crop_btn');
                var croppable = false;
                var cropper = new Cropper(image, {
                    aspectRatio: 16 / 9,
                    viewMode: 1,
                    ready: function () {
                        croppable = true;
                    },
                });

                // To reload image after uploading a image
                if ($('#trimed_image.cropper-hidden').length != 0) {
                    $('.cropper-container').eq(1).remove();
                    cropper.destroy();
                    cropper.replace(URL.createObjectURL(trimingImage), false);
                }

                button.onclick = function () {
                    var croppedCanvas;

                    if (!croppable) {
                        alert('トリミングする画像が設定されていません。');
                        return false;
                    }

                    croppedCanvas = cropper.getCroppedCanvas();
                    var blob;
                    if (croppedCanvas.toBlob) {
                        croppedCanvas.toBlob(function(blob){
                            var trimedImageForm = new FormData();
                            trimedImageForm.append('blob', blob);
                            $.ajax({
                                type: 'POST',
                                url: '/corp/upload-loan-image',
                                beforeSend: function(xhr) {
                                    xhr.setRequestHeader('X-CSRF-Token', csrf);
                                },
                                processData: false,
                                contentType: false,
                                data: trimedImageForm,
                            })
                            .done(function(res, textStatus, jqXHR){
                                var index = res.indexOf('<');
                                var str = res.substring(0, index);
                                var response = $.parseJSON(str);
                                if (response.status == 'success') {
                                    console.log(response);
                                    alert('アップロードしました。');
                                    setTimeout(function(){
                                        open('about:blank','_self').close();
                                    }, 500);
                                } else if (response.status == 'error') {
                                    alert('画像作成に失敗しました。再度お試しください。\n' + response.msg);
                                } else {
                                    alert('システムエラーが発生しました。');
                                }
                            })
                            // Ajaxリクエストが失敗した時発動
                            .fail(function(XMLHttpRequest, textStatus, errorThrown){
                                alert("失敗");
                                console.log("ajax通信に失敗しました");
                                console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                                console.log("textStatus     : " + textStatus);
                                console.log("errorThrown    : " + errorThrown.message);
                            });
                        });
                    }
                    else if (croppedCanvas.msToBlob) {
                        blob = croppedCanvas.msToBlob();
                        var trimedImageForm = new FormData();
                        trimedImageForm.append('blob', blob);
                        $.ajax({
                            type: 'POST',
                            url: '/corp/upload-loan-image',
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-CSRF-Token', csrf);
                            },
                            processData: false,
                            contentType: false,
                            data: trimedImageForm,
                        })
                        .done(function(res, textStatus, jqXHR){
                            var index = res.indexOf('<');
                            var str = res.substring(0, index);
                            var response = $.parseJSON(str);
                            if (response.status == 'success') {
                                console.log(response);
                                alert('アップロードしました。');
                            } else if (response.status == 'error') {
                                alert('画像作成に失敗しました。再度お試しください。\n' + response.msg);
                            } else {
                                alert('システムエラーが発生しました。');
                            }
                        })
                        .fail(function(XMLHttpRequest, textStatus, errorThrown){
                            alert("失敗");
                            console.log("ajax通信に失敗しました");
                            console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                            console.log("textStatus     : " + textStatus);
                            console.log("errorThrown    : " + errorThrown.message);
                        });
                    } else {
                        imageURL = canvas.toDataURL();
                    }
                };
            };
        });
    });
</script>