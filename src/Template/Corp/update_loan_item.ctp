<?php

$Loan = ${$tableAlias};
echo $this->Html->css('/scss/corp/create-loan.min.css');
echo $this->Html->script('/js/corp/update_loan_item.min.js', ['block' => true]);

?>
<div class="contents-wrapper container create-loan">
    <div class="contents-label">
        <?= __('案件情報更新') ?>
    </div>
    <?= $this->Form->create($Loan); ?>
    <fieldset>
        <div class="row m-3">
            <div class="col-12">
                <?php echo $this->Form->label('loan_title', __('案件名')); ?>
            </div>
            <div class="col-12">
                <?php echo $this->Form->control('loan_title', [
                    'label' => false,
                    'value' => h($Loan->loan_title)]); ?>
            </div>
        </div>
        <div class="row m-3">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <?php echo $this->Form->label('desired_loan_amount', __('融資希望額')); ?>
                    </div>
                    <div class="col-12">
                        <?php echo $this->Form->control('desired_loan_amount', [
                            'label' => false,
                            'value' => h($Loan->desired_loan_amount)]);?>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <?php echo $this->Form->label('loan_period', __('希望融資期間')); ?>
                    </div>
                    <div class="col-12">
                        <?php echo $this->Form->control('loan_period', [
                            'label' => false,
                            'value' => h($Loan->loan_period)]);?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-3">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <?php echo $this->Form->label('loan_purpose', __('資金使途')); ?>
                    </div>
                    <div class="col-12">
                        <?php echo $this->Form->control('loan_purpose', [
                        'type' => 'select',
                        'class' => 'form-control-md',
                        'options' => ['' => '', '0' => '運転資金', '1' => '設備資金', '2' => 'その他'],
                        'label' => false,
                        'value' => h($Loan->loan_purpose)]); ?>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <?php echo $this->Form->label('loan_purpose_detail', __('その他の場合')); ?>
                    </div>
                    <div class="col-12">
                        <?php echo $this->Form->control('loan_purpose_detail', [
                            'label' => false,
                            'value' => h($Loan->loan_purpose_detail)]);?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-3">
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <?php echo $this->Form->label('category', __('事業業種')); ?>
                    </div>
                    <div class="col-12">
                        <?php echo $this->Form->control('category', [
                            'type' => 'select',
                            'class' => 'form-control-md',
                            'options' => $category_list,
                            'label' => false,
                            'value' => h($Loan->category)]); ?>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-12">
                        <?php echo $this->Form->label('sub_category', __('業種詳細')); ?>
                    </div>
                    <div class="col-12">
                        <?php echo $this->Form->control('sub_category', [
                        'type' => 'select',
                        'class' => 'form-control-md',
                        'label' => false,
                        'options' => [h($Loan->sub_category) => h($Loan->sub_category_name)],
                        'default' => $Loan->sub_category]
                    ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-3">
            <div class="col-12">
                <?php echo $this->Form->label('job_description', __('事業内容')); ?>
            </div>
            <div class="col-12">
                <?php echo $this->Form->control('job_description', [
                    'type' => 'textarea',
                    'escape' => true,
                    'label' => false,
                    'value' => h($Loan->job_description)]); ?>
            </div>
        </div>
        <div class="row m-3">
            <div class="col-12">
                <?php echo $this->Form->label('prospect', __('追加コメント')); ?>
            </div>
            <div class="col-12">
                <?php echo $this->Form->control('prospect', [
                    'type' => 'textarea',
                    'escape' => true,
                    'label' => false,
                    'value' => h($Loan->prospect)]); ?>
            </div>
        </div>
    </fieldset>
    <div class="row mx-3">
        <div class="col-6 text-right pr-5">
        <input type="button" onclick="history.back()"  value="キャンセル" class="button button-md" id="back" />
        </div>
        <div class="col-6 text-left pl-5">
            <?= $this->Form->button(__('更新'), ['class' => 'button button-md']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>