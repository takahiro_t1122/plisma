<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';

if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?= $this->element('auto_table_warning') ?>
<?php
if (extension_loaded('xdebug')) :
    xdebug_print_function_stack();
endif;

$this->end();
endif;
?>
<div class="contents-wrapper container error-page text-center">
    <h1 class="pt-5"><i class="fas fa-exclamation-triangle"></i><strong><?= __(' 404 Not Found')?></strong></h1>
    <h5 class="pb-5">- ページが見つかりません -</h5>
    <p class="error-detail pb-2">
        申し訳ございませんが、お探しのページが見つかりませんでした。<br />
        お探しのページは削除されたか、URLが変更された可能性がございます。
    </p>
    <div class="row">
        <div class="col-6 text-right">
            <input type="button" onClick="history.back()" value="前の画面へ戻る" class="button button-md" />
        </div>
        <div class="col-6 text-left">
            <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'display', $loan->user->id]); ?>'" value="TOPへ戻る" class="button button-md" />
        </div>
    </div>
</div>