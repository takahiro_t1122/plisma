<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'default';

if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error500.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?php if ($error instanceof Error) : ?>
        <strong>Error in: </strong>
        <?= sprintf('%s, line %s', str_replace(ROOT, 'ROOT', $error->getFile()), $error->getLine()) ?>
<?php endif; ?>
<?php
    echo $this->element('auto_table_warning');

    if (extension_loaded('xdebug')) :
        xdebug_print_function_stack();
    endif;

    $this->end();
endif;
?>
<div class="contents-wrapper container error-page text-center">
    <h1 class="pt-5"><i class="fas fa-exclamation-triangle"></i><strong><?= __('　500 Internal Server Error')?></strong></h1>
    <h5 class="pb-5">- エラーが発生しました -</h5>
    <p class="error-detail pb-2">
        少しお時間を空けて再度アクセスをお願いします。<br />
        何度実施しても同じ現象が発生する場合はお手数ですが<em><?= EMAIL_SUPPORT ?></em>までご連絡ください。
    </p>
    <div class="row">
        <div class="update-button col-6 text-right">
            <input type="button" onClick="history.back()" value="前の画面へ戻る" class="button button-md" />
        </div>
        <div class="update-button col-6 text-left">
            <input type="button" onClick="location.href='<?php echo $this->Url->build(['controller' => 'Pages', 'action' => 'display', $loan->user->id]); ?>'" value="TOPへ戻る" class="button button-md" />
        </div>
    </div>
</div>