<h1>新規会話作成</h1>
<?php
    echo $this->Form->create($form, ['url' => ['action' => 'new']]);
    echo $this->Form->control('to');
    echo $this->Form->control('subject');
    echo $this->Form->control('body', ['rows' => '3']);
    echo $this->Form->button(__('送信する'));
    echo $this->Form->end();
?>