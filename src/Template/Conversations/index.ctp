<h1>Inbox</h1>
<?php if ($role ===  ROLE_BANK): ?>
    <?= $this->Html->link('新規会話作成', ['action' => 'new']) ?>
<?php endif; ?>

<style type="text/css">
.unread {font-weight:bold;}
</style>

<table>
    <tr>
        <th>Subject</th>
        <th>Last Reply</th>
        <th>Action</th>
    </tr>

    <?php foreach ($conversations as $conversation): ?>
    <tr>
        <td>
        <div class="<?php if ($conversation->unread) { echo 'unread'; } ?>">
            <?= $this->Html->link($conversation->subject, [
                'action' => 'detail',
                $conversation->id,
            ]) ?>
        </div>
        </td>
        <td>
        <div class="<?php if ($conversation->unread) { echo 'unread'; } ?>">
            <?= $conversation->last_reply ?>
        </div>
        </td>
        <td>
        <div class="<?php if ($conversation->unread) { echo 'unread'; } ?>">
            <?= $this->Form->postLink(
                '削除',[
                    'action' => 'delete',
                    $conversation->id,
                ],
                ['confirm' => 'よろしいですか?'])
            ?>
        </div>
        </td>
    </tr>
    <?php endforeach; ?>
</table>