<h1>会話詳細</h1>
<?= $this->Html->link('受信ボックス', ['action' => 'index']) ?>
<?php
    echo $this->Form->create($new_message, ['url' => ['action' => 'reply']]);
    echo $this->Form->hidden('conversation_id', ['value' => $conversation_id]);
    echo $this->Form->hidden('user_id', ['value' => $user_id]);
    echo $this->Form->control('content', [
        'required' => true,
        'rows' => '3'
    ]);
    echo $this->Form->button(__('返信する'));
    echo $this->Form->end();
?>

<style>
    .message_box { background-color:#EDF7FF; }
	.unread { background-color:#FFF5ED }
</style>
<?php foreach ($messages as $message): ?>
    <div class=" message_box <?php if ($message->unread) { echo 'unread'; } ?>">
        <p><?= $message->user_name ?>(<?= $message->sent_at ?>)</p>
        <p><?= $message->content ?></p>
    </div>
<?php endforeach; ?>
