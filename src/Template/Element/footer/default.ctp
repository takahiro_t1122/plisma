<?php
use Cake\Core\Configure;
?>
<!-- Footer Links -->
<div class="container text-center text-md-left">

    <!-- Footer links -->
    <div class="row text-center text-md-left mt-3 pb-3">

    <!-- Grid column -->
    <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
        <h6 class="mb-4"><a href="<?= COMPANY_URL ?>">株式会社plisma</a></h6>
        <p>Here you can use rows and columns here to organize your footer content. Lorem ipsum dolor sit amet, consectetur
        adipisicing elit.</p>
        <p><i class="fas fa-home mr-3"></i> <?= ADDRESS ?></p>
        <p><i class="fas fa-envelope mr-3"></i> <?= EMAIL_BASE ?></p>
    </div>
    <!-- Grid column -->

    <hr class="w-100 clearfix d-md-none">

    <!-- Grid column -->
    <div class="col-md-2 col-lg-2 col-xl-3 mx-auto mt-3">
        <h6 class="mb-4 font-weight-bold">About</h6>
        <p><a href="#!">plismaについて</a></p>
        <p><a href="#!">利用料金</a></p>
        <p><?= $this->Html->link("よくあるご質問", ['controller' => 'pages', 'action' => 'faq']);?></p>
    </div>
    <!-- Grid column -->

    <hr class="w-100 clearfix d-md-none">

    <!-- Grid column -->
    <div class="col-md-3 col-lg-2 col-xl-3 mx-auto mt-3">
        <h6 class="mb-4 font-weight-bold">My Page</h6>
        <p>
        <?= $this->Html->link("マイページトップ", ['controller' => 'allocations', 'action' => 'loginRedirect']);?>
        </p>
        <p>
        <a href="#!">案件情報</a>
        </p>
        <p>
        <a href="#!">メッセージ</a>
        </p>
    </div>

    <!-- Grid column -->
    <hr class="w-100 clearfix d-md-none">

    <!-- Grid column -->
    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
        <h6 class="mb-4 font-weight-bold">運営情報</h6>
        <p><a href="#!">運営会社</a></p>
        <p><?= $this->Html->link("利用規約", ['controller' => 'pages', 'action' => 'termsOfService']);?></p>
        <p><?= $this->Html->link("秘密保持契約", ['controller' => 'pages', 'action' => 'nda']);?></p>
        <p><?= $this->Html->link("個人情報保護方針", ['controller' => 'pages', 'action' => 'privacy']);?></p>
        <p><?= $this->Html->link("特定商取引法に基づく表示", ['controller' => 'pages', 'action' => 'tokusyo']);?></p>
        <!--
        <p><i class="fas fa-home mr-3"></i> New York, NY 10012, US</p><p>
        <i class="fas fa-envelope mr-3"></i> info@plisma.co.jp</p>
        <p>
        <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
        <p>
        <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>
        -->
    </div>
    <!-- Grid column -->

    </div>
    <!-- Footer links -->

    <hr>

    <!-- Grid row -->
    <div class="row d-flex align-items-center">

    <!-- Grid column -->
    <div class="col-md-7 col-lg-8">

        <!--Copyright-->
        <p class="text-center text-md-left">Copyright &copy; 2019- 
        <a href="<?= COMPANY_URL ?>">
            <strong>plisma</strong>
        </a>
        All Rights Reserved.
        </p>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-5 col-lg-4 ml-lg-0">

        <!-- Social buttons -->
        <div class="text-center text-md-right">
        <ul class="list-unstyled list-inline">
            <li class="list-inline-item">
            <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-facebook-f"></i>
            </a>
            </li>
            <li class="list-inline-item">
            <a class="btn-floating btn-sm rgba-white-slight mx-1">
                <i class="fab fa-twitter"></i>
            </a>
            </li>
        </ul>
        </div>

    </div>
    <!-- Grid column -->

    </div>
    <!-- Grid row -->

</div>
<!-- Footer Links -->
