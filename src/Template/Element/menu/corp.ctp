<nav class="navbar navbar-expand-sm fixed-top container-fluid">
    <?= $this->Html->link('plisma', '/', ['class' => 'navbar-brand']); ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbar">
        <ul class="navbar-nav text-right">
            <li class="nav-item">
                <?= $this->Html->link("ホーム", ['controller' => 'allocations', 'action' => 'loginRedirect'], ["class"=>"nav-link"]);?>
            </li>
            <li class="nav-item"><?= $this->Html->link("案件登録", ['controller' => 'corp', 'action' => 'createLoanItem'], ["class"=>"nav-link"]);?></li>
            <li class="nav-item"><?= $this->Html->link("登録情報", ['controller' => 'corp', 'action' => 'viewLoanItem'], ["class"=>"nav-link"]);?></li>
<!--            <li class="nav-item"><?= $this->Html->link("財務情報", ['controller' => 'corp', 'action' => ''], ["class"=>"nav-link"]);?></li>
-->            <li class="nav-item"><?= $this->Html->link("メッセージ", ['controller' => 'conversations', 'action' => 'index'], ["class"=>"nav-link"]);?></li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle nav-link" role="button" data-toggle="dropdown" id="navbarDropdownMenuLink" aria-haspopup="true" aria-expanded="false">設定</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <?= $this->Html->link("プロフィール確認", ['controller' => 'Users', 'action' => 'profile'], ['class' => 'dropdown-item']);?>
                <?= $this->Html->link("メールアドレス変更", ['controller' => 'Users', 'action' => 'changeEmail'], ['class' => 'dropdown-item']);?>
                <?= $this->Html->link("パスワード変更", ['controller' => 'Users', 'action' => 'changePassword'], ['class' => 'dropdown-item']);?>
                <?= $this->Html->link("ログアウト", ['controller' => 'Users', 'action' => 'logout'], ['class' => 'dropdown-item']);?>
                </div>
            </li>
        </ul>
    </div>
</nav>
