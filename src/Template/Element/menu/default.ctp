<?php

use Cake\Core\Configure;

echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.2/jquery.modal.min.js');
echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.2/jquery.modal.min.css');
echo $this->Html->css('/scss/nav.min.css');

?>

<nav class="navbar navbar-expand-sm fixed-top container-fluid">
    <?= $this->Html->link('plisma', '/', ['class' => 'navbar-brand']); ?>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbar">
        <ul class="navbar-nav text-right">
            <li class="nav-item"><?= $this->Html->link("ログイン", '#login-modal', ["data-modal" => "", "class"=>"nav-link"]);?></li>
            <li class="nav-item"><?= $this->Html->link("新規登録", ['controller' => 'users', 'action' => 'register'], ["class"=>"nav-link"]);?></li>
        </ul>
    </div>
</nav>

<!-- https://jquerymodal.com/ modal -->
<div id="login-modal" class="modal">
    <?= $this->Form->create('', ['url' => ['controller' => 'users', 'action' => 'login']]) ?>
    <fieldset>
        <legend><?= __d('CakeDC/Users', 'Login') ?></legend>
        <?= $this->Form->control('email', ['label' => false, 'placeholder' => 'メールアドレス']) ?>
        <?= $this->Form->control('password', ['label' => false, 'placeholder' => 'パスワード']) ?>
        <?= $this->Flash->render('login_fault', ['element' => './Flash/login_fault']) ?>
        <?php
        if (Configure::read('Users.reCaptcha.login')) {
            echo $this->User->addReCaptcha();
        }
        if (Configure::read('Users.RememberMe.active')) {
            echo $this->Form->control(Configure::read('Users.Key.Data.rememberMe'), [
                'type' => 'checkbox',
                'label' => __d('CakeDC/Users', 'Remember me'),
                'checked' => Configure::read('Users.RememberMe.checked')
            ]);
        }
        ?>
        <div class='password-forget'>
        <?= $this->Html->link(__('パスワードを忘れた方はこちら'), ['controller' => 'users', 'action' => 'requestResetPassword'], ['class' => 'font-x-small']); ?>
        </div>

    </fieldset>
    <?= $this->Form->button(__d('CakeDC/Users', 'Login'), ['class' => 'button button-md']); ?>
    <?= $this->Form->end() ?>
    <hr>
    <div class="first-time-use">
        <p class="font-x-small font-gray">Plismaを初めてご利用する方はこちらから</p>
    </div>
    <?php
    $registrationActive = Configure::read('Users.Registration.active');
    if ($registrationActive) {
    ?>
        <input type="button" class="button button-md button-white" value="会員登録" onClick="javascipt:window.location.href='/users/register'" >
    <?php } ?>
</div>

<script>
    $('a[data-modal]').click(function(event) {
        $(this).modal();
        return false;
    });

    $(function(){
        var url = location.href;
        var id = 'login-modal';
        if (url.match(id)) {
            $('#login-modal').modal();
        }
    });

    $.modal.defaults = {
        closeExisting: true,    // Close existing modals. Set this to false if you need to stack multiple modal instances.
        escapeClose: true,      // Allows the user to close the modal by pressing `ESC`
        clickClose: true,       // Allows the user to close the modal by clicking the overlay
        showClose: false,       // Shows a (X) icon/link in the top-right corner
        fadeDuration: 500
    }

    $(document).ready(function() {
        $(".navbar-nav li a").click(function(event) {
            $(".navbar-collapse").collapse('hide');
        });
    });
</script>