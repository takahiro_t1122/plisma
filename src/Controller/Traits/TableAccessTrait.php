<?php

namespace App\Controller\Traits;

use Cake\Core\Configure;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

trait TableAccessTrait
{
    protected $_userLoggingsTable = null;
    protected $_bankTable = null;
    protected $_categoryTable = null;
    protected $_subCategoryTable = null;
    protected $_blocklistTable = null;
    protected $_financeTable = null;
    protected $_loanTable = null;
    protected $_userPaymentTable = null;
    protected $_paymentDetailTable = null;
    protected $_bankFavoriteTable = null;

    /**
     * Gets the user loggings table instance
     *
     * @return Table
     */
    public function getUserLoggingsTable()
    {
        if ($this->_userLoggingsTable instanceof Table) {
            return $this->_userLoggingsTable;
        }
        $this->_userLoggingsTable = TableRegistry::getTableLocator()->get('UserLoggings');

        return $this->_userLoggingsTable;
    }

    /**
     * Set the user loggings table
     *
     * @param Table $table table
     * @return void
     */
    public function setUserLoggingsTable(Table $table)
    {
        $this->_userLoggingsTable = $table;
    }

    /**
     * Gets the user loggings table instance
     *
     * @return Table
     */
    public function getBankTable()
    {
        if ($this->_bankTable instanceof Table) {
            return $this->_bankTable;
        }
        $this->_bankTable = TableRegistry::getTableLocator()->get('Banks');

        return $this->_bankTable;
    }

    /**
     * Set the user loggings table
     *
     * @param Table $table table
     * @return void
     */
    public function setBankTable(Table $table)
    {
        $this->_bankTable = $table;
    }

    /**
     * Gets category table instance
     *
     * @return Table
     */
    public function getCategoryTable()
    {
        if ($this->_categoryTable instanceof Table) {
            return $this->_categoryTable;
        }
        $this->_categoryTable = TableRegistry::getTableLocator()->get('Categories');

        return $this->_categoryTable;
    }

    /**
     * Set category table
     *
     * @param Table $table table
     * @return void
     */
    public function setCategoryTable(Table $table)
    {
        $this->_categoryTable = $table;
    }

    /**
     * Gets sub category table instance
     *
     * @return Table
     */
    public function getSubCategoryTable()
    {
        if ($this->_subCategoryTable instanceof Table) {
            return $this->_subCategoryTable;
        }
        $this->_subCategoryTable = TableRegistry::getTableLocator()->get('SubCategories');

        return $this->_subCategoryTable;
    }

    /**
     * Set tsub category table
     *
     * @param Table $table table
     * @return void
     */
    public function setSubCategoryTable(Table $table)
    {
        $this->_subCategoryTable = $table;
    }

    /**
     * Gets blacklist table instance
     *
     * @return Table
     */
    public function getBlocklistTable()
    {
        if ($this->_blocklistTable instanceof Table) {
            return $this->_blocklistTable;
        }
        $this->_blocklistTable = TableRegistry::getTableLocator()->get('Blocklists');

        return $this->_blocklistTable;
    }

    /**
     * Set blucklist table
     *
     * @param Table $table table
     * @return void
     */
    public function setBlocklistTable(Table $table)
    {
        $this->_blocklistTable = $table;
    }

    /**
     * Gets finance table instance
     *
     * @return Table
     */
    public function getFinanceTable()
    {
        if ($this->_financeTable instanceof Table) {
            return $this->_financeTable;
        }
        $this->_financeTable = TableRegistry::getTableLocator()->get('Finance');

        return $this->_financeTable;
    }

    /**
     * Set finance table
     *
     * @param Table $table table
     * @return void
     */
    public function setFinanceTable(Table $table)
    {
        $this->_financeTable = $table;
    }

    /**
     * Gets loan table instance
     *
     * @return Table
     */
    public function getLoanTable()
    {
        if ($this->_loanTable instanceof Table) {
            return $this->_loanTable;
        }
        $this->_loanTable = TableRegistry::getTableLocator()->get('Loan');

        return $this->_loanTable;
    }

    /**
     * Set loan table
     *
     * @param Table $table table
     * @return void
     */
    public function setLoanTable(Table $table)
    {
        $this->_loanTable = $table;
    }

    /**
     * Gets user payment table instance
     *
     * @return Table
     */
    public function getUserPaymentTable()
    {
        if ($this->_userPaymentTable instanceof Table) {
            return $this->_userPaymentTable;
        }
        $this->_userPaymentTable = TableRegistry::getTableLocator()->get('UserPayments');

        return $this->_userPaymentTable;
    }

    /**
     * Set user payment table
     *
     * @param Table $table table
     * @return void
     */
    public function setUserPaymentTable(Table $table)
    {
        $this->_userPaymentTable = $table;
    }

    /**
     * Gets payment detail table instance
     *
     * @return Table
     */
    public function getPaymentDetailTable()
    {
        if ($this->_paymentDetailTable instanceof Table) {
            return $this->_paymentDetailTable;
        }
        $this->_paymentDetailTable = TableRegistry::getTableLocator()->get('PaymentDetails');

        return $this->_paymentDetailTable;
    }

    /**
     * Set payment detail table
     *
     * @param Table $table table
     * @return void
     */
    public function setPaymentDetailTable(Table $table)
    {
        $this->_paymentDetailTable = $table;
    }

    /**
     * Gets bank favorite table instance
     *
     * @return Table
     */
    public function getBankFavoriteTable()
    {
        if ($this->_bankFavoriteTable instanceof Table) {
            return $this->_bankFavoriteTable;
        }
        $this->_bankFavoriteTable = TableRegistry::getTableLocator()->get('BankFavorites');

        return $this->_bankFavoriteTable;
    }

    /**
     * Set bank favorite table
     *
     * @param Table $table table
     * @return void
     */
    public function setBankFavoritesTable(Table $table)
    {
        $this->_bankFavoriteTable = $table;
    }

}
