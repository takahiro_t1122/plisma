<?php

namespace App\Controller\Traits;

use App\Model\Table\UsersTable;
use App\Form\LoanItemForm;
use App\Form\UploadFileForm;
use App\Form\UploadPhotoForm;
use Cake\Validation\Validator;
use Cake\Core\Exception\Exception;
use Cake\I18n\Time;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\Exception\InvalidPrimaryKeyException;
use Cake\Routing\Router;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

trait LoanItemTrait
{

    /**
     * Creating Loan Item
     * Reserving input data into session and pass to the confirm page
     */
    public function createLoanItem()
    {
        $this->getCategoryList();
        $session = $this->request->getSession();
        $loginId = $this->Auth->user('id');
        $loan_query = $this->Loan->find()->where(['user_id' => $loginId])->first();
        $finance_query = $this->Finance->find()->where(['user_id' => $loginId])->first();

        $this->set('loginId', $loginId);

        // Allowed to create one loan item per one user
        if (!$loan_query->is_draft && !is_null($loan_query->is_draft)) {
            $this->Flash->error(__('案件登録は1件のみとなります。'));
            $this->redirect($this->request->referer());
            return ;
        }

        if ($this->request->is(['post', 'put'])) {
            $input_data = $this->request->getData();
            // temporary reserve
            if (isset($this->request->data['temp'])) {
                $input_data['is_draft'] = true;
                $session->write('loanData', $input_data);
                $session->write('loanData.sub_category_name', $this->SubCategories->find()->where(['id' => $input_data['sub_category']])->first()->name);
                $this->set('loanData', $session->read('loanData'));
                if ($this->saveLoanItem($input_data)) {
                    $this->Flash->success(__('一時保存致しました。'));
                } else {
                    $this->Flash->error(__('一時保存に失敗致しました。'));
                }
                return ;
            // reserve
            } elseif (isset($this->request->data['reserve'])) {
                $session->write('loanData', $input_data);
                $this->set('loanData', $input_data);
                $form = new LoanItemForm();
                $form->execute($input_data);
                if ($form->getErrors()) {
                    $this->set('form', $form);
                    return ;
                } else {
                    return $this->redirect(['controller' => 'corp', 'action' => 'confirmLoanItem']);
                }
            }
        } else {
            $session->write('loanData', $this->modifyTempDataFormat($loan_query, $finance_query));
            return ;
        }
    }

    /**
     * Getting data from database and reserve to session
     *
     * @param entity $loan_query
     * @param entity $finance_query
     * @return array $temp_data
     */
    private function modifyTempDataFormat($loan_query = null, $finance_query = null)
    {
        if (!empty($loan_query) || !empty($finance_query)) {
            $temp_revenues = unserialize(stream_get_contents($finance_query['revenues']));
            $temp_profits_losses = unserialize(stream_get_contents($finance_query['profits_losses']));
            $temp_data['loan_title'] = $loan_query->loan_title;
            $temp_data['category'] = $loan_query->category_id;
            $temp_data['sub_category'] = $loan_query->sub_category_id;
            $temp_data['sub_category_name'] = $this->SubCategories->find()->where(['id' => $loan_query->sub_category_id])->first()->name;
            $temp_data['job_description'] = $loan_query->job_description;
            $temp_data['desired_loan_amount'] = $loan_query->desired_loan_amount;
            $temp_data['loan_period'] = $loan_query->loan_period;
            $temp_data['loan_purpose'] = $loan_query->loan_purpose;
            $temp_data['loan_purpose_detail'] = $loan_query->loan_purpose_detail;
            $temp_data['prospect'] = $loan_query->prospect;
            $temp_data['assets'] = $finance_query->assets;
            $temp_data['loan_balance'] = $finance_query->loan_balance;
            $temp_data['financial_month'] = $finance_query->financial_month;
            $temp_data['fiscal_year_one'] = array_keys($temp_revenues)[0];
            $temp_data['revenue_one'] = $temp_revenues[array_keys($temp_revenues)[0]];
            $temp_data['profits_losses_one'] = $temp_profits_losses[array_keys($temp_revenues)[0]];
            $temp_data['fiscal_year_two'] =array_keys($temp_revenues)[1];
            $temp_data['revenue_two'] = $temp_revenues[array_keys($temp_revenues)[1]];
            $temp_data['profits_losses_two'] = $temp_profits_losses[array_keys($temp_revenues)[1]];
            $temp_data['fiscal_year_three'] = array_keys($temp_revenues)[2];
            $temp_data['revenue_three'] = $temp_revenues[array_keys($temp_revenues)[2]];
            $temp_data['profits_losses_three'] = $temp_profits_losses[array_keys($temp_revenues)[2]];
            return $temp_data;
        } else {
            return null;
        }
    }

    /**
     * Confirm registered information
     */
    public function confirmLoanItem()
    {
        $input_data = $this->request->getSession()->read('loanData');

        if ($this->request->is(['post', 'put'])) {
            $tmp = $this->request->getData();
            $input_data['mode'] = $this->request->getData('mode');
        }

        if ($input_data['mode'] == 'confirm') {
            // modifying data format
            $input_data = $this->modifyArrayDataFormat($input_data);
            if ($input_data['loan_purpose'] == 0) {
                $input_data['loan_purpose_view'] = '運転資金';
            } else if ($input_data['loan_purpose'] == 1) {
                $input_data['loan_purpose_view'] = '設備資金';
            } else {
                $input_data['loan_purpose_view'] = 'その他';
            }
            $input_data['loan_purpose'] = '';
            $input_data['category_view'] = $this->Categories->find()->where(['id' => $input_data['category']])->first()->name;
            $input_data['sub_category_view'] = $this->SubCategories->find()->where(['id' => $input_data['sub_category']])->first()->name;
            $input_data['user_id'] = $this->Auth->user('id');
            $this->set('loanData', $input_data);
            $this->render('confirm_loan_item');
        } else if ($input_data['mode'] == 'complete') {
            $input_data['is_draft'] = false;
            $this->request->getSession()->write('loanData', $input_data);

            if ($this->saveLoanItem($input_data)) {
                $this->Flash->success(__('案件登録が完了致しました。非公開先銀行を設定してください。'));
                $loan_id = $this->Loan->find()->where(['user_id' => $this->Auth->user('id')])->first()->id;
                return $this->redirect(['controller' => 'corp', 'action' => 'viewBlock', 'loan_id' => $loan_id]);
            } else {
                $this->Flash->error(__('案件登録に失敗致しました。'));
                return ;
            }
        } else {
            $this->Flash->error(__('予期せぬエラーが発生しました。'));

            return $this->redirect($this->request->referer());
        }
    }

    /**
     * After confirming the registered data,
     * this method is called and the data is registered into Database
     * The registered session will be deleted after registering data
     *
     * @param array input_data
     * @return boolean
     */
    public function saveLoanItem($input_data)
    {
        $loginId = $this->Auth->user('id');

        if (loan_data['is_draft']) {
            $input_data = $this->modifyArrayDataFormat($input_data);
        }
        $revenues = [
            $input_data['fiscal_year_one'] => $input_data['revenue_one'],
            $input_data['fiscal_year_two'] => $input_data['revenue_two'],
            $input_data['fiscal_year_three'] => $input_data['revenue_three']];
        $profits_losses = [
            $input_data['fiscal_year_one'] => $input_data['profits_losses_one'],
            $input_data['fiscal_year_two'] => $input_data['profits_losses_two'],
            $input_data['fiscal_year_three'] => $input_data['profits_losses_three']];
        $latest_year = max(array_keys($revenues));
        $category_id = ($input_data['category'] === '') ? null : $input_data['category'];
        $sub_category_id = ($input_data['sub_category'] === '') ? null : $input_data['sub_category'];

        $finance_data = [
            'user_id' => $loginId,
            'assets' => str_replace(',','', $input_data['assets']),
            'financial_month' => $input_data['financial_month'],
            'latest_revenue' => str_replace(',','', $revenues[$latest_year]),
            'latest_profit_lose' => str_replace(',','', $profits_losses[$latest_year]),
            'revenues' => serialize($revenues),
            'profits_losses' => serialize($profits_losses),
            'loan_balance' => str_replace(',','', $input_data['loan_balance']),
            'is_draft' => $input_data['is_draft']];
        $loan_data = [
            'user_id' => $loginId,
            'loan_title' => $input_data['loan_title'],
            'category_id' => $category_id,
            'sub_category_id' => $sub_category_id,
            'job_description' => $input_data['job_description'],
            'desired_loan_amount' => str_replace(',','', $input_data['desired_loan_amount']),
            'loan_period' => $input_data['loan_period'],
            'loan_purpose' => $input_data['loan_purpose'],
            'loan_purpose_detail' => $input_data['loan_purpose_detail'],
            'prospect' => $input_data['prospect'],
            'is_draft' => $input_data['is_draft']];

        try {
            $loan_query = $this->Loan->find()->where(['user_id' => $loginId])->first();
            $finance_query = $this->Finance->find()->where(['user_id' => $loginId])->first();

            // make validation invalid when the data is for draft
            if ($loan_data['is_draft']) {
                $validation = ['validate' => 'draft'];
            } else {
                $validation = ['validate' => true];
            }

            if ($loan_query != null) {
                $loan_entity = $this->Loan->patchEntity($loan_query, $loan_data, $validation);
            } else {
                $loan_entity = $this->Loan->newEntity($loan_data, $validation);
            }
            if ($finance_query != null) {
                $finance_entity = $this->Finance->patchEntity($finance_query, $finance_data, $validation);
            } else {
                $finance_entity = $this->Finance->newEntity($finance_data, $validation);
            }

            if (!$finance_entity->getErrors() && !$loan_entity->getErrors()){
                $this->Finance->save($finance_entity);
                $this->Loan->save($loan_entity);
                if (!$loan_data['is_draft']) {
                    $this->request->getSession()->delete('loanData');
                }
                return true;
            } else {
                return false;
            }
        } catch(Exception $e) {
            $message = $e->getMessage();
            $this->Flash->error($message);

            $this->redirect($this->request->referer());
        }
    }

    /**
     * Returning Loan information and Finance information of logging user
     *
     * @return entity $entities
     */

    public function viewLoanItem()
    {
        $loginId = $this->Auth->user('id');

        try {
            if ($this->Loan->exists(['user_id' => $loginId, 'is_draft' => false])) {
                $loan = $this->Loan->find()->where(['user_id' => $loginId])->contain(['Categories', 'SubCategories'])->first();

                if ($loan['loan_purpose'] == 0) {
                    $loan['loan_purpose_view'] = '運転資金';
                } else if ($loan['loan_purpose'] == 1) {
                    $loan['loan_purpose_view'] = '設備資金';
                } else {
                    $loan['loan_purpose_view'] = 'その他';
                }

                // block banks
                if ($this->Blocklists->exists(['loan_id' => $loan->id])) {
                    $blockbanks = $this->Blocklists->find('all')->where(['loan_id' => $loan->id])->contain(['banks'])->toArray();
                }
            }
            if ($this->Finance->exists(['user_id' => $loginId, 'is_draft' => false])){
                $finance = $this->Finance->find()->where(['user_id' => $loginId, 'is_draft' => false])->first();
                $finance['revenues'] = unserialize(stream_get_contents($finance['revenues']));
                $finance['profits_losses'] = unserialize(stream_get_contents($finance['profits_losses']));
            }

            $entities = [
                'finance' => $finance,
                'loan' => $loan,
                'blockbanks' => $blockbanks
            ];
            $this->set('entities', $entities);

            // upload file check
            //ToDo change
            $dir = new Folder(STORAGE_ROOT . $loan->id);
            $files = $dir->find();
            $this->set(compact('files'));

        if ($this->request->is('post') && isset($this->request->data['upload_file'])) {
            $fileform = $this->fileUpload($loan->id);
            $this->set(compact('fileform'));
        }

        } catch (RecordNotFoundException $ex) {
            $message = $ex->getMessage();
            $this->Flash->error($message);
            return $this->redirect($this->request->referer());
        } catch (InvalidPrimaryKeyException $ex) {
            $message = $ex->getMessage();
            $this->Flash->error($message);
            return $this->redirect($this->request->referer());
        }
    }

    public function fileUpload($loan_id = null)
    {
        $fileform = new UploadFileForm();
        $file = $this->request->data;
        if ($this->request->is('post') && isset($file['upload_file'])) {
            if ($fileform->validate($file)) {
                //ToDo change file save path to AWS S3
                $dir = new Folder(STORAGE_ROOT . $loan_id . DS, true, 0777);
                move_uploaded_file($file['upload_file']['tmp_name'],
                    sprintf($dir->path.'%s', $this->request->data['upload_file']['name']));
                $this->Flash->success(__('資料のアップロードが完了しました。'));
                return $this->redirect($this->request->referer());
            } else {
                $this->Flash->error(__('資料のアップロードに失敗しました。'));
                return $fileform;
            }
        }
    }

    public function fileDownload()
    {
        $this->autoRender = false;
        $loan_id = $this->request->getQuery('loan_id');
        $file_name = $this->request->getQuery('file_name');

        // ToDo change
        $dir = STORAGE_ROOT . $loan_id . DS . $file_name;
        $this->response->file($dir);
        $this->response->download($file_name);

        return ;
    }

    public function fileDelete()
    {
        $this->autoRender = false;
        $loan_id = $this->request->getQuery('loan_id');
        $file_name = $this->request->getQuery('file_name');

        // ToDo change
        $file = new File(STORAGE_ROOT . $loan_id . DS . $file_name);
        if ($file->delete()){
            $this->Flash->success(__('ファイルを削除致しました。'));
        } else {
            $this->Flash->error(__('ファイルを削除出来ませんでした。'));
        }
        return $this->redirect($this->request->referer());
    }

    /**
     * Make the visible status change
     * getting loan id from request parameter
     * @return
     */
    public function visibleStatusChange()
    {
        $this->autoRender = false;

        $loginId = $this->Auth->user('id');
        $loan_id = $this->request->query('loan_id');
        $status = $this->request->query('status');

        try {
            $loan = $this->Loan->find()->where(['id' => $loan_id, 'user_id' => $loginId, 'is_draft' => false])->first();

            if (is_null($loan)) {
                $this->Flash->error(__('データが登録されていません。'));
                return $this->redirect($this->request->referer());
            }

            $current_visible = $loan->is_visible;
            if ($status == null) {
                $update_visible = ['is_visible' => ($current_visible == false) ? true : false];
            } else if ($status == "open") {
                $update_visible = ['is_visible' => true];
            } else if ($status == "hide") {
                $update_visible = ['is_visible' => false];
            }
            if ($update_visible['is_visible']) {
                $update_visible['visible_from'] = Time::now();
            }
            $loan_entity = $this->Loan->patchEntity($loan, $update_visible);
            if ($saved_data = $this->Loan->save($loan_entity)) {
                if ($status == null) {
                    $this->Flash->success(__('ステータスの更新が完了しました。'));
                } else {
                    $this->Flash->success(__('案件登録が完了しました'));
                }
                return $this->redirect(['controller' => 'corp', 'action' => 'viewLoanItem']);
                return ;
            } else {
                $this->Flash->error(__('予期せぬエラーが発生しました。'));

                return $this->redirect($this->request->referer());
            }
        } catch(Exception $e) {
            $message = $e->getMessage();
            $this->Flash->error($message);

            $this->redirect($this->request->referer());
        }
    }

    /**
     * Delete loan item
     */
     public function deleteLoanItem()
     {
        $this->autoRender = false;

        $loginId = $this->Auth->user('id');
        $loan_id = $this->request->query('loan_id');
        $delte_blocklists = true;
        $delete_bankfav = true;

        try {
            $loan = $this->Loan->find()->where(['id' => $loan_id, 'user_id' => $loginId, 'is_draft' => false])->first();
            if (is_null($loan)) {
                $this->Flash->error(__('データが登録されていません。'));

                return $this->redirect($this->request->referer());
            }
            // delete block bank lists first
            if ($this->Blocklists->exists(['loan_id' => $loan_id])) {
                $delte_blocklists = $this->Blocklists->deleteAll(['loan_id' => $loan_id]);
            }
            if ($this->BankFavorites->exists(['loan_id' => $loan_id])) {
                $delete_bankfav = $this->BankFavorites->deleteAll(['loan_id' => $loan_id]);
            }

            // check header image exisit
            $header_img = LOAN_HEADER_DIR . $loginId . PNG;
            if (file_exists($header_img)) {
                $this->deleteLoanImage($loginId);
            }
            // delete loan item second
            if ($delte_blocklists && $delete_bankfav && $this->Loan->delete($loan)) {
                $this->Flash->success(__('データの削除が完了しました。'));
                $this->redirect($this->request->referer());
                return ;
            } else {
                $this->Flash->error(__('予期せぬエラーが発生しました。'));

                return $this->redirect($this->request->referer());
            }
        } catch(Exception $e) {
            $message = $e->getMessage();
            $this->Flash->error($message);

            $this->redirect($this->request->referer());
        }

     }

    /**
     * Update Loan item
     */
    public function updateLoanItem()
    {
        $this->getCategoryList();

        $loginId = $this->Auth->user('id');
        $loan_id = $this->request->query('loan_id');

        try {
            $table = $this->Loan;
            $tableAlias = $table->getAlias();
            $entity = $table->find()->where(['id' => $loan_id, 'user_id' => $loginId, 'is_draft' => false])->first();
            if (is_null($entity)) {
                $this->Flash->error(__('データが登録されていません。'));

                return $this->redirect($this->request->referer());
            }
            $entity['category'] = $entity->category_id;
            $entity['sub_category'] = $entity->sub_category_id;
            $entity['sub_category_name'] = $this->SubCategories->find()->where(['id' => $entity->sub_category_id])->first()->name;

            $this->set($tableAlias, $entity);
            $this->set('tableAlias', $tableAlias);
            $this->set('_serialize', [$tableAlias, 'tableAlias']);
            if (!$this->request->is(['patch', 'post', 'put'])) {
                return;
            }
            if (!empty($this->request->getData('category') && !empty($this->request->getData('sub_category')))) {
                $entity['category_id'] = $this->request->getData('category');
                $entity['sub_category_id'] = $this->request->getData('sub_category');
            }
            $entity = $table->patchEntity($entity, $this->request->getData());
            if ($table->save($entity)) {
                $this->Flash->success(__('案件情報の更新が完了しました。'));

                return $this->redirect(['controller' => 'corp', 'action' => 'viewLoanItem']);
            }
            $this->Flash->error(__('案件情報の更新に失敗しました。'));
        } catch (RecordNotFoundException $ex) {
            $message = $ex->getMessage();
            $this->Flash->error($message);
            return $this->redirect($this->request->referer());
        } catch (InvalidPrimaryKeyException $ex) {
            $message = $ex->getMessage();
            $this->Flash->error($message);
            return $this->redirect($this->request->referer());
        }

    }

    public function uploadLoanImage()
    {
        $fileform = new UploadPhotoForm();
        if ($this->request->is('ajax') && isset($_FILES['blob'])) {
            $res = [];
            $loginId = $this->Auth->user('id');
            try {
                if ($fileform->validate($_FILES)) {
                    $blob_img = $_FILES['blob'];
                    $dir = new Folder(LOAN_HEADER_DIR, true, 0777);
                    $file_path = $dir->path . DS . $loginId . PNG;

                    move_uploaded_file($blob_img['tmp_name'], $file_path);

                    $res = [
                        'status' => 'success',
                        'msg' => null,
                        'image_path' => $file_path,
                    ];
                } else {
                    $this->Flash->error($fileform->_errors);
                    return $this->redirect($this->request->referer());
                }
            } catch (Exception $ex) {
                $res = [
                    'status' => 'error',
                    'msg' => $ex->getMessge(),
                    'image_path' => null,
                ];
            }
            echo json_encode($res, JSON_PRETTY_PRINT);
        }
    }

    public function deleteLoanImage($loginId = null)
    {
        $this->autoRender = false;
        if ($loginId == null) {
            $loginId = $this->request->getQuery('loginId');
        }

        // ToDo change
        $file = new File(STORAGE_ROOT . 'loan_header' . DS . $loginId . PNG);
        if (!$file->delete()) {
            $this->Flash->error(__('画像を削除出来ません。'));
        }

        return $this->redirect($this->request->referer());
    }
}