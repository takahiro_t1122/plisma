<?php

namespace App\Controller\Traits;

trait BlocklistsTrait
{
    /**
     * Blocking banks of viewing loan items
     * Allows you to block and remove block banks
     *
     * @param string loan_id
     * @return array block_banks
     */
    public function viewBlock()
    {
        $loan_id = $this->request->query('loan_id');
        $loan = $this->Loan->get($loan_id);
        $this->set('loan', $loan);
        if ($loan_id == null) {
            $this->Flash->error(__('案件情報を取得することが出来ませんでした。'));
            return $this->redirect($this->request->referer());
        }

        // get bank list
        $this->getBankList();

        if ($this->request->is(['post', 'put'])) {
            // for addition
            if (isset($this->request->data['block'])) {
                $block_bank_id = $this->request->getData('bank_id');
                if (is_null($block_bank_id) || $block_bank_id == "") {
                    $this->Flash->error(__('銀行を選択してください。'));
                    return $this->redirect($this->request->referer());
                }
                if ($this->Blocklists->exists(['loan_id' => $loan_id, 'bank_id' => $block_bank_id])){
                    $this->Flash->error(__('選択された銀行は、すでにブロックリストに入っています。'));
                } else {
                    $insert_data = ['loan_id' => $loan_id, 'bank_id' => $block_bank_id];
                    $block_entity = $this->Blocklists->newEntity($insert_data);
                    if ($saved_data = $this->Blocklists->save($block_entity)) {
                        $this->Flash->success(__('ブロックリストに追加致しました。'));
                    } else {
                        $this->Flash->error(__('ブロックリストへの追加に失敗致しました。'));
                    }
                }
            // for deletion
            } else if (isset($this->request->data['remove_block'])) {
                $unblock_banks = $this->request->getData('unblock');
                if ($this->Blocklists->deleteAll(['loan_id' => $loan_id, 'bank_id IN' => $unblock_banks])) {
                    $this->Flash->success(__('選択された銀行をブロックリストから削除致しました。'));
                } else {
                    $this->Flash->error(__('選択された銀行はブロックリストから削除出来ませんでした。'));
                }
            }
            return $this->redirect($this->request->referer());
        // show every blocking banks
        } else {
            $block_banks = $this->Blocklists->find()->where(['loan_id' => $loan_id])->contain(['banks'])->toArray();
            $this->set('blockbanks', $block_banks);
        }
    }

    /**
     * Ajax function for returning bank list
     * input parameter should be the initial characters of bank name
     *
     * @return array bank_list
     */
    public function getBankListBySearch()
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $bank_name = $this->request->data('search_value');
            $bank_list = $this->Banks->find()->where(['banks.bank_name LIKE' => $bank_name.'%'])->toArray();
            $this->response->body(json_encode($bank_list, JSON_UNESCAPED_UNICODE));
            return ;
        } else {
            return $this->redirect($this->request->referer());
        }
    }
}