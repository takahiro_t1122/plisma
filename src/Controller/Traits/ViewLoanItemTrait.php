<?php

namespace App\Controller\Traits;

use Cake\Cache\Cache;
use Cake\Routing\Router;

trait ViewLoanItemTrait
{

    /**
     * Functions for search conditions
     * After this function, arguments remain to home action
     *
     * @param string query
     */
    public function itemSearchConditions()
    {
        $area_options = $this->area_options();
        $this->set(compact('area_options'));

        //delete session
        $this->request->getSession()->delete('param');

        // getting category / sub category list
        $category_list = $this->Categories->find()->toArray();
        $sub_category_list = $this->SubCategories->find()->toArray();
        $this->set(compact('category_list'));
        $this->set(compact('sub_category_list'));

        // getting data for bank blocking
        $bank_id = $this->Auth->user('bank_id');
        $view_block = $this->Blocklists->find()->where(['bank_id' => $bank_id])->toArray();
        $block_loan_item = [];
        foreach($view_block as $item) {
            $block_loan_item[] = $item['loan_id'];
        }

        if ($this->request->is('get') && $this->request->query()) {
            $search_param = $this->request->query();
            $this->request->getSession()->write('param', $search_param);
            return $this->redirect(['controller' => 'bank', 'action' => 'search']);
        }
    }

    public function search()
    {
        $area_options = $this->area_options();
        $this->set(compact('area_options'));

        // getting category / sub category list
        $category_list = $this->Categories->find()->toArray();
        $sub_category_list = $this->SubCategories->find()->toArray();
        $this->set(compact('category_list'));
        $this->set(compact('sub_category_list'));

        // getting data for bank blocking
        $bank_id = $this->Auth->user('bank_id');
        $view_block = $this->Blocklists->find()->where(['bank_id' => $bank_id])->toArray();
        $block_loan_item = [];
        foreach($view_block as $item) {
            $block_loan_item[] = $item['loan_id'];
        }

        try {
            $session = $this->request->getSession();
            // when the search filter is pressed
            if ($this->request->query() && !$this->request->query('sort')) {
                $search_param = $this->request->query();
                // for when the sort session exist
                if ($session->read('param.sort')) {
                    $search_param['sort'] = $session->read('param.sort');
                }
                if ($this->request->query('category') || $this->request->query('sub_category')) {
                    $search_param['area'] = $session->read('param.area');
                    $search_param['employees_min'] = $session->read('param.employees_min');
                    $search_param['employees_max'] = $session->read('param.employees_max');
                    $search_param['desired_loan_amount_min'] = $session->read('param.desired_loan_amount_min');
                    $search_param['desired_loan_amount_max'] = $session->read('param.desired_loan_amount_max');
                    $search_param['loan_period_min'] = $session->read('param.loan_period_min');
                    $search_param['loan_period_max'] = $session->read('param.loan_period_max');
                    $search_param['loan_purpose'] = $session->read('param.loan_purpose');
                } else {
                    $search_param['category'] = $session->read('param.category');
                    $search_param['sub_category'] = $session->read('param.sub_category');
                }
                $session->write('param', $search_param);
            // when the sort filter is pressed
            } else if ($this->request->query('sort')) {
                // for when the search session exist
                $search_param = $session->read('param');
                $search_param['sort'] = $this->request->query('sort');

                $session->write('param.sort', $search_param['sort']);
            } else if (!$this->request->query() && $session->read('param')) {
                $referer = substr($this->request->referer(), 0, strcspn($this->request->referer(),'?'));
                if ($referer == Router::url(NULL, true)) {
                    $session->delete('param.category');
                    $session->delete('param.sub_category');
                }
                $search_param = $session->read('param');
            } else {
                $search_param = '';
            }

            $random_num = $session->read('seed');

            // get data from database
            $query = $this->Loan->find('search', ['search' => $search_param])
                ->where([
                    'Loan.is_accomplished' => false,
                    'Loan.is_visible' => true,
                    'Loan.is_draft' => false])
                ->contain(['Categories', 'SubCategories', 'users']);
            if (!empty($block_loan_item)) {
                $query = $query->where(['Loan.id NOT IN' => $block_loan_item]);
            }
            // filter
            if (!empty(array_filter($search_param, 'strlen')) || !empty($search_param['sub_category']) || !empty($search_param['loan_purpose'])) {
                // category / sub_category
                if ($sub_category_array = $search_param['sub_category']) {
                    $query = $query->where(function($exp) use ($sub_category_array) {
                        return $exp->in('SubCategories.id', $sub_category_array);
                    });
                    //returns label
                    $category_array = $search_param['category'];
                    $subcategory_list = $this->SubCategories->find()->where(function($exp) use ($sub_category_array) {
                            return $exp->in('SubCategories.id', $sub_category_array);
                        })->contain(['Categories'])->toArray();
                    $this->set(compact('category_array'));
                    $this->set(compact('subcategory_list'));
                }
                // area
                if ($search_param['area']) {
                    $query = $query->where(['users.company_address1' => $search_param['area']]);
                }
                // employees
                if ($search_param['employees_min'] || $search_param['employees_max']) {
                    $query = $this->returnValueByRange($query, 'users.staff_count', $search_param['employees_min'], $search_param['employees_max']);
                }
                // desired loan amount
                if ($search_param['desired_loan_amount_min'] || $search_param['desired_loan_amount_max']) {
                    $query = $this->returnValueByRange($query, 'loan.desired_loan_amount', $search_param['desired_loan_amount_min'], $search_param['desired_loan_amount_max']);
                }
                // loan period
                if ($search_param['loan_period_min'] || $search_param['loan_period_max']) {
                    $query = $this->returnValueByRange($query, 'loan.loan_period', $search_param['loan_period_min'], $search_param['loan_period_max']);
                }
                // loan purpose
                if ($purpose_array = $search_param['loan_purpose']) {
                    $query = $query->where(function($exp) use ($purpose_array) {
                        return $exp->in('loan.loan_purpose', $purpose_array);
                    });
                }
                // sort
                if ($search_param['sort']) {
                    if ($search_param['sort'] == 'release_desc') {
                        $this->paginate['order'] = ['Loan.visible_from' => 'desc'];
                    } else if ($search_param['sort'] == 'view_desc') {
                        $this->paginate['order'] = ['Loan.viewed' => 'desc'];
                    } else if ($search_param['sort'] == 'view_asc') {
                        $this->paginate['order'] = ['Loan.viewed' => 'asc'];
                    }
                } else {
                    $this->paginate['order'] = 'RAND("$random_num")';
                }
            } else {
                $session->delete('param');
            }
            $this->set('loans', $this->paginate($query));

            // favorite items list
            $bank_favorite = $this->BankFavorites->find()->where(['user_id' => $this->Auth->user('id')])->toArray();
            foreach ($bank_favorite as $item) {
                $loan_list[] = $item['loan_id'];
            }
            $this->set('bank_favorite', $loan_list);

        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Common function for limitting the returning value
     * getting minimum range or maximum value
     *
     * @param entity $query
     * @param column_name $target
     * @param numeric $min
     * @param numeric $max
     * @return entity $query
     */
    public function returnValueByRange($query, $target, $min, $max)
    {
        if ($min && !$max) {
            $query = $query->where(function ($exp) use ($target, $min) {
                return $exp->gte($target, $min);
              });
        } else if (!$min && $max) {
            $query = $query->where(function ($exp) use ($target, $max) {
                return $exp->lte($target, $max);
              });
        } else {
            $query = $query->where(function ($exp) use ($target, $min, $max) {
                return $exp->between($target, $min, $max);
            });
        }
        return $query;
    }

    public function area_options()
    {
        $area_options = [
            '' => '',
            '北海道地方' => [
                '北海道' => '北海道',
            ],
            '東北地方' => [
                '青森県' => '青森県',
                '岩手県' => '岩手県',
                '秋田県' => '秋田県',
                '宮城県' => '宮城県',
                '山形県' => '山形県',
                '福島県' => '福島県',
            ],
            '関東地方' => [
                '茨城県' => '茨城県',
                '栃木県' => '栃木県',
                '群馬県' => '群馬県',
                '埼玉県' => '埼玉県',
                '千葉県' => '千葉県',
                '東京都' => '東京都',
                '神奈川県' => '神奈川県',
            ],
            '中部地方' => [
                '山梨県' => '山梨県',
                '長野県' => '長野県',
                '新潟県' => '新潟県',
                '富山県' => '富山県',
                '石川県' => '石川県',
                '福井県' => '福井県',
                '静岡県' => '静岡県',
                '愛知県' => '愛知県',
                '岐阜県' => '岐阜県',
            ],
            '近畿地方' => [
                '三重県' => '三重県',
                '滋賀県' => '滋賀県',
                '京都府' => '京都府',
                '大阪府' => '大阪府',
                '兵庫県' => '兵庫県',
                '奈良県' => '奈良県',
                '和歌山県' => '和歌山県',
            ],
            '中国地方' => [
                '鳥取県' => '鳥取県',
                '島根県' => '島根県',
                '岡山県' => '岡山県',
                '広島県' => '広島県',
                '山口県' => '山口県',
            ],
            '四国地方' => [
                '香川県' => '香川県',
                '愛媛県' => '愛媛県',
                '徳島県' => '徳島県',
                '高知県' => '高知県',
            ],
            '九州・沖縄地方' => [
                '福岡県' => '福岡県',
                '佐賀県' => '佐賀県',
                '長崎県' => '長崎県',
                '熊本県' => '熊本県',
                '大分県' => '大分県',
                '宮崎県' => '宮崎県',
                '鹿児島県' => '鹿児島県',
                '沖縄県' => '沖縄県',
            ],
        ];

        return $area_options;
    }

    /**
     * To see the loan item detail function
     * This controller returns loan and finance entity,
     * view count will be passed from cache and will be reserved into database every 10 views
     *
     * @param string loan_id
     * @return entity entities
     */
    public function viewDetail()
    {
        $loan_id = $this->request->query('id');
        $loan = $this->Loan->find()->where([
                'Loan.id' => $loan_id,
                'Loan.is_accomplished' => false,
                'Loan.is_visible' => true,
                'is_draft' => false])
            ->contain(['Categories', 'SubCategories', 'users'])->first();
        $finance = $this->Finance->find()->where(['user_id' => $loan->user->id, 'is_draft' => false])->first();
        $finance['revenues'] = unserialize(stream_get_contents($finance['revenues']));
        $finance['profits_losses'] = unserialize(stream_get_contents($finance['profits_losses']));

        if (is_null($loan) || is_null($finance)) {
            $this->Flash->error(__('データが確認できません。'));
            return $this->redirect($this->request->referer());
        }

        $user_id = $this->Auth->user('id');
        $loan['is_favorited'] = $this->BankFavorites->exists(['user_id' => $user_id, 'loan_id' => $loan_id]);

        if ($loan['loan_purpose'] == 0) {
            $loan['loan_purpose_view'] = '運転資金';
        } else if ($loan['loan_purpose'] == 1) {
            $loan['loan_purpose_view'] = '設備資金';
        } else {
            $loan['loan_purpose_view'] = 'その他';
        }

        $entities = [
            'finance' => $finance,
            'loan' => $loan
        ];
        $this->set('entities', $entities);

        // reserve view count into cache
        if (Cache::read($loan_id) === false) {
            // initial value
            Cache::write($loan_id, 0);
        }

        $current_url = Router::url(NULL, true);
        $referer_url = $this->request->referer();

        if (($current_url != $referer_url)) {
            $view_cache = Cache::increment($loan_id); // view count in cache
            $view_db = $loan->viewed; // view count in db
            if ($view_cache < $view_db) {
                $view_count = $view_db;
                Cache::write($loan_id, $view_db);
            } else {
                $view_count = $view_cache;
            }

            // reserve view count into db every 10 views
            if ($view_count % 10 == 0) {
                $loan_entity = $this->Loan->patchEntity($loan, ['viewed' => $view_count]);
                //TODO
                if ($saved_data = $this->Loan->save($loan_entity)) {
                    return ;
                } else {
                }
            }
        }
    }
}