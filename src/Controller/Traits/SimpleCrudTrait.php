<?php

namespace App\Controller\Traits;

use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\Utility\Inflector;
use Cake\Core\Configure;
use DateTime;

/**
 * Covers the baked CRUD actions, note we could use Crud Plugin too
 *
 * @property \Cake\Http\ServerRequest $request
 */
trait SimpleCrudTrait
{
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return mixed Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $table = $this->loadModel();
        $tableAlias = $table->getAlias();
        $entity = $table->get($id, [
            'contain' => []
        ]);

        if ($entity->role == ROLE_BANK) {
            $entity['bank_name'] = $this->getBankTable()->get($entity->bank_id)->bank_name;
        }
        $this->set($tableAlias, $entity);
        $this->set('tableAlias', $tableAlias);
        $this->set('_serialize', [$tableAlias, 'tableAlias']);
        if (!$this->request->is(['patch', 'post', 'put'])) {
            return;
        }
        $entity = $table->patchEntity($entity, $this->request->getData());
        $singular = Inflector::singularize(Inflector::humanize($tableAlias));
        if ($table->save($entity)) {
            $this->Flash->success(__d('CakeDC/Users', 'The {0} has been saved', $singular));

            return $this->redirect(['controller' => 'users', 'action' => 'profile']);
        }
        $this->Flash->error(__d('CakeDC/Users', 'The {0} could not be saved', $singular));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return Response Redirects to index.
     * @throws NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $table = $this->loadModel();
        $tableAlias = $table->getAlias();
        $entity = $table->get($id, [
            'contain' => []
        ]);
        $entity['active'] = false;
        $singular = Inflector::singularize(Inflector::humanize($tableAlias));

        // Invisible all loan items
        $this->loadModel('Loan');
        $invisible_count = $this->Loan->updateAll(
            // update columns
            ['is_visible' => false,
            'modified' => new DateTime()],
            // conditions
            ['user_id' => $id]);
        $loan_count = $this->Loan->find()->where(['user_id' => $id])->count();

        if ($table->save($entity) && ($invisible_count == $loan_count)) {
            $this->Flash->success(__d('CakeDC/Users', 'The {0} has been deleted', $singular));
        } else {
            $this->Flash->error(__d('CakeDC/Users', 'The {0} could not be deleted', $singular));
        }

        return $this->logout();
    }
}
