<?php

namespace App\Controller\Traits;

use Cake\Controller\Component\AuthComponent;
use Cake\Core\Configure;
use Cake\Datasource\Exception\InvalidPrimaryKeyException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Covers the profile action
 *
 * @property \Cake\Http\ServerRequest $request
 */
trait ProfileTrait
{

    /**
     * Profile action
     * @param mixed $id Profile id object.
     * @return mixed
     */
    public function profile($id = null)
    {
        $loggedUserId = $this->Auth->user('id');
        $isCurrentUser = false;
        if (!Configure::read('Users.Profile.viewOthers') || empty($id)) {
            $id = $loggedUserId;
        }
        try {
            if ($user->id === $loggedUserId) {
                $isCurrentUser = true;
            }
            $user = $this->Users->find()->where(['users.id' => $id])->contain(['banks'])->first();
        } catch (RecordNotFoundException $ex) {
            $this->Flash->error(__d('CakeDC/Users', 'User was not found'));

            return $this->redirect($this->request->referer());
        } catch (InvalidPrimaryKeyException $ex) {
            $this->Flash->error(__d('CakeDC/Users', 'Not authorized, please login first'));

            return $this->redirect($this->request->referer());
        }
        $this->set(compact('user', 'isCurrentUser'));
        $this->set('_serialize', ['user', 'isCurrentUser']);
    }
}
