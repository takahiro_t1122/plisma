<?php

namespace App\Controller\Traits;

use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Datasource\Exception\InvalidPrimaryKeyException;

trait FinanceTrait
{
    public function createFinanceItem()
    {
    }

    public function updateFinanceItem()
    {
        $loginId = $this->Auth->user('id');
        $finance_id = $this->request->query('finance_id');

        try {
            $table = $this->Finance;
            $tableAlias = $table->getAlias();
            $entity = $table->find()->where(['id' => $finance_id, 'user_id' => $loginId, 'is_draft' => false])->first();
            $entity['revenues'] = unserialize(stream_get_contents($entity['revenues']));
            $entity['profits_losses'] = unserialize(stream_get_contents($entity['profits_losses']));
            if (is_null($entity)) {
                $this->Flash->error(__('データが登録されていません。'));

                return $this->redirect($this->request->referer());
            }

            $this->set($tableAlias, $entity);
            $this->set('tableAlias', $tableAlias);
            $this->set('_serialize', [$tableAlias, 'tableAlias']);
            if (!$this->request->is(['patch', 'post', 'put'])) {
                return;
            }

            $input_data = $this->modifyArrayDataFormat($this->request->getData());
            $revenues = [
                $input_data['fiscal_year_one'] => $input_data['revenue_one'],
                $input_data['fiscal_year_two'] => $input_data['revenue_two'],
                $input_data['fiscal_year_three'] => $input_data['revenue_three']];
            $profits_losses = [
                $input_data['fiscal_year_one'] => $input_data['profits_losses_one'],
                $input_data['fiscal_year_two'] => $input_data['profits_losses_two'],
                $input_data['fiscal_year_three'] => $input_data['profits_losses_three']];
            $latest_year = max(array_keys($revenues));
            $category_id = ($input_data['category'] === '') ? null : $input_data['category'];
            $sub_category_id = ($input_data['sub_category'] === '') ? null : $input_data['sub_category'];

            $finance_data = [
                'assets' => str_replace(',','', $input_data['assets']),
                'latest_revenue' => str_replace(',','', $revenues[$latest_year]),
                'latest_profit_lose' => str_replace(',','', $profits_losses[$latest_year]),
                'revenues' => serialize($revenues),
                'profits_losses' => serialize($profits_losses),
                'loan_balance' => str_replace(',','', $input_data['loan_balance'])];

            $entity = $table->patchEntity($entity, $finance_data);
            if ($table->save($entity)) {
                $this->Flash->success(__('財務情報の更新が完了しました。'));

                return $this->redirect(['controller' => 'corp', 'action' => 'viewLoanItem']);
            }
            $this->Flash->error(__('財務情報の更新に失敗しました。'));
        } catch (RecordNotFoundException $ex) {
            $message = $ex->getMessage();
            $this->Flash->error($message);
            return $this->redirect($this->request->referer());
        } catch (InvalidPrimaryKeyException $ex) {
            $message = $ex->getMessage();
            $this->Flash->error($message);
            return $this->redirect($this->request->referer());
        }
    }

    /**
     * Delete finance item
     */
    public function deleteFinanceInfo()
    {
       $this->autoRender = false;

       $loginId = $this->Auth->user('id');
       $finance_id = $this->request->query('finance_id');

       try {
           $finance = $this->Finance->find()->where(['id' => $finance_id, 'user_id' => $loginId, 'is_draft' => false])->first();
           if (is_null($finance)) {
            $this->Flash->error(__('データが登録されていません。'));

            return $this->redirect($this->request->referer());
        }

           if ($result = $this->Finance->delete($finance)) {
               $this->Flash->success(__('データの削除が完了しました。'));
               $this->redirect($this->request->referer());
               return ;
           } else {
               $this->Flash->error(__('予期せぬエラーが発生しました。'));

               return $this->redirect($this->request->referer());
           }
       } catch(Exception $e) {
           $message = $e->getMessage();
           $this->Flash->error($message);

           $this->redirect($this->request->referer());
       }

    }
}