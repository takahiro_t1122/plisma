<?php

namespace App\Controller\Traits;

use Cake\Http\Exception\BadRequestException;

trait FavoritesTrait
{
    /**
     * Adding to favorite for bank users
     * If the item is already pushed as favorited, it turns unfavorite
     * This is a ajax functinon
     *
     * @param string loan_id
     * @return array data
     */
    public function favoriteAction()
    {
        $this->autoRender = false;

        $loginId = $this->Auth->user('id');
        $loan_id = $this->request->data('loan_id');

        // Ajax以外の通信の場合
        if (!$this->request->is('ajax')) {
            throw new BadRequestException;
        }

        if (is_null($loan_id)) {
            $result = 'error';
            $message = __('案件が存在しません');
        }

        try {
            $bank_favorite = $this->BankFavorites->find()->where(['user_id' => $loginId, 'loan_id' => $loan_id])->first();

            // add favorite
            if (is_null($bank_favorite)) { $this->addFavorite($loginId, $loan_id); }
            // remove favorite
            else { $this->removeFavorite($bank_favorite); }

            $result = 'success';
            $message = 'no error';

        } catch(Exception $e) {
            $message = $e->getMessage();
            $result = 'error';
        }
        $data = ['result' => $result, 'message' => $message];
        echo json_encode($data);
        return ;
    }

    /**
     * Adding favorite function
     * New record will be inserted in database
     *
     * @param string loginId
     * @param string loan_id
     *
     */
    public function addFavorite($loginId = null, $loan_id = null)
    {
        try {
            $favorite_entity = $this->BankFavorites->newEntity(['user_id' => $loginId, 'loan_id' => $loan_id]);
            $this->BankFavorites->save($favorite_entity);
        } catch(Exception $e) {
            $message = $e->getMessage();
            $this->Flash->error($message);
        }
    }

    /**
     * Unfavorite function
     * Existing record will be deleted in this function
     *
     * @param entity favorite_entity
     *
     */

    public function removeFavorite($favorite_entity = null)
    {
        try {
            $this->BankFavorites->delete($favorite_entity);
        } catch(Exception $e) {
            $message = $e->getMessage();
            $this->Flash->error($message);
        }
    }
}