<?php

namespace App\Controller\Traits;

use CakeDC\Users\Controller\Component\UsersAuthComponent;
use CakeDC\Users\Exception\UserNotActiveException;
use CakeDC\Users\Exception\UserNotFoundException;
use CakeDC\Users\Exception\WrongPasswordException;
use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\Validation\Validator;
use Exception;
use Cake\Routing\Router;

/**
 * Covers the password management: reset, change
 *
 * @property \Cake\Http\ServerRequest $request
 */
trait PasswordManagementTrait
{
    /**
     * Change password
     *
     * @return mixed
     */
    public function changePassword()
    {
        $user = $this->getUsersTable()->newEntity();
        $id = $this->Auth->user('id');
        if (!empty($id)) {
            $user->id = $this->Auth->user('id');
            $validatePassword = true;
            //@todo add to the documentation: list of routes used
            $redirect = Configure::read('Users.Profile.route');
        } else {
            $user->id = $this->request->getSession()->read(Configure::read('Users.Key.Session.resetPasswordUserId'));
            $validatePassword = false;
            if (!$user->id) {
                $this->Flash->error(__d('CakeDC/Users', 'User was not found'));
                $this->redirect($this->Auth->getConfig('loginAction'));

                return;
            }
            //@todo add to the documentation: list of routes used
            $redirect = '/#login-modal';
        }
        $this->set('validatePassword', $validatePassword);
        if ($this->request->is('post')) {
            try {
                $validator = $this->getUsersTable()->validationPasswordConfirm(new Validator());
                if (!empty($id)) {
                    $validator = $this->getUsersTable()->validationCurrentPassword($validator);
                }
                $user = $this->getUsersTable()->patchEntity(
                    $user,
                    $this->request->getData(),
                    ['validate' => $validator]
                );
                if ($user->getErrors()) {
                    $this->Flash->error(__d('CakeDC/Users', 'Password could not be changed'));
                } else {
                    $result = $this->getUsersTable()->changePassword($user);
                    if ($result) {
                        $event = $this->dispatchEvent(UsersAuthComponent::EVENT_AFTER_CHANGE_PASSWORD, ['user' => $result]);
                        if (!empty($event) && is_array($event->result)) {
                            return $this->redirect($event->result);
                        }
                        $this->Flash->success(__d('CakeDC/Users', 'Password has been changed successfully'));

                        return $this->redirect($redirect);
                    } else {
                        $this->Flash->error(__d('CakeDC/Users', 'Password could not be changed'));
                    }
                }
            } catch (UserNotFoundException $exception) {
                $this->Flash->error(__d('CakeDC/Users', 'User was not found'));
            } catch (WrongPasswordException $wpe) {
                $this->Flash->error($wpe->getMessage());
            } catch (Exception $exception) {
                $this->Flash->error(__d('CakeDC/Users', 'Password could not be changed'));
                $this->log($exception->getMessage());
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Reset password
     *
     * @param null $token token data.
     * @return void
     */
    public function resetPassword($token = null)
    {
        $this->validate('password', $token);
    }

    /**
     * Reset password
     *
     * @return void|\Cake\Http\Response
     */
    public function requestResetPassword()
    {
        $this->set('user', $this->getUsersTable()->newEntity());
        $this->set('_serialize', ['user']);
        if (!$this->request->is('post')) {
            return;
        }

        $reference = $this->request->getData('reference');
        try {
            $resetUser = $this->getUsersTable()->resetToken($reference, [
                'expiration' => Configure::read('Users.Token.expiration'),
                'checkActive' => false,
                'sendEmail' => true,
                'ensureActive' => Configure::read('Users.Registration.ensureActive'),
                'type' => 'password'
            ]);
            if ($resetUser) {
                $msg = __d('CakeDC/Users', 'Please check your email to continue with password reset process');
                $this->Flash->success($msg);
            } else {
                $msg = __d('CakeDC/Users', 'The password token could not be generated. Please try again');
                $this->Flash->error($msg);
            }

            $url = Router::url('/', true);
            return $this->redirect($url);

        } catch (UserNotFoundException $exception) {
            $this->Flash->error(__d('CakeDC/Users', 'User {0} was not found', $reference));
        } catch (UserNotActiveException $exception) {
            $this->Flash->error(__d('CakeDC/Users', 'The user is not active'));
        } catch (Exception $exception) {
            $this->Flash->error(__d('CakeDC/Users', 'Token could not be reset'));
            $this->log($exception->getMessage());
        }
    }

    /**
     * resetGoogleAuthenticator
     *
     * Resets Google Authenticator token by setting secret_verified
     * to false.
     *
     * @param mixed $id of the user record.
     * @return mixed.
     */
    public function resetGoogleAuthenticator($id = null)
    {
        if ($this->request->is('post')) {
            try {
                $query = $this->getUsersTable()->query();
                $query->update()
                    ->set(['secret_verified' => false, 'secret' => null])
                    ->where(['id' => $id]);
                $query->execute();

                $message = __d('CakeDC/Users', 'Google Authenticator token was successfully reset');
                $this->Flash->success($message, 'default');
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $this->Flash->error($message, 'default');
            }
        }

        return $this->redirect($this->request->referer());
    }

    /**
     * Change email
     *
     * @param null $token token data.
     * @return void
     */
    public function changeEmail()
    {
        $user = $this->getUsersTable()->newEntity();
        $user_id = $this->Auth->user('id');

        if ($this->request->is('post')) {
            $reference = $this->request->getData('email');
            $expiration = Configure::read('Users.Token.expiration');

            if (empty($reference)) {
                throw new \InvalidArgumentException(__d('CakeDC/Users', "Reference cannot be null"));
            }

            if (empty($expiration)) {
                throw new \InvalidArgumentException(__d('CakeDC/Users', "Token expiration cannot be empty"));
            }

                try {
                $user->id = $user_id;
                $user = $this->getUsersTable()->patchEntity(
                    $user,
                    ['email' => $reference],
                    ['validate' => 'changeEmail']
                );

                if ($user->getErrors()) {
                    $this->set('user', $user);
                    return ;
                }

                $user = $this->getUsersTable()->get($user_id);
                $user->updateToken($expiration);
                $user->change_email = $reference;
                $saveResult = $this->getUsersTable()->save($user);
                $mailer = Configure::read('Users.Email.mailerClass') ?: 'CakeDC/Users.Users';
                $this
                    ->getMailer($mailer)
                    ->send('changeEmail', [$user, $reference]);

                $this->Flash->success('変更メールアドレスにメールを送信しました。メールを確認し、認証してください。');
                return $this->redirect(['controller' => 'Allocations', 'action' => 'loginRedirect']);
            } catch (UserNotFoundException $exception) {
                $this->Flash->error(__d('CakeDC/Users', 'User was not found'));
            } catch (Exception $exception) {
                $this->Flash->error(__('メールアドレスが変更できませんでした'));
                $this->log($exception->getMessage());
            }
        }
    }

    /**
     * Confirm email
     *
     * @param null $token token data.
     * @return void
     */
    public function confirmEmail($token = null)
    {
        $this->validate('confirmEmail', $token);
    }

    /**
     * Complete changing email
     *
     * @return mixed
     */
    public function completeChangeEmail()
    {
        $this->autoRender = false;
        $user_id = $this->Auth->user('id');
        $user = $this->getUsersTable()->get($user_id);
        $new_email = $user->change_email;
        $old_email = $user->email;
        $user = $this->getUsersTable()->patchEntity($user, ['email' => $new_email, 'change_email' => $old_email, 'token_expires' => null]);
        $saveResult = $this->getUsersTable()->save($user);

        if ($saveResult) {
            $mailer = Configure::read('Users.Email.mailerClass') ?: 'CakeDC/Users.Users';
            $this
                ->getMailer($mailer)
                ->send('completeChangeEmail', [$user]);
        }

        $redirect = Configure::read('Users.Profile.route');
        return $this->redirect($redirect);

    }
}
