<?php

namespace App\Controller\Traits;

trait CommonFuncTrait
{
    /**
     * Returns bank list for combobox
     *
     * @return array bank_list
     */
    public function getBankList()
    {
        $all_banks = $this->getBankTable()->find('all');
        $bank_list = array();
        $key[] = '';
        $value[] = '';
        if (!empty($all_banks)) {
            foreach ($all_banks as $bank) {
                $key[] = $bank['id'];
                $value[] = $bank['bank_name'];
            }
            $bank_list = array_combine($key, $value);
        }
        $this->set(compact("bank_list"));
    }

    /**
     * Returns Category list when the page is loaded
     *
     * @return array
     */
    public function getCategoryList()
    {
        $all_categories = $this->Categories->find('all');
        $category_list = array();
        $key[] = '';
        $value[] = '';
        if (!empty($all_categories)) {
            foreach ($all_categories as $category) {
                $key[] = $category['id'];
                $value[] = $category['name'];
            }
            $category_list = array_combine($key, $value);
        }
        $this->set(compact("category_list"));
    }

    /**
     * Returns Sub Category list according to category value
     * This function is called with ajax
     *
     * @param string category_name
     * @return array
     */
    public function getSubcategoryList()
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $category_id = $this->request->data('category_id');
            $sub_categories = $this->SubCategories->find()
                ->contain('Categories')->where(['Categories.id' => $category_id]);
            $sub_category_list = $sub_categories->toArray();
            array_unshift($sub_category_list, ['id' => '', 'name' => '']);
            echo json_encode($sub_category_list, JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * Date data formating function
     *
     * @param array $input_data
     * @return array $input_data
     */
    private function modifyArrayDataFormat($input_data = null)
    {
        $input_data['fiscal_year_one'] = $input_data['fiscal_year_one']['year'];
        $input_data['fiscal_year_two'] = $input_data['fiscal_year_two']['year'];
        $input_data['fiscal_year_three'] = $input_data['fiscal_year_three']['year'];
        if (substr($input_data['financial_month']['month'], 0, 1) == '0') {
            $input_data['financial_month']['month'] = substr($input_data['financial_month']['month'], 1);
        }
        $input_data['financial_month'] = $input_data['financial_month']['month'];

        return $input_data;
    }
}