<?php

namespace App\Controller\Traits;

use Cake\Core\Configure;
use CakeDC\Users\Controller\Component\UsersAuthComponent;

trait UserLoggingsTrait
{
    public function loggingCheck()
    {
        $loginUser = $this->Auth->user();
        if (!isset($loginUser)) return ;

        $login_token = $this->request->getSession()->read('logging_token');
        if (is_null($login_token)) {
            $login_token = $this->UserLoggings->find()->where(['user_id' => $loginUser['id']])->first()->token;
        }
        $isExist = $this->UserLoggings->exists(['user_id' => $loginUser['id'], 'token' => $login_token]);
        if (!$isExist) {
            $eventBefore = $this->dispatchEvent(UsersAuthComponent::EVENT_BEFORE_LOGOUT, ['user' => $user]);
            if (is_array($eventBefore->result)) {
                return $this->redirect($eventBefore->result);
            }
            $this->request->getSession()->destroy();
            $this->Flash->success(__('他デバイスから同一アカウントを使用してのアクセスを確認したため、ログアウト致します。'));

            $eventAfter = $this->dispatchEvent(UsersAuthComponent::EVENT_AFTER_LOGOUT, ['user' => $user]);
            if (is_array($eventAfter->result)) {
                return $this->redirect($eventAfter->result);
            }

            return $this->redirect($this->Auth->logout());
        }
    }
}
