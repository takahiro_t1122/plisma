<?php

namespace App\Controller\Traits;

use App\Controller\AllocationsController;
use App\Model\Table\UsersTable;
use App\Event\NotificationListener;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\InvalidPrimaryKeyException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Helper\SessionHelper;
use CakeDC\Users\Controller\Component\UsersAuthComponent;
use Cake\Event\Event;
use Cake\Event\EventManager;

/**
 * Covers registration features and email token validation
 *
 * @property \Cake\Http\ServerRequest $request
 */
trait UserDetailTrait
{
    public function initialize()
    {
        parent::initialize();

        $this->Notification = new NotificationListener();
        EventManager::instance()->attach($this->Notification);
    }

    /**
     * Registering detail for corporation user
     * Reserves data into session and passes to confirm page
     *
     */
    public function registerDetailCorp()
    {
        if (!Configure::read('Users.Registration.active')) {
            throw new NotFoundException();
        }

        $loginId = $this->Auth->user('id');
        $loginUser = $this->Users->get($loginId);

        if (empty($loginUser['id'])) {
            $this->Flash->error(__d('CakeDC/Users', 'You must register a user account first'));
            return $this->redirect($this->request->referer());
        }

        if (empty($loginUser['activation_date'])) {
            $this->Flash->error(__d('CakeDC/Users', 'You must activate the user first with email we have sent'));
            return $this->redirect($this->request->referer());
        }

        $user = $this->Users->patchEntity(
            $this->Users->get($loginUser['id']),
            $this->request->getData(),
            ['validate' => ROLE_CORP]
        );

        if ($this->request->is(['post', 'put'])) {

            // reserve data to session
            $session = $this->request->getSession();
            if ($session->check('inputData')) {
                $session->delete('inputData');
            }
            $inputData = $this->request->getData();
            $session->write('inputData', $inputData);

            if ($user->getErrors()) {
                $this->set(compact('user'));
                return ;
            } else {
                $session->write('inputData.mode', $inputData['mode']);
                $session->write('inputData.role', ROLE_CORP);
                return $this->redirect(['controller' => 'users', 'action' => 'confirmDetail']);
            }
        }
    }

    /**
     * Registering detail for bank user
     * Reserves data into session and passes to confirm page
     *
     */
    public function registerDetailBank()
    {
        if (!Configure::read('Users.Registration.active')) {
            throw new NotFoundException();
        }

        $loginId = $this->Auth->user('id');
        $loginUser = $this->Users->get($loginId);

        if (empty($loginUser['id'])) {
            $this->Flash->error(__d('CakeDC/Users', 'You must register a user account first'));
            return $this->redirect($this->request->referer());
        }

        if (empty($loginUser['activation_date'])) {
            $this->Flash->error(__d('CakeDC/Users', 'You must activate the user first with email we have sent'));
            return $this->redirect($this->request->referer());
        }

        // get bank list
        $this->getBankList();

        $user = $this->Users->patchEntity(
            $this->Users->get($loginUser['id']),
            $this->request->getData(),
            ['validate' => ROLE_BANK]
        );

        if ($this->request->is(['post', 'put'])) {

            // reserve data to session
            $session = $this->request->getSession();
            if ($session->check('inputData')) {
                $session->delete('inputData');
            }
            $inputData = $this->request->getData();
            $session->write('inputData', $inputData);

            if ($user->getErrors()) {
                $this->set(compact('user'));
                return ;
            } else {
                $session->write('inputData.mode', $inputData['mode']);
                $session->write('inputData.role', ROLE_BANK);
                return $this->redirect(['controller' => 'users', 'action' => 'confirmDetail']);
            }
        }
    }

    /**
     * Confirming pages of register detail, getting data from session
     * This function is common for bank and corp user
     *
     */
    public function confirmDetail()
    {
        $inputData = $this->request->getSession()->read('inputData');

        if ($this->request->is(['post', 'put'])) {
            $inputData['mode'] = $this->request->getData('mode');
        }

        if ($inputData['role'] == ROLE_CORP) {
            $inputData['established_in'] = implode("-", $inputData['established_in']);
            if (preg_match("/(^-|-$)/", $inputData['established_in'])) {
                $inputData['established_in'] = "";
            }
        } else if ($inputData['role'] == ROLE_BANK) {
            $inputData['bank_name'] = $this->getBankTable()->get($inputData['bank_id'])->bank_name;
        } else {
            $this->Flash->error(__d('CakeDC/Users', 'You are not authorized to access that location'));
            return $this->redirect($this->request->referer());
        }

        if ($inputData['mode'] == 'confirm') {
            $this->set('input_data', $inputData);
            $this->render('confirm_detail');
        } else if ($inputData['mode'] == 'complete') {
            $this->request->getSession()->write('inputData.mode', $inputData['mode']);
            $this->request->getSession()->write('inputData.established_in', $inputData['established_in']);
            return $this->redirect(['controller' => 'users', 'action' => 'completeDetail']);
        } else {
            $this->Flash->error(__d('CakeDC/Users', 'You cannot access this page directly'));
            return $this->redirect($this->request->referer());
        }
    }

    /**
     * Completing user detail registration
     * the data is reserved when reaching this function
     *
     */
    public function completeDetail()
    {
        $inputData = $this->request->getSession()->read('inputData');
        $loginId = $this->Auth->user('id');

        if ($inputData['mode'] == 'complete') {
            // update
            $user = $this->Users->patchEntity($this->Users->get($loginId), $inputData, ['validate' => $inputData['role']]);
            collection($inputData)->filter(function ($value, $field) use ($user) {
                return !$user->isAccessible($field);
            })->each(function ($value, $field) use (&$user) {
                $user->{$field} = $value;
            });
            $this->request->getSession()->delete('Auth.User.role');
            $this->request->getSession()->write('Auth.User.role', $inputData['role']);
            $savedUser = $this->Users->save($user);

            // inactivate bank account
            if ($inputData['role'] == ROLE_BANK) $user['active'] = false;

            if ($savedUser = $this->Users->save($user)) {
                $this->request->getSession()->delete('inputData');
                if ($inputData['role'] == ROLE_CORP) {
                    $this->render('complete_detail_corp');
                } else if ($inputData['role'] == ROLE_BANK) {
                    // Mail sending event
                    $event = new Event('Notification.Email', $this, [
                        'user' => $user
                    ]);
                    $this->getEventManager()->dispatch($event);

                    $eventBefore = $this->dispatchEvent(UsersAuthComponent::EVENT_BEFORE_LOGOUT, ['user' => $user]);
                    if (is_array($eventBefore->result)) {
                        return $this->redirect($eventBefore->result);
                    }
                    $this->request->getSession()->destroy();
                    $eventAfter = $this->dispatchEvent(UsersAuthComponent::EVENT_AFTER_LOGOUT, ['user' => $user]);
                    if (is_array($eventAfter->result)) {
                        return $this->redirect($eventAfter->result);
                    }
                    $this->Auth->logout();
                    $this->render('complete_detail_bank');
                }
            } else {
                $this->set(compact('user'));
                $this->Flash->error(__d('CakeDC/Users', 'The user could not be saved'));
                return $this->redirect($this->request->referer());
            }
        } else {
            $this->Flash->error(__d('CakeDC/Users', 'You cannot access this page directly'));
            return $this->redirect($this->request->referer());
        }
    }
}
