<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use App\Form\ConversationForm;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\Http\Exception\InternalErrorException;
use Cake\Core\Configure;

class ConversationsController extends AppController
{

    /**
     * 会話一覧を取得し、画面に返す。
     */
    public function index() 
    {
        #ログイン情報からuser_idを取得
        $user_id = $this->Auth->user('id');
        $this->log('login_user_id: ' .$user_id, LOG_DEBUG);

        #user_idの属する未削除の会話を取得
        $conversations = $this->Conversations->find()
            ->select(['Conversations.id', 'Conversations.subject', 
                'Conversations__last_reply' => 'MAX(conversation_messages.sent_at)',
                'Conversations__unread' => 'MAX(conversation_messages.sent_at) > conversation_users.read_at'])
            ->join([
                'table' => 'conversation_messages',
                'type' => 'LEFT',
                'conditions' => 'conversation_messages.conversation_id = Conversations.id'
            ])
            ->join([
                'table' => 'conversation_users',
                'type' => 'INNER',
                'conditions' => [
                    'conversation_users.conversation_id = Conversations.id',
                    'conversation_users.is_deleted = 0'
                ]
            ])
            ->where(['conversation_users.user_id' => $user_id])
            ->group(['Conversations.id', 'conversation_users.read_at'])
            ->order(['Conversations__last_reply' => 'DESC']);

        $this->log('sql query: '.$conversations, LOG_DEBUG);
        $role = $this->Auth->user('role');
        $this->set(compact('conversations', 'role'));
    }

    /**
     * 指定した会話スレッド情報を取得し、画面に返す。
     * @param string conversation_id
     */
    public function detail($conversation_id = null)
    {
        #ログイン情報からuser_idを取得
        $user_id = $this->Auth->user('id');
        $this->log('login_user_id: ' .$user_id, LOG_DEBUG);

        #conversation_idの会話に属するメッセージを取得
        $this->loadModel('ConversationMessages');
        $messages = $this->ConversationMessages->find()
        ->select([
            'ConversationMessages.sent_at', 'ConversationMessages.content',
            'ConversationMessages__user_name' => 'users.last_name',
            'ConversationMessages__unread' => 'ConversationMessages.sent_at > conversation_users.read_at',
            'ConversationMessages__read_at' => 'conversation_users.read_at'
        ])
        ->join([
            'table' => 'users',
            'type' => 'INNER',
            'conditions' => 'users.id = ConversationMessages.user_id'
        ])
        ->join([
            'table' => 'conversation_users',
            'type' => 'INNER',
            'conditions' => 'conversation_users.conversation_id = ConversationMessages.conversation_id'
        ])
        ->where([
            'ConversationMessages.conversation_id' => $conversation_id,
            'conversation_users.user_id' => $user_id
        ])
        ->order(['ConversationMessages.sent_at' => 'DESC'])
        ->all();
        $this->log($messages, LOG_DEBUG);

        #ユーザのread_at更新
        $this->loadModel('ConversationUsers');
        $user = $this->ConversationUsers->find()
        ->where([
            'conversation_id' => $conversation_id,
            'user_id ' => $user_id
        ])
        ->first();
        $this->log('update target: '.print_r($user, true), LOG_DEBUG);
        $user->read_at = Time::now(); #時刻はサーバ側で取得
        $this->ConversationUsers->save($user);

        #返信用のメッセージEntityを用意
        #これを書かないとdetail.ctpでフォームが作れない。。。
        $new_message = $this->ConversationMessages->newEntity();

        $this->set(compact('messages', 'user_id', 'conversation_id', 'new_message'));
    }

    /**
     * 会話スレッドを新規作成する。
     * リクエストメソッドにより、処理が異なる。
     * GETの場合：入力用フォームを初期化する
     * POSTの場合：画面から受け取ったフォームを基に新規作成する。
     */
    public function new()
    {
        #ログイン情報からuser_idを取得
        $user_id = $this->Auth->user('id');
        $this->log('login_user_id: ' .$user_id, LOG_DEBUG);

        $form = new ConversationForm();
        if ($this->request->is('post')) {
            $this->log('POST conversations/new, user_id='.$user_id, LOG_DEBUG);

            #権限チェック. bank以外は却下
            if ($this->Auth->user('role') != ROLE_BANK) {
                $this->log('forbidden. role='.$this->Auth->user('role'), LOG_ERR);
                $this->Flash->error(__('会話を作成する権限がありません。'));
                return $this->redirect(['action' => 'index']);
            }

            #保存処理実行. 処理を分割したい...
            $data = $this->request->getData();
            if ($form->validate($data)) {
                $this->log('transaction start.', LOG_DEBUG);
                #3テーブルへのinsertをトランザクションで制御
                $connection = ConnectionManager::get('default');
                $connection->begin();
                $time = Time::now(); #サーバ側で現在日時を取得
                try {
                    #toを分割
                    $to_names = explode(',', Hash::get($data, 'to'));
                    foreach ($to_names as $name) {
                        $name = trim($name);
                    }
                    $this->log('to_names: ' .print_r($to_names, true), LOG_DEBUG);

                    #toのuserエンティティを取得
                    $this->loadModel('Users');
                    $to_users = $this->Users->find()
                    ->select(['Users.id', 'Users.last_name'])
                    ->where(['Users.last_name IN' => $to_names])
                    ->all();
                    $this->log('to_users: ' .print_r($to_users, true), LOG_DEBUG);

                    #挿入要user_idリスト作成
                    $to_ids = array($user_id);
                    foreach ($to_users as $to_user) {
                        $to_ids[] = $to_user->id;
                    }

                    #TODO:toがマッチしなかったときのハンドリング

                    #Conversations保存
                    $conversation = $this->Conversations->newEntity([
                        'subject' => Hash::get($data, 'subject')]);
                    if (!($result_cnv = $this->Conversations->save($conversation))) {
                        $this->log('failed to save Conversation!', LOG_ERR);
                        throw new InternalErrorException('failed to save Conversation!');
                    }

                    #ConversationMessages保存
                    $this->loadModel('ConversationMessages');
                    $message = $this->ConversationMessages->newEntity([
                        'conversation_id' => $result_cnv->id,
                        'user_id' => $user_id,
                        'sent_at' => $time,
                        'content' => Hash::get($data, 'body')
                    ]);           
                    if (!($this->ConversationMessages->save($message))) {
                        $this->log('failed to save Message!', LOG_ERR);
                        throw new InternalErrorException('failed to save Message!');
                    }

                    #ConversationUsers保存
                    $this->loadModel('ConversationUsers');
                    $values = array();
                    foreach ($to_ids as $to_id) {
                        $this->log('to_user: ' .$to_user, LOG_DEBUG);
                        $values[] = [
                            'conversation_id' => $result_cnv->id,
                            'user_id' => $to_id,
                            'read_at' => $time,
                            'is_deleted' => 0
                        ];
                    }
                    $this->log('values: ' .print_r($values, true), LOG_DEBUG);
                    $cnvs_members = $this->ConversationUsers->newEntities($values);
                    $this->log('cnvs_members: ' .print_r($cnvs_members, true), LOG_DEBUG);
                    if (!($this->ConversationUsers->saveMany($cnvs_members))) {
                        $this->log('failed to save ConversationUsers!', LOG_ERR);
                        throw new InternalErrorException('failed to save ConversationUsers!');
                    }

                    $this->Conversations->connection()->commit();
                    $this->Flash->success(__('会話を作成しました。'));
                    return $this->redirect(['action' => 'index']);
                } catch (Exception $e) {
                    $this->log($e->getMessage(), LOG_ERR);
                    $connection->rollback(); #rollback
                }
            }
            $this->log('error occured.', LOG_DEBUG);
            $this->Flash->error(__('会話を作成できませんでした。'));
        }
        $this->set(compact('form'));
    }

    /**
     * 指定したconversation_idの会話、メンバー情報、メッセージを削除する。
     * 他の宛先メンバーが会話を削除していない場合、会話は論理削除となる。
     * 他の宛先メンバーが全員会話を削除している場合、会話は物理削除となる。
     * @param string conversation_id
     */
    public function delete($conversation_id)
    {
        $this->request->allowMethod(['post', 'delete']);

        #ログイン情報からuser_idを取得
        $user_id = $this->Auth->user('id');
        $this->log('login_user_id: ' .$user_id, LOG_DEBUG);

        #他のメンバーの削除状態をチェック
        $this->loadModel('ConversationUsers');
        $result = $this->ConversationUsers->find()
        ->select(['is_deleted'])
        ->distinct(['is_deleted'])
        ->where([
            'conversation_id' => $conversation_id,
            'user_id !=' => $user_id
        ])
        ->all();
        $this->log('member check. result: '.print_r($result->toArray(), true), LOG_DEBUG);

        if ($result->count() == 1 && $result->first()->is_deleted) {
            #他のメンバーが削除しているので物理削除する
            #アソシエーションで関連するConversationUsers,ConversationMessagesも削除
            #以下のようにエンティティを取得して削除しないとアソシエーション先を消してくれない
            $conversation = $this->Conversations->find()
            ->where(['id' => $conversation_id])
            ->first();
            if ($this->Conversations->delete($conversation)) {
                $this->Flash->success(__('会話を削除しました。'));
                return $this->redirect(['action' => 'index']);
            }

            $this->log('failed to delete physically.', LOG_ERR);
        } else {
            #削除していないメンバーがいるので論理削除
            $member = $this->ConversationUsers->find()
            ->where([
                'conversation_id' => $conversation_id,
                'user_id ' => $user_id
            ])
            ->first();
            $this->log('logically delete. target: '.print_r($member, true), LOG_DEBUG);

            $member->is_deleted = 1;
            if ($this->ConversationUsers->save($member)) {
                $this->Flash->success(__('会話を削除しました。'));
                return $this->redirect(['action' => 'index']);
            }

            $this->log('failed to delete logically.', LOG_ERR);
        }

        $this->Flash->error(__('会話を削除できませんでした。'));
        return $this->redirect(['action' => 'index']);
    }

    /**
     * メッセージを返信する。
     */
    public function reply()
    {
        #POSTリクエストしか来ないが一応
        if ($this->request->is('post')) {
            $time = Time::now(); #時刻はサーバ側で取得

            #メッセージ追加
            $this->loadModel('ConversationMessages');
            $message = $this->ConversationMessages->newEntity($this->request->getData());
            $message->sent_at = $time;
            $this->log($message, LOG_DEBUG);
            if (($this->ConversationMessages->save($message))) {
                $this->Flash->success(__('返信しました。'));
                return $this->redirect([
                    'action' => 'detail',
                    $message->conversation_id,
                ]);
            }
            $this->Flash->error(__('返信できませんでした。'));
        }
    }
}
