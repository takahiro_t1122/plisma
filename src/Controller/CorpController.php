<?php

namespace App\Controller;

use App\Controller\Traits\UserLoggingsTrait;
use App\Controller\Traits\LoanItemTrait;
use App\Controller\Traits\FinanceTrait;
use App\Controller\Traits\BlocklistsTrait;
use App\Controller\Traits\TableAccessTrait;
use App\Controller\Traits\CommonFuncTrait;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Customaized Controller for Corporation account users
 */

class CorpController extends AppController
{
    use LoanItemTrait;
    use FinanceTrait;
    use BlocklistsTrait;
    use CommonFuncTrait;
    use TableAccessTrait;
    use UserLoggingsTrait;

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Users');
        $this->loadModel('Loan');
        $this->loadModel('Banks');
        $this->loadModel('Blocklists');
        $this->loadModel('Finance');
        $this->loadModel('Categories');
        $this->loadModel('SubCategories');
        $this->loadModel('UserLoggings');
        //temporary for stripe
        $this->loadModel('UserPayments');
        $this->loadModel('PaymentDetails');
        $this->loadModel('BankFavorites');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        // multi-logging check
        $this->loggingCheck();

        $userId = $this->Auth->user('id');
        if (!isset($userId)) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }

        $user = $this->Users->get($userId);
        if (empty($user['phone'])) {
            return $this->redirect(['controller' => 'Users', 'action' => 'registerDetailCorp']);
        }
    }

    public function home()
    {
        try {
            $loginId = $this->Auth->user('id');
            $loan = $this->Loan->find()->where(['user_id' => $loginId])->contain(['users'])->first();
            $finance = $this->Finance->find()->where(['user_id' => $loginId])->first();

            // stripe temporary info
            $current_value = $this->UserPayments->find()->where(['user_id' => $loginId])->first()->current_value;
            $this->set('current_value', $current_value);
            $this->set('amout_value', $this->PaymentDetails->find()->where(['id' => Configure::read('ServiceCharge.corp_current_id'), 'is_valid' => true])->first()->fee);
            // temporary end

            $entity = ['loan' => $loan, 'finance' => $finance];
            $this->set('entity', $entity);
            $this->render();
            return ;
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Temporary placement for stripe payment
     */
    public function stripeFunc()
    {
        $this->autoRender = false;
        $this->loadComponent('Stripe');

        $loginId = $this->Auth->user('id');
        $payment_entity = $this->UserPayments->find()->where(['user_id' => $loginId])->first();
        $user->cus_id = $payment_entity->stripe_id;
        $user->email = $this->request->getData('stripeEmail');
        $token = $this->request->getData('stripeToken');

        // additional service charge id
        $payment_id = Configure::read('ServiceCharge.corp_current_id');
        $payment_detail_entity = $this->PaymentDetails->find()->where(['id' => $payment_id, 'is_valid' => true])->first();
        if (is_null($payment_detail_entity)) {
            $this->Flash->error(__('一時的にサービスのご利用ができません。'));
        }

        $stripeId = $this->Stripe->createCustomerIfNotExist($user, $token);
        if (is_null($user->cus_id)) {
            $payment_entity = $this->UserPayments->newEntity(['user_id' => $loginId, 'stripe_id' => $stripeId, 'payment_id' => $payment_id]);
            $this->UserPayments->save($payment_entity);
        }
        //説明
        $description = Configure::read('ServiceCharge.corp_description');
        //明細書表記 (英文字)
        $statementDescriptor = Configure::read('ServiceCharge.corp_statementDescriptor');
        $amount = $payment_detail_entity->fee;
        $charge_id = $this->Stripe->chargeByCustomerId($stripeId, $amount, $description, $statementDescriptor, $loginId);

        // in case of success
        if ($charge_id) {
            $update_value = $payment_entity->current_value + $payment_detail_entity->value;
            $update_payment_entity = $this->UserPayments->patchEntity($payment_entity, ['current_value' => $update_value]);
            $this->UserPayments->save($update_payment_entity);
        } else {
            $this->Flash->error(__('エラーが発生しました。'));
        }
        return $this->redirect($this->request->referer());
    }
}