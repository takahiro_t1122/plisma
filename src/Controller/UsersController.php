<?php

namespace App\Controller;

use App\Model\Table\UsersTable;
use App\Controller\AppController;
use App\Controller\Traits\UserDetailTrait;
use App\Controller\Traits\LoginTrait;
use App\Controller\Traits\SimpleCrudTrait;
use App\Controller\Traits\TableAccessTrait;
use App\Controller\traits\CommonFuncTrait;
use App\Controller\Traits\ProfileTrait;
use App\Controller\Traits\RegisterTrait;
use App\Controller\Traits\PasswordManagementTrait;
use App\Controller\Traits\UserValidationTrait;
use CakeDC\Users\Controller\Traits\ReCaptchaTrait;
use Cake\Mailer\MailerAwareTrait;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\ORM\Table;

/**
 * Users Controller
 *
 * @property UsersTable $Users
 */
class UsersController extends AppController
{
    use UserDetailTrait;
    use LoginTrait;
    use ProfileTrait;
    use ReCaptchaTrait;
    use PasswordManagementTrait;
    use RegisterTrait;
    use SimpleCrudTrait;
    use TableAccessTrait;
    use CommonFuncTrait;
    use UserValidationTrait;
    use MailerAwareTrait;
}
