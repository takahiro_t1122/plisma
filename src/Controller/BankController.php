<?php

namespace App\Controller;

use App\Controller\Traits\TableAccessTrait;
use App\Controller\Traits\UserLoggingsTrait;
use App\Controller\Traits\ViewLoanItemTrait;
use App\Controller\Traits\LoginTrait;
use App\Controller\Traits\CommonFuncTrait;
use App\Controller\Traits\FavoritesTrait;
use Cake\Core\Configure;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Routing\Router;

/**
 * Customaized Controller for Bank account users
 */

class BankController extends AppController
{
    use CommonFuncTrait;
    use ViewLoanItemTrait;
    use UserLoggingsTrait;
    use LoginTrait;
    use FavoritesTrait;

    public $paginate = [
        'limit' => 25,
        ];

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Users');
        $this->loadModel('Loan');
        $this->loadModel('Banks');
        $this->loadModel('Blocklists');
        $this->loadModel('Finance');
        $this->loadModel('Categories');
        $this->loadModel('SubCategories');
        $this->loadModel('UserLoggings');
        $this->loadModel('BankFavorites');
        $this->loadComponent('Paginator');

        // add friendsofcake/search
        $this->loadComponent('Search.Prg', [
            'actions' => ['home']
        ]);
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        //multi-logging check
        $this->loggingCheck();

        // create random number
        if ($this->request->getSession()->read('seed') == '') {
            $this->request->getSession()->write('seed', mt_rand());
        }

        $userId = $this->Auth->user('id');
        if (!isset($userId)) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }

        $user = $this->Users->get($userId);
        if (empty($user['phone'])) {
            return $this->redirect(['controller' => 'Users', 'action' => 'registerDetailBank']);
        }
    }

    public function home()
    {
        try {
            $loginId = $this->Auth->user('id');
            $user = $this->Users->find()->where(['users.id' => $loginId])->contain(['banks'])->first();
            $this->itemSearchConditions();

            $favs = $this->BankFavorites->find()->where(['user_id' => $user->id])->toArray();
            foreach ($favs as $fav) {
                $fav_items[] = $this->Loan->find()->where(['id' => $fav->loan_id])->first();
            }

            $this->set('user', $user);
            $this->set('fav_items', $fav_items);
            $this->render();
            return ;

        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Common function for limitting the returning value
     * getting minimum range or maximum value
     *
     * @param entity $query
     * @param column_name $target
     * @param numeric $min
     * @param numeric $max
     * @return entity $query
     */
    public function returnValueByRange($query, $target, $min, $max)
    {
        if ($min && !$max) {
            $query = $query->where(function ($exp) use ($target, $min) {
                return $exp->gte($target, $min);
              });
        } else if (!$min && $max) {
            $query = $query->where(function ($exp) use ($target, $max) {
                return $exp->lte($target, $max);
              });
        } else {
            $query = $query->where(function ($exp) use ($target, $min, $max) {
                return $exp->between($target, $min, $max);
            });
        }
        return $query;
    }
}