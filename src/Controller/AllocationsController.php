<?php

namespace App\Controller;

use Cake\Datasource\Exception\InvalidPrimaryKeyException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Controller\Component\AuthComponent;
use Cake\Core\Configure;
use Cake\Event\Event;

class AllocationsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Users');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        $user = $this->Auth->user();
        if (!isset($user)) {
            return $this->redirect($this->request->referer().'#login-modal');
        }
    }

    public function loginRedirect()
    {
        $this->autoRender = false;

        try {
            $loginId = $this->Auth->user('id');
            $user = $this->Users->get($loginId);
            $role = $user['role'];

            if ($this->request->is(['post', 'put'])) {
                $role = $this->request->getData('role');
            }
       } catch (RecordNotFoundException $ex) {
            $this->Flash->error(__d('CakeDC/Users', 'User was not found'));

            return $this->redirect($this->request->referer());
        } catch (InvalidPrimaryKeyException $ex) {
            $this->Flash->error(__d('CakeDC/Users', 'Not authorized, please login first'));

            return $this->redirect($this->request->referer());
        }

        // before registering detail
        if (empty($user['phone'])) {
            if ($role == ROLE_CORP) {
                return $this->redirect(['controller' => 'Users', 'action' => 'registerDetailCorp']);
            } else if ($role == ROLE_BANK) {
                return $this->redirect(['controller' => 'Users', 'action' => 'registerDetailBank']);
            } else if ($role == ROLE_NONE) {
                return $this->render('/Users/register_detail');
            }
        }
        // allocating to each role's randing pages
        else {
            if ($role == ROLE_CORP) {
                return $this->redirect(['controller' => 'Corp', 'action' => 'home']);
            } else if ($role == ROLE_BANK) {
                return $this->redirect(['controller' => 'Bank','action' => 'home']);
            }
        }
    }
}