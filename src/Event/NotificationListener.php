<?php
namespace App\Event;

use Cake\Event\EventListenerInterface;
use App\Mailer\UsersMailer;
use Cake\Core\Configure;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class NotificationListener
 * @package App\Event
 */
class NotificationListener implements EventListenerInterface
{
    use MailerAwareTrait;

    public function implementedEvents()
    {
        return [
            'Notification.Email' => 'mailNotification',
        ];
    }

    /**
     * Email sender
     * @param $event
     * @param $user
     */
    public function mailNotification($event, $user)
    {
        $mailer = Configure::read('Users.Email.mailerClass');
        $this->getMailer($mailer)->send('registerNotificationEmail', [$user, $sendTo]);
    }
}