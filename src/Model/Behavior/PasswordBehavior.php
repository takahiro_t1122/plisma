<?php
namespace App\Model\Behavior;

use CakeDC\Users\Model\Behavior\PasswordBehavior as PasswordBehaviorParent;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Mailer\MailerAwareTrait;
use Cake\Utility\Hash;

class PasswordBehavior extends PasswordBehaviorParent
{

    /**
     * Override base PasswordBehavior not to look at Username
     *
     * @param string $reference reference can be only email
     * @return mixed user entity if found
     */
    protected function _getUser($reference)
    {
        return $this->_table->findByEmail($reference)->first();
    }
}