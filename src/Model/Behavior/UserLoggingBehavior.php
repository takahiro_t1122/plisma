<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Datasource\EntityInterface;
use Cake\I18n\Time;

class UserLoggingBehavior extends Behavior
{
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function hasToken($reference){

        if ($this->_getUserLogging($reference)) {
            return true;
        } else {
            return false;
        }

    }

    protected function _getUserLogging($reference)
    {
        return $this->_table->findByUserId($reference)->first();
    }

    public function createLoggingToken(EntityInterface $userLogging)
    {
        $userLogging->generateToken();

        $userLoggingSaved = $this->_table->save($userLogging);

        return $userLoggingSaved;
    }

    public function updateLoggingToken(EntityInterface $userLogging)
    {
        $userLoggingSaved = $this->_table->save($userLogging);

        return $userLoggingSaved;
    }
}