<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Bank extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}