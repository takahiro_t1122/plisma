<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Blocklist extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}