<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Security;

class UserLogging extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    public function generateToken()
    {
        $this->token = bin2hex(Security::randomBytes(16));
    }
}