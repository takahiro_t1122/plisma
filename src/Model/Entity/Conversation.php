<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Conversation extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}