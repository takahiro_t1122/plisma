<?php
namespace App\Model\Entity;

use CakeDC\Users\Model\Entity\User as UserParent;

use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\ORM\Entity;
use Cake\Utility\Text;
use Cake\Utility\Security;
use DateTime;

class User extends UserParent
{
    protected function _getUsername()
    {
        return $this->email;
    }

    protected function _setUsername($email)
    {
        return $this->$email;
    }

    protected $_accessible = [
        '*' => true,
        'id' => false,
        'is_superuser' => false,
        'role' => true,
    ];

}