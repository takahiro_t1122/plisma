<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class UserPayment extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}