<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ConversationMessage extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}