<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class BankFavorite extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}