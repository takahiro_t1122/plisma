<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class SubCategory extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}