<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class ConversationUser extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}