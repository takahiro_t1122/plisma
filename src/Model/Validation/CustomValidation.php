<?php

namespace App\Model\Validation;

use Cake\Validation\Validation;
use Cake\Core\Configure;

class CustomValidation extends Validation
{
    /**
     * Validating the file format
     * allowing specific extention as follows
     *
     * @param file $files
     * @return boolean $ret
     */
    public function fileFormatCheck($files)
    {
        $ret = true;
        $allows_mime = [
              'application/pdf', //.pdf
              'application/vnd.ms-powerpoint', //.ppt
              'application/vnd.openxmlformats-officedocument.presentationml.presentation', //.pptx
              'application/vnd.ms-excel', //.xls
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', //.xlsx
              'application/msword', //.doc
              'application/vnd.openxmlformats-officedocument.wordprocessingml.document', //.docx
              'text/plain', //.txt
              'image/jpeg', //.jpg
        ];
        $allows_ext = ['pdf', 'ppt', 'pptx', 'xls', 'xlsx', 'doc', 'docx', 'txt', 'jpeg', 'jpg'];
        $ext = substr($files['name'], strrpos($files['name'], '.') + 1);
        if (!in_array($files['type'], $allows_mime) || !in_array($ext, $allows_ext)) {
              $ret = false;
        }
        return $ret;
    }

    /**
     * Validating file name
     * - Not allowing initial or last letter '.'(dot)
     * - Not allowing of containing '/' (slash)
     * - Not allowing extention of '.php', 'cgi', '.py', '.rb'
     *
     * @param file $files
     * @return boolean
     */
    public function fileNameCheck($files)
    {
        return (bool) preg_match('/\A(?!\.)[\w.-]++(?<!\.)(?<!\.php)(?<!\.cgi)(?<!\.py)(?<!\.rb)\z/i', $files['name']);
    }

    /**
     * Limiting file size
     * Allowing 2MB maximum
     *
     * @param file $files
     * @return boolean
     */
    public function limitFileSize($files)
    {
        return ($files['size'] < 2097152); //2MB
    }

    /**
     * Validating the photo format
     * allowing specific extention as follows
     *
     * @param file $files
     * @return boolean $ret
     */
    public function photoFormatCheck($files)
    {
        $ret = true;
        $allows_mime = [
              'image/jpeg', //.jpg
              'image/png', //.png
              'image/gif', //.gif
              'image/bmp', //.bmp
        ];
        $allows_ext = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        $ext = substr($files['name'], strrpos($files['name'], '.') + 1);
        if (!in_array($files['type'], $allows_mime) || !in_array($ext, $allows_ext)) {
              $ret = false;
        }
        return $ret;
    }
}