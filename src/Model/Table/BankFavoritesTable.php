<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Behavior\CounterCacheBehavior;

class BankFavoritesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bank_favorites');
        $this->belongsTo('Loan');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CounterCache', [
            'Loan' => ['bank_favorite_count']
        ]);
    }
}