<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class CategoriesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('category');
        $this->setPrimaryKey('id');
        $this->hasMany('SubCategories');
        $this->hasMany('corp_loan');
    }

}