<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class SubCategoriesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sub_category');
        $this->setPrimaryKey('id');
        $this->belongsTo('Categories');
        $this->hasMany('corp_loan');

    }

}