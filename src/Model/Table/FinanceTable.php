<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

class FinanceTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('corp_finance');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

    }

    public function validationDefault(Validator $validator)
    {
        $validator = parent::validationDefault($validator);

        $validator
            ->numeric('assets', __('0以上の数値を入力してください。'))
            ->maxLength('assets', 12, __('12桁以下の値を入力してください。'));

        return $validator;
    }

    public function validationDraft(Validator $validator)
    {
        $validator
            ->numeric('latest_revenue', __('0以上の数値を入力してください。'))
            ->maxLength('latest_revenue', 12, __('12桁以下の値を入力してください。'));

        $validator
            ->numeric('latest_profit_lose', __('0以上の数値を入力してください。'))
            ->maxLength('latest_profit_lose', 12, __('12桁以下の値を入力してください。'));

        $validator
            ->allowEmpty('assets')
            ->maxLength('assets', 12, __('12桁以下の値を入力してください。'))
            ->add('assets', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')]);

        $validator
            ->maxLength('latest_revenue', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('latest_revenue');

        $validator
            ->maxLength('latest_profit_lose', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('latest_profit_lose');

        $validator
            ->add('loan_balance', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('loan_balance', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('loan_balance');

        $validator
            ->add('revenue_one', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('revenue_one', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('revenue_one');

        $validator
            ->add('revenue_two', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('revenue_two', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('revenue_two');

        $validator
            ->add('revenue_three', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('revenue_three', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('revenue_three');

        $validator
            ->add('profits_losses_one', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('profits_losses_one', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('profits_losses_one');

        $validator
            ->add('profits_losses_two', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('profits_losses_two', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('profits_losses_two');

        $validator
            ->add('profits_losses_three', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('profits_losses_three', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('profits_losses_three');

        return $validator;
    }
}