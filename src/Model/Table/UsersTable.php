<?php
namespace App\Model\Table;

use CakeDC\Users\Model\table\UsersTable as UsersTableParent;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\Rule\IsUnique;
use Cake\Utility\Hash;
use Cake\Validation\Validator;


/**
 * Users Model
 */
class UsersTable extends UsersTableParent
{
    /**
     * Flag to set email check in buildRules or not
     *
     * @var bool
     */
    public $isValidateEmail = true;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('email');
        $this->removeBehavior('Password'); // Removes CakeDC/Users.Password behavior
        $this->addBehavior('Password'); // Adds src/Model/Behavior/PasswordBehavior.php
        $this->belongsTo('banks');
        $this->hasMany('corp_loan');
    }

    /**
     * Removes username validation rule from parent
     * Add role validation rule
     *
     * @param \Cake\Validation\Validator $validator Validator
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $custom_validator = parent::validationDefault($validator);

        $custom_validator
            ->remove('username');

        $custom_validator
            ->add('password', 'custom', [
                'rule' => function ($value, $context) {
                    return preg_match('/\A(?=.*?[a-z])(?=.*?[A-Z])(?=.*?\d)[a-zA-Z\d]{8,100}+\z/', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Password has to be at least 8 characters and must include at least one small and capital letter, and number')
            ]);

        $custom_validator
            ->requirePresence('role', 'create')
            ->notEmpty('role');

        return $custom_validator;
    }

    public function validationChangeEmail(Validator $validator)
    {
        $validator
            ->add('email', [
                'valid' => ['rule' => 'email'],
                'unique' => ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __d('CakeDC/Users', 'Email already exists')]
                ])
            ->notEmpty('email');

        return $validator;
    }

    public function validationPasswordConfirm(Validator $validator)
    {
        $validator
            ->requirePresence('password_confirm', 'create')
            ->notEmpty('password_confirm');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password')
            ->add('password', [
                'password_confirm_check' => [
                    'rule' => ['compareWith', 'password_confirm'],
                    'message' => __d('CakeDC/Users', 'Your password does not match your confirm password. Please try again'),
                    'allowEmpty' => false
                ]])
            ->add('password', 'custom', [
                'rule' => function ($value, $context) {
                    return preg_match('/\A(?=.*?[a-z])(?=.*?[A-Z])(?=.*?\d)[a-zA-Z\d]{8,100}+\z/', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Password has to be at least 8 characters and must include at least one small and capital letter, and number')
                ]);

        return $validator;
    }

    public function validationCorp(Validator $validator)
    {
        $validator
            ->notEmpty('company_name');

        $validator
            ->add('company_name_kana', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[ぁ-ん]+$/u', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Kana only')
            ]);

        $validator
            ->notEmpty('president_last_name');

        $validator
            ->notEmpty('president_first_name');

        $validator
            ->add('president_last_name_kana', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[ぁ-ん]+$/u', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Kana only')
            ])
            ->notEmpty('president_last_name_kana');

        $validator
            ->add('president_first_name_kana', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[ぁ-ん]+$/u', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Kana only')
            ])
            ->notEmpty('president_first_name_kana');

        $validator
            ->add('staff_count', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[1-9]|([0-9]{,5})$/', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Invalid number')
            ]);

        $validator
            ->url('url', __d('CakeDC/Users', 'Invalid format'))
            ->allowEmpty('url');

        $validator
            ->add('company_zipcode', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^.*[0-9０-９]$/u', $value) === 1;
                },
                'message' => __d('CakeDC/Users', '7 digits number only')
            ]);

        $validator
            ->notEmpty('company_address1');

        $validator
            ->notEmpty('company_address2');

        $validator
            ->add('company_phone', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Within 13 numbers including hyphen')
            ])
            ->allowEmpty('company_phone');

        $validator
            ->notEmpty('last_name');

        $validator
            ->notEmpty('first_name');

        $validator
            ->add('last_name_kana', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[ぁ-ん]+$/u', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Kana only')
            ])
            ->notEmpty('last_name_kana');

        $validator
            ->add('first_name_kana', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[ぁ-ん]+$/u', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Kana only')
            ])
            ->notEmpty('first_name_kana');

        $validator
            ->add('phone', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Within 13 numbers including hyphen')
            ]);

        return $validator;
    }

    public function validationBank(Validator $validator)
    {

        $validator
            ->notEmpty('bank_id');

        $validator
            ->notEmpty('last_name');

        $validator
            ->notEmpty('first_name');

            $validator
            ->add('last_name_kana', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^[ぁ-ん]+$/u', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Kana only')
            ])
            ->notEmpty('last_name_kana');

        $validator
            ->add('first_name_kana', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^[ぁ-ん]+$/u', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Kana only')
            ])
            ->notEmpty('first_name_kana');

        $validator
            ->add('phone', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/', $value) === 1;
                },
                'message' => __d('CakeDC/Users', 'Within 13 numbers including hyphen')
            ]);

    return $validator;
    }

}