<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class ConversationsTable extends Table
{
    public function initialize(array $config)
    {
        #リレーション
        $this->hasMany('ConversationUsers', [
            'foreign_key' => 'conversation_id',
            'dependent' => true,
        ]);

        $this->hasMany('ConversationMessages', [
            'foreign_key' => 'conversation_id',
            'dependent' => true,
        ]);

    }
}