<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

class LoanTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('corp_loan');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Categories');
        $this->belongsTo('SubCategories');
        $this->belongsTo('users');
        $this->hasMany('BankFavorites');

        // added for search
        $this->addBehavior('Search.Search');
        $this->searchManager()
        ->add('search', 'Search.Like', [
            'before' => true,
            'after' => true,
            'mode' => 'or',
            'comparison' => 'LIKE',
            'wildcardAny' => '*',
            'wildcardOne' => '?',
            'multiValue' => true,
            'multiValueSeparator' => '　',
            // 検索カラム名
            'field' => ['loan_title', 'job_description', 'loan_purpose', 'prospect']
          ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator = parent::validationDefault($validator);

        $validator
            ->maxLength('loan_title', 50, __('50文字以内で入力してください。'));

        $validator
            ->maxLength('job_description', 800, __('800文字以内で入力してください。'));

        $validator
            ->numeric('desired_loan_amount', __('0以上の数値を入力してください。'))
            ->maxLength('desired_loan_amount', 12, __('12桁以下の値を入力してください。'));

        $validator
            ->maxLength('loan_purpose_detail', 20, __('20文字以内で入力してください。'));

        $validator
            ->maxLength('loan_purpose', 800, __('800文字以内で入力してください。'));

        $validator
            ->maxLength('prospect', 800, __('800文字以内で入力してください。'))
            ->allowEmpty('prospect');

        $validator
            ->notEmpty('loan_purpose');

        $validator
            ->notEmpty('loan_purpose_detail', '資金使途を記載してください。', function ($context) {
                return ($context['data']['loan_purpose'] == '2');
            })
            ->maxLength('loan_purpose_detail', 20, __('20桁以下の値を入力してください。'));

        return $validator;
    }

    public function validationDraft(Validator $validator)
    {
        $validator
            ->numeric('desired_loan_amount', __('0以上の数値を入力してください。'))
            ->maxLength('desired_loan_amount', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('desired_loan_amount');

        $validator
            ->numeric('loan_period', __('0以上の数値を入力してください。'))
            ->maxLength('loan_period', 3, __('3桁以下の値を入力してください。'))
            ->allowEmpty('loan_period');

        $validator
            ->maxLength('loan_purpose', 800, __('800文字以内で入力してください。'))
            ->allowEmpty('loan_purpose');

        $validator
            ->maxLength('prospect', 800, __('800文字以内で入力してください。'))
            ->allowEmpty('prospect');

        return $validator;
    }

}