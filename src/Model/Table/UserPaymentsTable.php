<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class UserPaymentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_payment');
        $this->addBehavior('Timestamp');
        $this->belongsTo('PaymentDetails');
    }

}