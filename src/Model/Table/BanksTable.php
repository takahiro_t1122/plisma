<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class BanksTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('banks');
        $this->setPrimaryKey('id');
        $this->hasMany('Blocklists');
        $this->hasMany('users');
    }

}