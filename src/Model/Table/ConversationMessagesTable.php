<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class ConversationMessagesTable extends Table
{
    public function initialize(array $config)
    {
        #リレーション
        $this->belongsTo('Conversations', [
            'foreign_key' => 'conversation_id',
        ]);
        $this->belongsTo('Users', [
            'foreign_key' => 'user_id',
        ]);

    }
}