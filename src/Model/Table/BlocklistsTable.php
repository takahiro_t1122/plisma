<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

class BlocklistsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('corp_blocklists');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('banks');

    }
}