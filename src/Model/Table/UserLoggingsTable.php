<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class UserLoggingsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('user_logging');
        $this->addBehavior('UserLogging');
        $this->addBehavior('Timestamp');
    }

}