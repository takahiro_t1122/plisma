<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Validation\Validator;

class UploadFileForm extends Form
{
  protected function _buildValidator(Validator $validator)
  {
    $validator->provider('customValidate', 'App\Model\Validation\CustomValidation');

    $validator
      ->add('upload_file', 'fileFormatCheck', [
        'provider' => 'customValidate',
        'rule' => 'fileFormatCheck',
        'message' => __('pdf, ppt(x), xls(x), doc(x), txt, jpeg/jpg形式のみアップロード可能です'),
      ])
      ->add('upload_file', 'fileNameCheck', [
        'provider' => 'customValidate',
        'rule' => 'fileNameCheck',
        'message' => __('対象のファイル名は受け付けられません(日本語名不可)。ファイル名を変更してください'),
      ])
      ->add('upload_file', 'limitFileSize', [
        'provider' => 'customValidate',
        'rule' => 'limitFileSize',
        'message' => __('ファイルサイズの上限は2MBです'),
      ]);

    return $validator;
  }
}