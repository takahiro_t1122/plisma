<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * 会話作成用フォーム
 */
class ConversationForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema
            ->addField('to', ['type' => 'string'])
            ->addField('subject', ['type' => 'string'])
            ->addField('body', ['type' => 'text'])
            ->addField('user_id', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator)
    {
        $validator->add('to', [
            'notEmpty' => [
                'rule' => 'notEmpty',
                'last' => true,
                'message' => 'この項目は必須です'
            ]
        ]);

        $validator->add('subject', [
            'notEmpty' => [
                'rule' => 'notEmpty',
                'last' => true,
                'message' => 'この項目は必須です'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 128],
                'message' => 'この項目は128文字までです'
            ]
        ]);
        
        $validator->add('body', [
            'notEmpty' => [
                'rule' => 'notEmpty',
                'last' => true,
                'message' => 'この項目は必須です'
            ],
            'maxLength' => [
                'rule' => ['maxLength', 10000],
                'message' => 'この項目は10000文字までです'
            ]
        ]);

        return $validator;
    }

    protected function _execute(array $data)
    {
        return true;
    }
}