<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class LoanItemForm extends Form
{
    protected function _buildSchema(Schema $schema)
    {
        $schema
            ->addField('loan_title', ['type' => 'string'])
            ->addField('cateogry', ['type' => 'select'])
            ->addField('sub_category', ['type' => 'select'])
            ->addField('assets', ['type' => 'string'])
            ->addField('loan_balance', ['type' => 'string'])
            ->addField('job_description', ['type' => 'text'])
            ->addField('desired_loan_amount', ['type' => 'string'])
            ->addField('loan_period', ['type' => 'string'])
            ->addField('loan_purpose', ['type' => 'text'])
            ->addField('prospect', ['type' => 'text'])
            ->addField('fiscal_year_one', ['type' => 'select'])
            ->addField('fiscal_year_two', ['type' => 'select'])
            ->addField('fiscal_year_three', ['type' => 'select'])
            ->addField('revenue_one', ['type' => 'string'])
            ->addField('revenue_two', ['type' => 'string'])
            ->addField('revenue_three', ['type' => 'string'])
            ->addField('profits_losses_one', ['type' => 'string'])
            ->addField('profits_losses_two', ['type' => 'string'])
            ->addField('profits_losses_three', ['type' => 'string']);

        return $schema;
    }

    protected function _buildValidator(Validator $validator)
    {
        $validator
            ->notEmpty('loan_title')
            ->maxLength('loan_title', 50, __('50文字以下の値を入力してください。'));

        $validator
            ->notEmpty('category');

        $validator
            ->notEmpty('sub_category');

        $validator
            ->maxLength('assets', 12, __('12桁以下の値を入力してください。'))
            ->add('assets', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')]);

        $validator
            ->maxLength('job_description', '800', __('800文字以内で入力してください。'));

        $validator
            ->maxLength('desired_loan_amount', 12, __('12桁以下の値を入力してください。'))
            ->add('desired_loan_amount', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')]);

        $validator
            ->numeric('loan_period', __('0以上の数値を入力してください。'))
            ->maxLength('loan_period', 3, __('3桁以下の値を入力してください。'));

        $validator
            ->maxLength('prospect', '800', __('800文字以内で入力してください。'))
            ->allowEmpty('prospect');

        $validator
            ->add('loan_balance', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('loan_balance', 12, __('12桁以下の値を入力してください。'));

        $validator
            ->add('revenue_one', 'custom', [
                'rule' => function ($value, $context) {
                  return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('revenue_one', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('revenue_one');

        $validator
            ->add('revenue_two', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('revenue_two', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('revenue_two');

        $validator
            ->add('revenue_three', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('revenue_three', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('revenue_three');

        $validator
            ->add('profits_losses_one', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('profits_losses_one', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('profits_losses_one');

        $validator
            ->add('profits_losses_two', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('profits_losses_two', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('profits_losses_two');

        $validator
            ->add('profits_losses_three', 'custom', [
                'rule' => function ($value, $context) {
                return preg_match('/^-?[0-9]+(,-?[0-9]+)*$/', $value) === 1;
                },
                'message' => __('数値を入力してください。')])
            ->maxLength('profits_losses_three', 12, __('12桁以下の値を入力してください。'))
            ->allowEmpty('profits_losses_three');

        $validator
            ->notEmpty('loan_purpose');

        $validator
            ->notEmpty('loan_purpose_detail', '資金使途を記載してください。', function ($context) {
                return ($context['data']['loan_purpose'] == '2');
            })
            ->maxLength('loan_purpose_detail', '20', __('20桁以下の値を入力してください。'));

        return $validator;
    }

    protected function _execute(array $data)
    {
        return true;
    }
}