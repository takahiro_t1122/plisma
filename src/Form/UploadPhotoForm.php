<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Validation\Validator;

class UploadPhotoForm extends Form
{
  protected function _buildValidator(Validator $validator)
  {
    $validator->provider('customValidate', 'App\Model\Validation\CustomValidation');

    $validator
      ->add('upload_photo', 'photoFormatCheck', [
        'provider' => 'customValidate',
        'rule' => 'photoFormatCheck',
        'message' => __('jp(e)g, png, gif, bmp形式のみアップロード可能です'),
      ])
      ->add('upload_photo', 'limitphotoSize', [
        'provider' => 'customValidate',
        'rule' => 'limitphotoSize',
        'message' => __('ファイルサイズの上限は2MBです'),
      ]);

    return $validator;
  }
}