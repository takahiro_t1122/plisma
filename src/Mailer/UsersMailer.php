<?php

namespace App\Mailer;

use Cake\Datasource\EntityInterface;
use CakeDC\Users\Mailer\UsersMailer as UsersMailerParent;
use Cake\Core\Configure;

class UsersMailer extends UsersMailerParent
{
    public function validation(EntityInterface $user)
    {
        // un-hide the token to be able to send it in the email content
        $user->setHidden(['password', 'token_expires', 'api_token']);
        $subject = __('【plisma】ユーザー本登録のお願い');
        $this
            ->setTo($user['email'])
             ->setSubject($subject)
             ->setViewVars($user->toArray())
             ->setTemplate('validation');
    }

    public function resetPassword(EntityInterface $user)
    {
        $subject = __('【plisma】パスワード再設定のご案内');
        // un-hide the token to be able to send it in the email content
        $user->setHidden(['password', 'token_expires', 'api_token']);

        if($user->role == ROLE_BANK) {
            $this->loadModel('Banks');
            $user->bank_name = $this->Banks->find()->where(['id' => $user['bank_id']])->first()->bank_name;
        }
        
        $this
            ->setTo($user['email'])
            ->setSubject($subject)
            ->setViewVars($user->toArray())
            ->setTemplate('resetPassword');
    }

    /**
     * Send the change email-address email to the user
     *
     * @param EntityInterface $user User entity
     *
     * @return void
     */
    protected function changeEmail(EntityInterface $user, $reference)
    {
        $subject = __('【plisma】メールアドレス変更のご確認');
        // un-hide the token to be able to send it in the email content
        $user->setHidden(['password', 'token_expires', 'api_token']);

        if($user->role == ROLE_BANK) {
            $this->loadModel('Banks');
            $user->bank_name = $this->Banks->find()->where(['id' => $user['bank_id']])->first()->bank_name;
        }

        $this
            ->setTo($reference)
            ->setSubject($subject)
            ->setViewVars($user->toArray())
            ->setTemplate('changeEmail');
    }

    /**
     * Send emails after changing email to both old and new email address
     *
     * @param EntityInterface $user User entity
     *
     * @return void
     */
    protected function completeChangeEmail(EntityInterface $user)
    {
        $subject = __('【plisma】メールアドレス変更完了のご連絡');
        if($user->role == ROLE_BANK) {
            $this->loadModel('Banks');
            $user->bank_name = $this->Banks->find()->where(['id' => $user['bank_id']])->first()->bank_name;
        }

        $this
            ->setTo($user->email)
            ->setCc($user->change_email)
            ->setSubject($subject)
            ->setViewVars($user->toArray())
            ->setTemplate('completeChangeEmail');
    }

    /**
    * Auto send mail when bank user register the service
    * @param EntityInterface $user User entity
    * @return void
    */
    protected function registerNotificationEmail(EntityInterface $user)
    {
        $subject = __('【内部連絡】銀行ユーザ新規登録通知');

        $this->loadModel('Banks');
        $user->bank_name = $this->Banks->find()->where(['id' => $user['bank_id']])->first()->bank_name;

        $this
            // TODO changes needed
            ->setTo('plisma.redmine@gmail.com')
            ->setSubject($subject)
            ->setViewVars($user->toArray())
            ->setTemplate('registerNotificationEmail');
    }
}