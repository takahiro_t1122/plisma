<?php
use Cake\Core\Configure;

return [
    // definition of consts
    define("ROLE_BANK", "bank"),
    define("ROLE_CORP", "corp"),
    define("ROLE_NONE", "user"),
    define("ROLE_ADMIN", "superuser"),

    define("PNG", ".png"),
    // definition of variables
    // Configure::write("text1", "変数のテキスト"),

    define("CEO", "石亀直樹"),
    define("ADDRESS", "134-0084 東京都江戸川区東葛西1丁目1番4号 201号室"),
    define("COMPANY_URL", "http://www.plisma.co.jp"),
    define("SERVICE_URL", "https://www.plisma.jp"),
    define("EMAIL_BASE", "info@plisma.co.jp"),
    define("EMAIL_TO", "contact@plisma.co.jp"),
    define("EMAIL_SUPPORT", "support@plisma.co.jp"),


];