<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Cake\Core\Configure;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

/**** Users functions ****/
$routes->connect('/users/login', ['controller' => 'Users', 'action' => 'login']);
$routes->connect('/users/register', ['controller' => 'Users', 'action' => 'register']);
$routes->connect('/users/register-detail-corp', ['controller' => 'Users', 'action' => 'registerDetailCorp']);
$routes->connect('/users/register-detail-bank', ['controller' => 'Users', 'action' => 'registerDetailBank']);
$routes->connect('/users/bank-auto-comp', ['controller' => 'Users', 'action' => 'bankAutoComp']);
$routes->connect('/users/confirm-detail', ['controller' => 'Users', 'action' => 'confirmDetail']);
$routes->connect('/users/complete-detail', ['controller' => 'Users', 'action' => 'completeDetail']);
$routes->connect('/users/request-reset-password', ['controller' => 'Users', 'action' => 'requestResetPassword']);
$routes->connect('/users/validate-email/*', ['controller' => 'Users', 'action' => 'validateEmail']);
$routes->connect('/users/reset-password/*', ['controller' => 'Users', 'action' => 'resetPassword']);
$routes->connect('/users/change-password/*', ['controller' => 'Users', 'action' => 'changePassword']);
$routes->connect('/users/logout', ['controller' => 'Users', 'action' => 'logout']);
$routes->connect('/users/profile', ['controller' => 'Users', 'action' => 'profile']);
$routes->connect('/users/edit/*', ['controller' => 'Users', 'action' => 'edit']);
$routes->connect('/users/delete/*', ['controller' => 'Users', 'action' => 'delete']);
$routes->connect('/users/change-email', ['controller' => 'Users', 'action' => 'changeEmail']);
$routes->connect('/users/confirm-email/*', ['controller' => 'Users', 'action' => 'confirmEmail']);
$routes->connect('/users/complete-change-email', ['controller' => 'Users', 'action' => 'completeChangeEmail']);
/**** Corp functions ****/
$routes->connect('/corp/create-loan-item', ['controller' => 'Corp', 'action' => 'createLoanItem']);
$routes->connect('/corp/get-subcategory-list', ['controller' => 'Corp', 'action' => 'getSubcategoryList']);
$routes->connect('/corp/view-loan-item', ['controller' => 'Corp', 'action' => 'viewLoanItem']);
$routes->connect('/corp/visible-status-change', ['controller' => 'Corp', 'action' => 'visibleStatusChange']);
$routes->connect('/corp/delete-loan-item', ['controller' => 'Corp', 'action' => 'deleteLoanItem']);
$routes->connect('/corp/delete-finance-item', ['controller' => 'Corp', 'action' => 'deleteFinanceInfo']);
$routes->connect('/corp/update-loan-item', ['controller' => 'Corp', 'action' => 'updateLoanItem']);
$routes->connect('/corp/update-finance-item', ['controller' => 'Corp', 'action' => 'updateFinanceItem']);
$routes->connect('/corp/view-block/*', ['controller' => 'Corp', 'action' => 'viewBlock']);
$routes->connect('/corp/get-bank-list-by-search', ['controller' => 'Corp', 'action' => 'getBankListBySearch']);
$routes->connect('/corp/file-download/*', ['controller' => 'Corp', 'action' => 'fileDownload']);
$routes->connect('/corp/file-delete/*', ['controller' => 'Corp', 'action' => 'fileDelete']);
$routes->connect('/corp/upload-loan-image', ['controller' => 'Corp', 'action' => 'uploadLoanImage']);
$routes->connect('/corp/delete-loan-image/*', ['controller' => 'Corp', 'action' => 'deleteLoanImage']);
/**** Bank functions ****/
$routes->connect('/bank/view-detail', ['controller' => 'Bank', 'action' => 'viewDetail']);
$routes->connect('/bank/search', ['controller' => 'Bank', 'action' => 'search']);
$routes->connect('/bank/favorite-action', ['controller' => 'Bank', 'action' => 'favoriteAction']);

//temp
$routes->connect('/corp/stripe-func', ['controller' => 'Corp', 'action' => 'stripeFunc']);

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    /**
     * Apply a middleware to the current route scope.
     * Requires middleware to be registered via `Application::routes()` with `registerMiddleware()`
     */
    $routes->applyMiddleware('csrf');

    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */

    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *
     * ```
     * $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);
     * $routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);
     * ```
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * Router::scope('/api', function (RouteBuilder $routes) {
 *     // No $routes->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */