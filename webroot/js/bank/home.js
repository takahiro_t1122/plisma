$(function() {
    $('ul input[name="category[]"]').change(function(){
        if ($(this).is(':checked')) {
            $(this).parents('.category').find('input[type="checkbox"]').prop('checked', true);
        } else {
            $(this).parents('.category').find('input[type="checkbox"]').prop('checked', false);
            $(this).parents('li').each(function(){
                $(this).children('input[type="checkbox"]').prop('checked', false);
            });
        }
    });

    $('li input[name="sub_category[]"]').change(function(){
        if (!$(this).is(':checked')) {
            $(this).parents('li.category').find('input[name="category[]"]').prop('checked', false);
        }
    });

    $('#reset').click(function(){
        $('#search_param').find("textarea, :text, select").val("").end().find(":checked").prop("checked", false);
    });
});