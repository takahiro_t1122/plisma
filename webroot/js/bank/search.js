$(function() {
    $('#sort').change(function(){
        $("#sort_param").submit();
    });

    $('ul input[name="category[]"]').change(function(){
        if ($(this).is(':checked')) {
            $(this).parents('.category').find('input[type="checkbox"]').prop('checked', true);
        } else {
            $(this).parents('.category').find('input[type="checkbox"]').prop('checked', false);
            $(this).parents('li').each(function(){
                $(this).children('input[type="checkbox"]').prop('checked', false);
            });
        }
    });

    $('li input[name="sub_category[]"]').change(function(){
        if (!$(this).is(':checked')) {
            $(this).parents('li.category').find('input[name="category[]"]').prop('checked', false);
        }
    });


    $('#reset').click(function(){
        $('#search_param').find("textarea, :text, select").val("").end().find(":checked").prop("checked", false);
    });

    // modal
    $('input[data-modal]').click(function(event) {
        $(this).modal();
        return false;
    });

    $(function(){
        var url = location.href;
        var id = 'category-update';
        if (url.match(id)) {
            $('#category-update').modal();
        }
    });

    $.modal.defaults = {
        closeExisting: true,    // Close existing modals. Set this to false if you need to stack multiple modal instances.
        escapeClose: true,      // Allows the user to close the modal by pressing `ESC`
        clickClose: true,       // Allows the user to close the modal by clicking the overlay
        showClose: false,       // Shows a (X) icon/link in the top-right corner
        fadeDuration: 500
    }
});