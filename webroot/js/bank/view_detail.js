$(function() {
    // ajax for favorite button
    $('#fav_btn').click(function(){
        var loan_id = var_loan_id;

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/bank/favorite-action',
            data: {
                "loan_id" : loan_id,
            },
        })
        .done(function(data, textStatus, jqXHR){
            if (data.result == 'error') {
                alert(data.message);
            } else {
                location.reload();
            }

        })
        // Ajaxリクエストが失敗した時発動
        .fail(function(XMLHttpRequest, textStatus, errorThrown){
            alert("失敗");
            console.log("ajax通信に失敗しました");
            console.log("XMLHttpRequest : " + XMLHttpRequest.status);
            console.log("textStatus     : " + textStatus);
            console.log("errorThrown    : " + errorThrown.message);
        });
    })
});