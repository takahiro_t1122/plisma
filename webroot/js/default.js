$(function() {
    // alert time setting
    setTimeout(function(){
        $('.alert').fadeOut(3000);
    }, 3000);

    // height adjustment
    var len = $('.contents-wrapper').length;
    if (len == 1) {
        screen_height = $(window).height();
        header_height = $('header').height();
        content_height_wm = $('.contents-wrapper').outerHeight(true);
        content_height_wom = $('.contents-wrapper').outerHeight();
        content_mt_height = (content_height_wm - content_height_wom) / 2;
        footer_height = $('footer').height();

        content_mb_height = screen_height - header_height - footer_height - content_height_wom - content_mt_height;
        if (content_mb_height > 0) {
            $('.contents-wrapper').css('margin-bottom', content_mb_height);
        }
    }
});