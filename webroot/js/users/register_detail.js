$(function(){
    $('input[name=role]').change(function() {
        if ($(this).val() == 'corp') {
            $("label[for='role-bank']").css('background-color', 'initial');
            $("label[for='role-corp']").css('background-color', '#d1d0d2');
        } else if ($(this).val() == 'bank') {
            $("label[for='role-bank']").css('background-color', '#d1d0d2');
            $("label[for='role-corp']").css('background-color', 'initial');
        } else {
            $("label[for='role-bank']").css('background-color', 'initial');
            $("label[for='role-corp']").css('background-color', 'initial');
        }
    });
});