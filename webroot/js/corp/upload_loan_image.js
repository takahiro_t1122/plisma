$(function() {
    $('#upload-link').on('click', function(e) {
        e.preventDefault();
        var win = window.open(this.href, 'window', 'width=1000,height=800');
        $(win).on('load', function () {
            $(win).on('unload', function () {
                var $header_img = "/" + header_img_var;
                if (fileExistCheck($header_img)) {
                    $('#loan-header-img img').remove();
                    $("#loan-header-img").append('<img src=' + $header_img + ' class="w-100" />');
                }
            });
        });
    })

    function fileExistCheck(image_path){
        var xhr = new XMLHttpRequest();
        try {
            xhr.open("HEAD", image_path, false);
            xhr.send(null);
            if (xhr.status == 200) {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    }
});