$(function() {
    $('#category').change(function(){
        var category_id = $(this).val();
        var csrf = $('[name="_csrfToken"]').val();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/corp/get-subcategory-list',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', csrf);
            },
            data: {
                "category_id" : category_id,
            },
        })
        .done(function(sub_category_list, textStatus, jqXHR){
            $('#sub-category option').remove();
            for(var i in sub_category_list) {
                $("#sub-category").append("<option value=" +sub_category_list[i].id + ">" + sub_category_list[i].name +  "</option>");
            }
        })
        // Ajaxリクエストが失敗した時発動
        .fail(function(XMLHttpRequest, textStatus, errorThrown){
            alert("失敗");
            console.log("ajax通信に失敗しました");
            console.log("XMLHttpRequest : " + XMLHttpRequest.status);
            console.log("textStatus     : " + textStatus);
            console.log("errorThrown    : " + errorThrown.message);
        });
    })
});