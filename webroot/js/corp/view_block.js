$(function() {
    // ajax for bank list
    $('#search_btn').click(function(){
        var search_value = $('#search-value').val();
        var csrf = $('[name="_csrfToken"]').val();
        var get_fields = $('[name="_Token[fields]"]').val();
        var get_key = $('[name="_Token[key]"]').val();
        var get_unlocked = $('[name="_Token[unlocked]"]').val();
        var get_debug = $('[name="_Token[debug]"]').val();

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/corp/get-bank-list-by-search',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', csrf);
            },
            data: {
                "search_value" : search_value,
                "_Token" : {
                    "fields" : get_fields,
                    "key" : get_key,
                    "unlocked" : get_unlocked,
                    "debug" : get_debug
                },
            },
        })
        .done(function(bank_list, textStatus, jqXHR){
            $('#bank-id option').remove();
            for(var i in bank_list) {
                $("#bank-id").append("<option value=" + bank_list[i].id + ">" + bank_list[i].bank_name +  "</option>");
            }
        })
        // Ajaxリクエストが失敗した時発動
        .fail(function(XMLHttpRequest, textStatus, errorThrown){
            alert("失敗");
            console.log("ajax通信に失敗しました");
            console.log("XMLHttpRequest : " + XMLHttpRequest.status);
            console.log("textStatus     : " + textStatus);
            console.log("errorThrown    : " + errorThrown.message);
        });
    })
});