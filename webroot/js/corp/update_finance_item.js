$(function() {
    $('[name="fiscal_year_one[year]"]').change(function(){
        var y_one = $('[name="fiscal_year_one[year]"]').val();
        if (y_one != "") {
            $('[name="fiscal_year_two[year]"] option[value='+y_one+']').css('display','none');
            $('[name="fiscal_year_three[year]"] option[value='+y_one+']').css('display','none');
        }
    })

    $('[name="fiscal_year_two[year]"]').change(function(){
        var y_two = $('[name="fiscal_year_two[year]"]').val();
        if (y_two != "") {
            $('[name="fiscal_year_one[year]"] option[value='+y_two+']').css('display','none');
            $('[name="fiscal_year_three[year]"] option[value='+y_two+']').css('display','none');
        }
    })

    $('[name="fiscal_year_three[year]"]').change(function(){
        var y_three = $('[name="fiscal_year_three[year]"]').val();
        if (y_three != "") {
            $('[name="fiscal_year_one[year]"] option[value='+y_three+']').css('display','none');
            $('[name="fiscal_year_two[year]"] option[value='+y_three+']').css('display','none');
        }
    })

    /**
    * 数値の3桁カンマ区切り
    * 入力値をカンマ区切りにして返却
    * [引数]   numVal: 入力数値
    * [返却値] String(): カンマ区切りされた文字列
    */
    function addFigure(numVal) {
    // 空の場合そのまま返却
    if (numVal == ''){
        return '';
    }
    // 全角から半角へ変換し、既にカンマが入力されていたら事前に削除
    numVal = toHalfWidth(numVal).replace(/,/g, "").trim();
    // 数値でなければそのまま返却
    if ( !/^[+|-]?(\d*)(\.\d+)?$/.test(numVal) ){
        return numVal;
    　　}
    // 整数部分と小数部分に分割
    var numData = numVal.toString().split('.');
    // 整数部分を3桁カンマ区切りへ
    numData[0] = Number(numData[0]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    // 小数部分と結合して返却
    return numData.join('.');
    }

    /**
    * カンマ外し
    * 入力値のカンマを取り除いて返却
    * [引数]   strVal: 半角でカンマ区切りされた数値
    * [返却値] String(): カンマを削除した数値
    */
    function delFigure(strVal){
    return strVal.replace( /,/g , "" );
    }

    /**
    * 全角から半角への変革関数
    * 入力値の英数記号を半角変換して返却
    * [引数]   strVal: 入力値
    * [返却値] String(): 半角変換された文字列
    */
    function toHalfWidth(strVal){
    // 半角変換
    var halfVal = strVal.replace(/[！-～]/g,
        function( tmpStr ) {
        // 文字コードをシフト
        return String.fromCharCode( tmpStr.charCodeAt(0) - 0xFEE0 );
        }
    );
    return halfVal;
    }

    /**
    * 処理を適用するテキストボックスへのイベント設定
    * onBlur : カンマ区切り処理実施
    * onFocus : カンマ削除処理実施
    */
    var elm5 = document.getElementById('revenue-one');
    var elm6 = document.getElementById('revenue-two');
    var elm7 = document.getElementById('revenue-three');
    var elm8 = document.getElementById('profits-losses-one');
    var elm9 = document.getElementById('profits-losses-two');
    var elm10 = document.getElementById('profits-losses-three');
    elm5.addEventListener('blur', function(){ this.value = addFigure(this.value) }, false);
    elm6.addEventListener('blur', function(){ this.value = addFigure(this.value) }, false);
    elm7.addEventListener('blur', function(){ this.value = addFigure(this.value) }, false);
    elm8.addEventListener('blur', function(){ this.value = addFigure(this.value) }, false);
    elm9.addEventListener('blur', function(){ this.value = addFigure(this.value) }, false);
    elm10.addEventListener('blur', function(){ this.value = addFigure(this.value) }, false);
    elm5.addEventListener('focus', function(){ this.value = delFigure(this.value) }, false);
    elm6.addEventListener('focus', function(){ this.value = delFigure(this.value) }, false);
    elm7.addEventListener('focus', function(){ this.value = delFigure(this.value) }, false);
    elm8.addEventListener('focus', function(){ this.value = delFigure(this.value) }, false);
    elm9.addEventListener('focus', function(){ this.value = delFigure(this.value) }, false);
    elm10.addEventListener('focus', function(){ this.value = delFigure(this.value) }, false);
});